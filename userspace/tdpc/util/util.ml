(** [is_uint16 i] is whether [i] fits within an unsigned 16-bit int. *)
let is_uint16 (i : int) : bool =
  not (i < 0 || i > 0xffff)

(** [opt_uint32 i] is [Some i] if [i] fits in an [int32], else [None]. *)
let opt_uint32 (i : int) : int32 option =
  if i < 0 || i > 0xffffffff then None
  else Some (Int32.of_int i)

(** [add_c_string buf s] adds [s] as a C-string to [buf]. *)
let add_c_string (buf : Buffer.t) (s : string) : unit =
  Buffer.add_string buf s;
  Buffer.add_char buf '\x00'
