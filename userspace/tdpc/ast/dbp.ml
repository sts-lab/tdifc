(** DBP AST types, which corresponds to the underlying DBP format expected by
    the LSM. Having two separate AST types allows dropping location
    information (since some statements may not correspond to a single parsed
    statement, and we no longer expect user errors) and better typing (since
    some AST nodes have been desugared into other nodes). *)

type namespace =
  | Unique
  | Shared of int32

type logfile =
  | Fd of int32
  | File of string

type flow_policy =
  | MaxProcess of int32
  | MaxSocket of int32

type tag_frag =
  | String of string
  | Capture of int

type capability =
  | Plus (* can increase security level *)
  | Minus (* can decrease security level *)

type tag = tag_frag list

type stmt =
  | Addtags of (capability list * tag) list
  | Deltags of tag list
  | Settags of (capability list * tag) list
  | Addcaps of (capability list * tag) list
  | Delcaps of (capability list * tag) list
  | Setcaps of (capability list * tag) list
  | Dropcaps

type pid =
  | Self
  | Parent
  | Children
  | CapturePID of int
  | Literal of int32

(* pid list must be nonempty *)
type block = pid list * stmt list

type policy = {
    id : int32;
    ns : namespace;
    logfiles : logfile list;
    flow_policies : flow_policy list;
    match_blocks : (Regex.t * block list) list;
    init_blocks : block list;
  }
