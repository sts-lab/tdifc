(** Parser AST node types. *)

(** Attaches positions to a value. *)
type 'a node = {
    v : 'a;
    p0 : Lexing.position; (* start position *)
    p1 : Lexing.position; (* end position *)
  }

(** Namespaces are either Unique or some shared namespace. *)
type namespace' = Dbp.namespace =
  | Unique
  | Shared of int32
type namespace = namespace' node

(** Monitored logs are either written to a file descriptor or some path. *)
type logfile' = Dbp.logfile =
  | Fd of int32
  | File of string
type logfile = logfile' node

type flow_policy' = Dbp.flow_policy =
  | MaxProcess of int32
  | MaxSocket of int32
type flow_policy = flow_policy' node

(** A tag fragment is either a string literal or a capture index. *)
type tag_frag' = Dbp.tag_frag =
  | String of string
  | Capture of int
type tag_frag = tag_frag' node

(** Tag ownership capabilities. *)
type capability' = Dbp.capability =
  | Plus (* can increase security level *)
  | Minus (* can decrease security level *)
type capability = capability' node

(** A tag is a nonempty list of tag fragments. *)
type tag = tag_frag list node (* nonempty *)

(** Statements within process blocks. *)
type stmt' =
  | Addtags of (capability list * tag) list (* tuple list is nonempty *)
  | Deltags of tag list (* nonempty *)
  | Settags of (capability list * tag) list
  | Addcaps of (capability list * tag) list (* tuple list is nonempty *)
  | Delcaps of (capability list * tag) list (* tuple list is nonempty *)
  | Setcaps of (capability list * tag) list
  | Dropcaps
type stmt = stmt' node

(** PID to affect *)
type pid' = Dbp.pid =
  | Self
  | Parent
  | Children
  | CapturePID of int
  | Literal of int32
type pid = pid' node

(** Process block, i.e., what PIDs to affect *)
type process_block' = pid list * stmt list
type process_block = process_block' node

(** Top-level statements, i.e., all possible statements in a .dp file. *)
type top_stmt' =
  (* Policy ID. Each policy may only declare one ID, and it must be unique
     (relative to all other loaded policies). *)
  | PolicyID of int32 node
  | Include of string
  | Logfile of logfile
  | Namespace of namespace
  | FlowPolicy of flow_policy
  | Match of Regex.t node * process_block list
  | Init of process_block list
type top_stmt = top_stmt' node

(** Output policies won't have any Include nodes (they will already be
    resolved), although this isn't enforced at the type level. *)
type policy = top_stmt list node
