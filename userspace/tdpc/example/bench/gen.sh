#!/bin/sh

set -eu

max_labelsize=20
max_stmts=20

# gen LABELSIZE STATEMENTS
gen() {
  local labelsize=$1
  local stmts=$2
  local id=$(( 100 + max_stmts * (labelsize - 1) + (stmts - 1) ))
  local dpfile="$labelsize-$stmts.dp"
  local idfile="$labelsize-$stmts.id"
  printf "$id\n" > $idfile

  # header: id, match
  cat >$dpfile <<EOF
id $id;
namespace unique;
logfile stderr;

match "Benchmark: <.*>\n" {
  process self {
EOF

  # settags: LABELSIZE label, STATEMENTS times
  local i
  local k
  for ((i = 0; i != stmts; ++i)); do
    printf "    settags " >>$dpfile

    # add LABELSIZE-1 labels
    for ((k = 1; k != labelsize; ++k)); do
      printf "+tag(\"$k\") " >>$dpfile
    done

    printf "+tag(<1>);\n" >>$dpfile
  done

  # end syntax
  cat >>$dpfile <<EOF
  }
}
EOF
}

for i in $(seq 1 "$max_labelsize"); do
  for k in $(seq 1 "$max_stmts"); do
    gen "$i" "$k"
  done
done
