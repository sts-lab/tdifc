# DIFC Policy Compiler

This compiles TDIFC policy sources (`\*.dp`) into TDIFC binary policies
(`\*.dbp`) which can be loaded at runtime by the TDIFC Linux Security Module.
Mount `tdifcfs` and then pass the absolute path to a binary policy to
`dbp_load_file`, e.g.:

```
$ echo '/etc/difc/foo.dp' > /sys/fs/tdifc/dbp_load_file
```

## Build Requirements

Version requirements may not be strict; they merely reflect dependency
versions used during development and testing.

* `ocaml >= 4.08.1`
* `dune >= 2.6.0`
* `findlib >= 1.8.1`
* `sedlex >= 2.1`
* `ocaml-migrate-parsetree >= 1.5.0`
* `ppx_tools_versioned >= 5.2.3`
* `menhir >= 2.1`
* `ctypes >= 0.15.1`
* `GNU Make >= 4.2.1` (can probably be older)
* `GNU Bison >= 3.5.1`

## dune note

This project was originally developed with dune 2.2, but the no-infer stanza
introduced in dune 2.6 is very useful for interoperation with the C regex
library. If you specifically need to use a version of dune < 2.6, then comment
out the no-infer stanza and uncomment the old stanza in `regex/c/dune`.
