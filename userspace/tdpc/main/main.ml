module Fmt = Format

let ( let* ) = Result.bind

(** [fmt_pos out p] formats [p], a pair of positions for errors. *)
let fmt_pos (fmt : Fmt.formatter)
    ((p0, p1) : Lexing.position * Lexing.position) : unit =
  let open Lexing in
  let col p = p.pos_cnum - p.pos_bol in
  Fmt.fprintf fmt "%s:(%u,%u) - (%u,%u)"
    p0.pos_fname p0.pos_lnum (col p0) p1.pos_lnum (col p1)

(** [fmt_err_t fmt error] formats the type of [error]. *)
let fmt_err_t (fmt : Fmt.formatter) = function
  | `LexerError _ -> Fmt.fprintf fmt "lexical error"
  | `ParserError _ -> Fmt.fprintf fmt "syntax error"
  | `ParserCheckError _
  | `ConvertASTError _ -> Fmt.fprintf fmt "error"
  | _ -> assert false

(** [print_error error] formats and prints a message based on [error]. *)
let rec print_error error =
  match error with
  | `LexerError (p0, p1, msg)
  | `ParserError (p0, p1, msg)
  | `ParserCheckError (p0, p1, msg)
  | `ConvertASTError (p0, p1, msg) -> begin
    Fmt.eprintf "%a: %a: %s\n" fmt_pos (p0, p1) fmt_err_t error msg
  end
  | `ParserIncludeError (l, e) -> begin
    List.iter (fun p -> Fmt.eprintf "included by %a:\n" fmt_pos p) l;
    print_error e
  end
  | `SerializeDBPError msg
  | `ConvertASTMissing msg -> Fmt.eprintf "%s\n" msg

let output_bytecode (path : string) (p : Ast.Dbp.policy) : unit =
  let out = open_out_bin path in
  let fmt = Fmt.formatter_of_out_channel out in
  Serialize.Bytecode.print fmt p |> Result.iter_error print_error;
  try close_out out
  with Sys_error msg -> Fmt.eprintf "%s\n" msg

let compile (path : string) (out_path : string) : unit =
  let p =
    let* p = Parse.parse path in
    let* p = Convert_ast.to_dbp p in
    Format.printf "%a@." Serialize.Pprint.pp_policy p;
    output_bytecode out_path p;
    Ok ()
  in
  match p with
  | Ok _ -> ()
  | Error e -> print_error e

let default_out (path : string) : string =
  Filename.remove_extension path ^ ".dbp"

let main () =
  let input = ref None in
  let output = ref None in
  let specs = [
    ("-o", Arg.String (fun s -> output := Some s),
      "Name of output file; defaults to <input>.dbp")
    ]
  in
  let anon_fn (a : string) : unit =
    match !input with
    | None -> input := Some a
    | Some _ -> raise @@ Arg.Bad "only one input is allowed"
  in
  let usage = Printf.sprintf (
"usage: %s INPUT [-o OUTPUT]

Compiles INPUT into a DIFC Binary Policy file.\n") Sys.argv.(0)
  in
  Arg.parse specs anon_fn usage;
  match !input, !output with
  | None, _ -> print_string usage
  | Some input, None -> compile input (default_out input)
  | Some input, Some output -> compile input output

let () = main ()
