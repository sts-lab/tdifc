type 'a error = [
  | Parse_file.error
  | 'a Include.error
  ]

let ( >>= ) = Result.bind

let parse path =
  Parse_file.parse path >>= fun p ->
  Include.resolve_includes p
