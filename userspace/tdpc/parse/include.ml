module P = Ast.Parsed
module H = Hashtbl

type 'a error = 'a constraint 'a = [
  | `ParserIncludeError of (Lexing.position * Lexing.position) list
                           * [> Parse_file.error]
  ]

let ( >>= ) = Result.bind
let ( >>!| ) r f = Result.map_error f r

(* dune doesn't like ppx + let* for some reason
let ( let* ) = Result.bind
let ( let+! ) r f = Result.map_error f r
*)

let resolve_includes (policy : P.policy) =
  let parsed_files : (string, unit) H.t = H.create 64 in
  H.add parsed_files P.(policy.p0.pos_fname) ();
  let rec aux (stmts : P.top_stmt list)
      : (P.top_stmt list, _ error) result =
    match stmts with
    | [] -> Ok []
    | P.{ v = Include path; p0; p1 } :: ss
      when not (H.mem parsed_files path) -> begin
      H.add parsed_files path ();
      (* mixing map_error and bind is an awkward affair,
         but is needed to make the result error type correct *)
      Parse_file.parse path >>!| (fun e ->
        `ParserIncludeError ([(p0, p1)], e)
      ) >>= fun p ->
      aux p.v >>!| (fun (`ParserIncludeError (l, e)) ->
        `ParserIncludeError ((p0, p1) :: l, e)
      ) >>= fun inc ->
      aux ss >>= fun ss ->
      Ok (inc @ ss)
      (* dune doesn't like ppx + let* for some reason
      let* p =
        let+! e = Parse_file.parse path in
        `ParserIncludeError ([(p0, p1)], e)
      in
      let* inc =
        let+! `ParserIncludeError (l, e) = aux p.v in
        `ParserIncludeError ((p0, p1) :: l, e)
      in
      let* ss = aux ss in
      Ok (inc @ ss)
      *)
    end
    | P.{ v = Include _; _ } :: ss -> aux ss
    | s :: ss -> begin
      (* dune doesn't like ppx + let* for some reason
      let* ss = aux ss in
      *)
      aux ss >>= fun ss ->
      Ok (s :: ss)
    end
  in
  let v = (aux policy.v : (P.top_stmt list, _ error) result
    :> (P.top_stmt list, [> _ error]) result)
  in
  v >>= fun v -> Ok { policy with v }
