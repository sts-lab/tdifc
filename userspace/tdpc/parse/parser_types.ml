exception Parse_error of Lexing.position * Lexing.position * string

type check_error = [
  | `ParserCheckError of Lexing.position * Lexing.position * string
  ]
