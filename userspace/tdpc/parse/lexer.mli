type error = [
  | `LexerError of Lexing.position * Lexing.position * string
  ]

(** [token buf] the next token in [buf]. Note that [Sedlexing]'s (and OCaml's
    [Lexing]'s) [lexbuf] is impure by design. *)
val token: Sedlexing.lexbuf -> (M.token, [> error]) result
