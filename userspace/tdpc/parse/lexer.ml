module S = Sedlexing

type error = [
  | `LexerError of Lexing.position * Lexing.position * string
  ]

type 'a res = 'a constraint 'a = (M.token, [> error]) result

let line_comment = [%sedlex.regexp? '#', Star (Compl '\n'), '\n']
let str = [%sedlex.regexp? '"', Star (Compl '"' | "\\\""), '"']
let qstr = [%sedlex.regexp? '\'', Star (Compl '\''), '\'']

(** [add_lexeme buf acc] adds the lexeme in [buf] to [acc]. *)
let add_lexeme (buf : S.lexbuf) (acc : Buffer.t) : unit =
  Array.iter (fun u -> Buffer.add_utf_8_uchar acc u) (S.lexeme buf)

(** [lexeme_error msg] returns an Error with [msg] at the current position. *)
let lexeme_error (buf : S.lexbuf) (msg : string) : _ res =
  let p0, p1 = S.lexing_positions buf in
  Error (`LexerError (p0, p1, msg))

(** [extract_str buf] extracts the actual double-quote string in [buf],
    handling any escapes as necessary. *)
let extract_str (buf : S.lexbuf) : _ res =
  (* copy buf, to avoid messing up its positions *)
  let buf' = S.from_uchar_array @@ S.lexeme buf in
  let p0, _ = S.lexing_positions buf in
  S.set_position buf' p0;
  S.set_filename buf' p0.pos_fname;
  let buf = buf' in
  ignore @@ S.next buf; (* skip opening quote *)
  let rec aux (acc : Buffer.t) : _ res =
    match%sedlex buf with
    | '"' -> Ok (M.STRING (Buffer.contents acc))
    | '\\' -> escape acc
    | any -> add_lexeme buf acc; aux acc
    | _ -> assert false
  and escape (acc : Buffer.t) : _ res =
    let p0, _ = S.lexing_positions buf in
    match%sedlex buf with
    | 'a' -> Buffer.add_char acc '\x07'; aux acc
    | 'b' -> Buffer.add_char acc '\b'; aux acc
    | 'e' -> Buffer.add_char acc '\x1b'; aux acc
    | 'f' -> Buffer.add_char acc '\x0c'; aux acc
    | 'n' -> Buffer.add_char acc '\n'; aux acc
    | 'r' -> Buffer.add_char acc '\r'; aux acc
    | 't' -> Buffer.add_char acc '\t'; aux acc
    | 'v' -> Buffer.add_char acc '\x0b'; aux acc
    | 'x', Rep (ascii_hex_digit, 1 .. 2) -> begin
      (* ignore wide digits, since they complicate this *)
      let hexdigits = S.Latin1.sub_lexeme buf 1 (S.lexeme_length buf - 1) in
      let hs = "0x" ^ hexdigits in
      try
        Buffer.add_utf_8_uchar acc (Uchar.of_int @@ int_of_string hs);
        aux acc
      with
      | _ -> begin
        let _, p1 = S.lexing_positions buf in
        Error (`LexerError (p0, p1, "invalid hex escape: " ^ hexdigits))
      end
    end
    | Rep ('0' .. '7', 1 .. 3) -> begin
      let octdigits = S.Latin1.lexeme buf in
      let os = "0o" ^ octdigits in
      try
        Buffer.add_utf_8_uchar acc (Uchar.of_int @@ int_of_string os);
        aux acc
      with
      | _ -> begin
        let _, p1 = S.lexing_positions buf in
        Error (`LexerError (p0, p1, "invalid octal escape: " ^ octdigits))
      end
    end
    | _ -> Buffer.add_char acc '\\'; add_lexeme buf acc; aux acc
  in
  aux (Buffer.create 80)

let rec token buf =
  match%sedlex buf with
  | white_space -> token buf
  | line_comment -> token buf
  | str -> extract_str buf
  | qstr -> begin
    let b = Buffer.create 80 in
    Array.iter (fun u -> Buffer.add_utf_8_uchar b u)
      (S.sub_lexeme buf 1 (S.lexeme_length buf - 2));
    Ok (M.STRING (Buffer.contents b))
  end
  | "/*" -> lex_block_comment buf
  | "id" -> Ok M.ID
  | "include" -> Ok M.INCLUDE
  | "namespace" -> Ok M.NAMESPACE
  | "unique" -> Ok M.UNIQUE
  | "logfile" -> Ok M.LOGFILE
  | "max_process_label" -> Ok M.MAX_PROCESS
  | "max_socket_label" -> Ok M.MAX_SOCKET
  | "stdout" -> Ok M.STDOUT
  | "stderr" -> Ok M.STDERR
  | "process" -> Ok M.PROCESS
  | "self" -> Ok M.SELF
  | "parent" -> Ok M.PARENT
  | "children" -> Ok M.CHILDREN
  | "match" -> Ok M.MATCH
  | "init" -> Ok M.INIT
  | '{' -> Ok M.LBRACE
  | '}' -> Ok M.RBRACE
  | '(' -> Ok M.LPAREN
  | ')' -> Ok M.RPAREN
  | '<' -> Ok M.LANGLE
  | Plus ('0' .. '9') -> Ok (M.INT (int_of_string (S.Latin1.lexeme buf)))
  | '>' -> Ok M.RANGLE
  | '+' -> Ok M.PLUS
  | '-' -> Ok M.MINUS
  | '*' -> Ok M.STAR
  | "empty" -> Ok M.EMPTY
  | ';' -> Ok M.SEMICOLON
  | "settags" -> Ok M.SETTAGS
  | "addtags" -> Ok M.ADDTAGS
  | "deltags" -> Ok M.DELTAGS
  | "setcaps" -> Ok M.SETCAPS
  | "addcaps" -> Ok M.ADDCAPS
  | "delcaps" -> Ok M.DELCAPS
  | "dropcaps" -> Ok M.DROPCAPS
  | "tag" -> Ok M.TAG
  | eof -> Ok M.EOF
  | '\'' -> lexeme_error buf "unmatched '"
  | '"' -> lexeme_error buf "unmatched \""
  | _ -> lexeme_error buf "unrecognized input"

(** [lex_block_comment buf] skips past a block comment in [buf] and returns
    either an error or the next token. *)
and lex_block_comment (buf : S.lexbuf) : _ res =
  let p0, _ = S.lexing_positions buf in
  let rec aux buf =
    match%sedlex buf with
    | "*/" -> token buf
    (* require closing delimiter always; may help to catch bugs *)
    | eof -> begin
      let _, p1 = S.lexing_positions buf in
      Error (`LexerError (p0, p1, "unmatched /*"))
    end
    | any -> aux buf
    | _ -> assert false
  in aux buf
