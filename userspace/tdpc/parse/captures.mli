(** Checks that regex captures are valid. *)

type error = [
  | Parser_types.check_error
  ]

(** [check policy] walks [policy] and ensures each capture refers to a valid
    index in the corresponding regex. If [Ok], the returned policy is
    unmodified. *)
val check : Ast.Parsed.policy -> (Ast.Parsed.policy, [> error]) result
