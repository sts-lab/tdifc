%token ID
%token INCLUDE
%token NAMESPACE UNIQUE
%token LOGFILE STDOUT STDERR
%token MAX_SOCKET MAX_PROCESS
%token MATCH INIT
%token PROCESS SELF PARENT CHILDREN
%token LBRACE RBRACE
%token PLUS MINUS STAR EMPTY SEMICOLON
%token SETTAGS ADDTAGS DELTAGS SETCAPS ADDCAPS DELCAPS DROPCAPS
%token TAG LPAREN RPAREN LANGLE RANGLE
%token <string> STRING
%token <int> INT
%token EOF

%start <Ast.Parsed.policy> policy

%{ module A = Ast.Parsed %}

%%

%inline node(X):
| x = X
  { A.{ v = x; p0 = $startpos; p1 = $endpos } }

%inline u32:
| i = INT
  { match Util.opt_uint32 i with
    | Some i -> i
    | None -> begin
      let msg = Printf.sprintf
        "error: %u is not an unsigned 32-bit integer" i
      in
      raise @@ Parser_types.Parse_error ($startpos, $endpos, msg)
    end
  }

%inline _id_stmt:
| ID i = node(u32)
  { A.PolicyID i }

id_stmt:
| x = node(_id_stmt)
  { x }

%inline _logfile:
| STDOUT
  { A.Fd 1l }
| STDERR
  { A.Fd 2l }
| path = STRING
  { A.File path }

%inline _logfile_stmt:
| LOGFILE x = node(_logfile)
  { A.Logfile x }

logfile_stmt:
| x = node(_logfile_stmt)
  { x }

%inline _namespace:
| UNIQUE
  { A.Unique }
| i = u32
  { A.Shared i }

%inline _namespace_stmt:
| NAMESPACE x = node(_namespace)
  { A.Namespace x }

namespace_stmt:
| x = node(_namespace_stmt)
  { x }

%inline _flow_policy:
| MAX_PROCESS i = u32
  { A.MaxProcess i }
| MAX_SOCKET i = u32
  { A.MaxSocket i }

%inline _flow_policy_stmt:
| x = node(_flow_policy)
  { A.FlowPolicy x }

flow_policy_stmt:
| x = node(_flow_policy_stmt)
  { x }

%inline _tag_frag:
| LANGLE i = INT RANGLE
  { A.Capture i }
| s = STRING
  { A.String s }

%inline _tag:
| TAG LPAREN fs = node(_tag_frag)+ RPAREN
  { fs }

tag:
| x = node(_tag)
  { x }

%inline _capability:
| PLUS
  { A.Plus }
| MINUS
  { A.Minus }

capability:
| x = node(_capability)
  { x }

tag_with_capabilities:
| cs = capability* t = tag
  { (cs, t) }

tag_with_capabilities_list:
| EMPTY
  { [] }
| cts = tag_with_capabilities*
  { cts }

%inline _stmt:
| ADDTAGS cts = tag_with_capabilities_list SEMICOLON
  { A.Addtags cts }
| DELTAGS ts = tag+ SEMICOLON
  { A.Deltags ts }
| DELTAGS STAR SEMICOLON
  { A.Settags [] }
| SETTAGS cts = tag_with_capabilities_list SEMICOLON
  { A.Settags cts }
| SETCAPS cts = tag_with_capabilities_list SEMICOLON
  { A.Setcaps cts }
| ADDCAPS cts = tag_with_capabilities_list SEMICOLON
  { A.Addcaps cts }
| DELCAPS cts = tag_with_capabilities_list SEMICOLON
  { A.Delcaps cts }
| DROPCAPS STAR SEMICOLON
  { A.Dropcaps }

stmt:
| x = node(_stmt)
  { x }

%inline _pid:
| SELF
  { A.Self }
| PARENT
  { A.Parent }
| CHILDREN
  { A.Children }
| LANGLE i = INT RANGLE
  { A.CapturePID i }
| i = u32
  { A.Literal i }

pid:
| x = node(_pid)
  { x }

%inline _process_block:
| PROCESS pids = pid+ LBRACE stmts = stmt+ RBRACE
  { (pids, stmts) }

process_block:
| x = node(_process_block)
  { x }

%inline _regex:
| pattern = STRING
  { match Regex.parse pattern false with
    (* it'd be nice to use result instead of exceptions, but we really want
       execution to stop HERE and raise a syntax error, not to continue.
       rather than inspecting each reduction step, this seems easier. *)
    | Error _ -> begin
      let msg = Printf.sprintf "invalid regex \"%s\"" pattern in
      raise @@ Parser_types.Parse_error ($startpos, $endpos, msg)
    end
    | Ok regex -> regex
  }

regex:
| x = node(_regex)
  { x }

%inline _block:
| MATCH regex = regex LBRACE blocks = process_block+ RBRACE
  { A.Match (regex, blocks) }
| INIT LBRACE blocks = process_block+ RBRACE
  { A.Init blocks }

block:
| x = node(_block)
  { x }

%inline _include:
| INCLUDE path = STRING
  { A.Include path }

include_stmt:
| x = node(_include)
  { x }

top_stmt:
| stmt = id_stmt SEMICOLON
| stmt = include_stmt SEMICOLON
| stmt = logfile_stmt SEMICOLON
| stmt = flow_policy_stmt SEMICOLON
| stmt = namespace_stmt SEMICOLON
| stmt = block
  { stmt }

policy:
| stmts = node(top_stmt*) EOF
  { stmts }
