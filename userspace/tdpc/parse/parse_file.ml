module I = M.MenhirInterpreter
module S = Sedlexing

let ( >>= ) = Result.bind

type error = [
  | Lexer.error
  | `ParserError of Lexing.position * Lexing.position * string
  | Parser_types.check_error
  ]

(** [loop lexbuf checkpoint] pulls tokens from [lexbuf] to drive the parser in
    module [M]. *)
let rec loop (lexbuf : S.lexbuf)
    (checkpoint : Ast.Parsed.policy I.checkpoint)
    : (Ast.Parsed.policy, [> error]) result =
  match checkpoint with
  | I.InputNeeded _ -> begin
    match Lexer.token lexbuf with
    | Ok t -> begin
      let p0, p1 = S.lexing_positions lexbuf in
      loop lexbuf (I.offer checkpoint (t, p0, p1))
    end
    | Error _ as e -> e
  end
  | I.Shifting _ | I.AboutToReduce _ -> loop lexbuf (I.resume checkpoint)
  | I.Rejected | I.HandlingError _ -> begin
    let p0, p1 = S.lexing_positions lexbuf in
    let b = Buffer.create 32 in
    Buffer.add_string b "unexpected token \"";
    Array.iter (fun u -> Buffer.add_utf_8_uchar b u) (S.lexeme lexbuf);
    Buffer.add_char b '"';
    Error (`ParserError (p0, p1, Buffer.contents b))
  end
  | I.Accepted p -> Ok p

(** [parse_in input name] parses an in_channel [input] named [name]. *)
let parse_in (input : in_channel) (name : string)
    : (Ast.Parsed.policy, [> error]) result =
  let lexbuf = S.Utf8.from_channel input in
  S.set_filename lexbuf name;
  let p0, _ = S.lexing_positions lexbuf in
  (* yes, it's really exceptions and results used together =( *)
  try
    loop lexbuf (M.Incremental.policy p0)
  with
  | Parser_types.Parse_error (p0, p1, s) -> Error (`ParserError (p0, p1, s))

let parse path =
  let input = open_in path in
  let p = parse_in input path in
  close_in_noerr input;
  p >>= Captures.check
