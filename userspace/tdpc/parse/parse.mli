(** DP file parser frontend. *)

type 'a error = [
  | Parse_file.error
  | 'a Include.error
  ]

(** [parse path] completely parses a policy, including any included files,
    from a UTF-8 encoded file at [path]. *)
val parse : string -> (Ast.Parsed.policy, [> _ error]) result
