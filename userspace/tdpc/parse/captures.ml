module P = Ast.Parsed

(* dune doesn't like let* for some reason
let ( let* ) = Result.bind
*)
let ( >>= ) = Result.bind

type error = [
  | Parser_types.check_error
  ]

let error_msg_match (i : int) : string =
  Printf.sprintf "capture index %d is out of range" i

let error_msg_init : string =
  "cannot use captures in init block"

(** [check_tag r t] returns [Error `ParserCheckError] if [t] has an
    invalid capture for [r], or Ok () otherwise. *)
let check_tag (r : Regex.t) (t : P.tag) : (unit, [> error]) result =
  let ncg = Regex.Fields.ncg r in
  let rec aux = function
    | [] -> Ok ()
    | P.{ v = Capture i; p0; p1 } :: _ when i <= 0 || i > ncg -> begin
      Error (`ParserCheckError (p0, p1, error_msg_match i))
    end
    | _ :: fs -> aux fs
  in
  aux t.v

(** [check_pids r ps] returns [Error `ParserCheckError] if [ps] has an invalid
    capture for [r], or Ok () otherwise. *)
let rec check_pids (r : Regex.t) (ps : P.pid list)
    : (unit, [> error]) result =
  let ncg = Regex.Fields.ncg r in
  match ps with
  | [] -> Ok ()
  | P.{ v = CapturePID i; p0; p1 } :: _ when i <= 0 || i > ncg -> begin
      Error (`ParserCheckError (p0, p1, error_msg_match i))
  end
  | _ :: ps -> check_pids r ps

(** [check_match' r ss] returns [Error `ParserCheckError] if at least one
    tag in [ss] has an invalid capture for [r], or Ok () otherwise. *)
let rec check_match' (r : Regex.t) (ss : P.stmt list)
    : (unit, [> error]) result =
  match ss with
  | [] -> Ok ()
  | (P.{ v = Addtags ((_, t) :: tl); _ } as s) :: ss
  | (P.{ v = Settags ((_, t) :: tl); _ } as s) :: ss
  | (P.{ v = Setcaps ((_, t) :: tl); _ } as s) :: ss -> begin
    (* dune doesn't like let* for some reason
    let* () = check_tag r t in
    *)
    check_tag r t >>= fun () ->
    check_match' r (P.{ s with v = Addtags tl } :: ss)
  end
  | (P.{ v = Deltags (t :: tl); _ } as s) :: ss -> begin
    (* dune doesn't like let* for some reason
    let* () = check_tag r t in
    *)
    check_tag r t >>= fun () ->
    check_match' r (P.{ s with v = Deltags tl } :: ss)
  end
  | _ :: ss -> check_match' r ss

(** [check_match r bs] is [check_match'], but for block lists instead. *)
let rec check_match r bs =
  match bs with
  | [] -> Ok ()
  | P.{ v = (ps, ss); _ } :: bs -> begin
    (* dune doesn't like let* for some reason
    let* () = check_pids r ps in
    let* () = check_match' r ss in
    *)
    check_pids r ps >>= fun () ->
    check_match' r ss >>= fun () ->
    check_match r bs
  end

let check_tag_init (t : P.tag) : (unit, [> error]) result =
  let rec aux = function
    | [] -> Ok ()
    | P.{ v = Capture _; p0; p1 } :: _ -> begin
      Error (`ParserCheckError (p0, p1, error_msg_init))
    end
    | _ :: fs -> aux fs
  in
  aux t.v

(** [check_init' ss] returns [Error `ParserCheckError] if there is any capture
    group at all in [ss], or Ok () otherwise. *)
let rec check_init' (ss : P.stmt list) : (unit, [> error]) result =
  match ss with
  | [] -> Ok ()
  | (P.{ v = Addtags ((_, t) :: tl); _ } as s) :: ss
  | (P.{ v = Settags ((_, t) :: tl); _ } as s) :: ss
  | (P.{ v = Setcaps ((_, t) :: tl); _ } as s) :: ss -> begin
    (* dune doesn't like let* for some reason
    let* () = check_tag_init t in
    *)
    check_tag_init t >>= fun () ->
    check_init' (P.{ s with v = Addtags tl } :: ss)
  end
  | (P.{ v = Deltags (t :: tl); _ } as s) :: ss -> begin
    (* dune doesn't like let* for some reason
    let* () = check_tag_init t in
    *)
    check_tag_init t >>= fun () ->
    check_init' (P.{ s with v = Deltags tl } :: ss)
  end
  | _ :: ss -> check_init' ss

(** [check_init_pids ps] ensures there are no CapturePIDs in [ps]. *)
let rec check_init_pids (ps : P.pid list) : (unit, [> error]) result =
  match ps with
  | [] -> Ok ()
  | P.{ v = CapturePID _; p0; p1 } :: _ -> begin
    Error (`ParserCheckError (p0, p1, error_msg_init))
  end
  | _ :: ps -> check_init_pids ps

let rec check_init bs =
  match bs with
  | [] -> Ok ()
  | P.{ v = (ps, ss); _ } :: bs -> begin
    (* dune doesn't like let* for some reason
    let* () = check_init_ps ps in
    let* () = check_init' ss in
    *)
    check_init_pids ps >>= fun () ->
    check_init' ss >>= fun () ->
    check_init bs
  end

let check policy =
  let rec aux = function
    | [] -> Ok policy
    | P.{ v = Match ({ v = r; _ }, bs); _ } :: tl -> begin
      match check_match r bs with
      | Ok () -> aux tl
      | Error _ as e -> e
    end
    | P.{ v = Init bs; _ } :: tl -> begin
      match check_init bs with
      | Ok () -> aux tl
      | Error _ as e -> e
    end
    | _ :: tl -> aux tl
  in
  aux P.(policy.v)
