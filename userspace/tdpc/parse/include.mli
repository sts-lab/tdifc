(** Recursively parses included policies. *)

type 'a error = 'a constraint 'a = [
  (* list of include positions + underlying error *)
  | `ParserIncludeError of (Lexing.position * Lexing.position) list
                           * [> Parse_file.error]
  ]

(** [resolve_includes policy] walks through [policy] and recursively parses
    included files, inserting the parsed policies at the [Include] node, until
    no Ast.Include nodes remain.

    A particular path can only be included once. Note that it is possible for
    multiple paths to refer to the same file, so this doesn't necessarily
    guarantee that a single file is only ever included once. *)
val resolve_includes :
    Ast.Parsed.policy -> (Ast.Parsed.policy, [> _ error]) result
