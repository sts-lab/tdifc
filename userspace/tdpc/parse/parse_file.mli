(** Parser for .dp files. They are expected to be UTF-8 encoded. *)

type error = [
  | Lexer.error
  | `ParserError of Lexing.position * Lexing.position * string
  | Parser_types.check_error
  ]

(** [parse path] parses a single file, but doesn't recurse on includes. *)
val parse : string -> (Ast.Parsed.policy, [> error]) result
