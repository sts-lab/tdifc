(** This module contains human-readable pretty-printers for a DBP AST. *)

val pp_namespace : Format.formatter -> Ast.Dbp.namespace -> unit

val pp_logfile : Format.formatter -> Ast.Dbp.logfile -> unit

val pp_flow_policy : Format.formatter -> Ast.Dbp.flow_policy -> unit

val pp_tag_frag : Format.formatter -> Ast.Dbp.tag_frag -> unit

val pp_capability : Format.formatter -> Ast.Dbp.capability -> unit

val pp_tag : Format.formatter -> Ast.Dbp.tag -> unit

val pp_stmt : Format.formatter -> Ast.Dbp.stmt -> unit

val pp_policy : Format.formatter -> Ast.Dbp.policy -> unit
