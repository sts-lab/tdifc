open Ast.Dbp

module F = Format

let pp_namespace fmt ns =
  F.fprintf fmt "@[<hov 1>(namespace@ @[<h>(";
  let () =
    match ns with
    | Unique -> F.fprintf fmt "unique"
    | Shared i -> F.fprintf fmt "shared@ %lu" i
  in
  F.fprintf fmt ")@])@]"

let pp_logfile fmt log =
  F.fprintf fmt "@[<hov 1>(logfile@ @[<h>(";
  let () =
    match log with
    | Fd 1l -> F.fprintf fmt "stdout"
    | Fd 2l -> F.fprintf fmt "stderr"
    | Fd i -> F.fprintf fmt "fd@ %ld" i
    | File p -> F.fprintf fmt "path@ %s" p
  in
  F.fprintf fmt ")@])@]"

let pp_flow_policy fmt log =
  F.fprintf fmt "@[<hov 1>(flow_policy@ @[<h>(";
  let () =
    match log with
    | MaxProcess i -> F.fprintf fmt "process@ %ld" i
    | MaxSocket i -> F.fprintf fmt "socket@ %ld" i
  in
  F.fprintf fmt ")@])@]"

let pp_tag_frag fmt frag =
  F.fprintf fmt "@[<hov 1>(";
  let () =
    match frag with
    | String s -> F.fprintf fmt "string@ %s" s
    | Capture i -> F.fprintf fmt "capture@ %d" i
  in
   F.fprintf fmt ")@]"

let pp_capability fmt cap =
  match cap with
  | Plus -> F.fprintf fmt "(+)"
  | Minus -> F.fprintf fmt "(-)"

let pp_tag fmt tag =
  F.fprintf fmt "@[<hov 1>(tag";
  List.iter (fun frag -> Format.fprintf fmt "@ %a" pp_tag_frag frag) tag;
  F.fprintf fmt ")@]"

let pp_cap_tag fmt (cs, t) =
  F.fprintf fmt "@[<h>(";
  List.iter (fun c -> F.fprintf fmt "%a@ " pp_capability c) cs;
  F.fprintf fmt "%a)@]" pp_tag t

let pp_cap_tag_list fmt l =
  F.fprintf fmt "@[<hov>(";
  List.iteri (fun i t ->
    if i = 0 then F.fprintf fmt "%a" pp_cap_tag t
    else F.fprintf fmt "@ %a" pp_cap_tag t) l;
  F.fprintf fmt ")@]"

let pp_stmt fmt s =
  F.fprintf fmt "@[<hov 1>(";
  let () =
    match s with
    | Addtags l -> F.fprintf fmt "addtags@ %a" pp_cap_tag_list l
    | Deltags l -> begin
      F.fprintf fmt "deltags@ %a"
        (fun fmt l -> List.iter (fun t -> pp_tag fmt t) l) l
    end
    | Settags l -> F.fprintf fmt "settags@ %a" pp_cap_tag_list l
    | Addcaps l -> F.fprintf fmt "Addcaps@ %a" pp_cap_tag_list l
    | Delcaps l -> F.fprintf fmt "Delcaps@ %a" pp_cap_tag_list l
    | Setcaps l -> F.fprintf fmt "Setcaps@ %a" pp_cap_tag_list l
    | Dropcaps -> F.fprintf fmt "dropcaps"
  in
  F.fprintf fmt ")@]"

let pp_pid fmt p =
  F.fprintf fmt "@[<h>(";
  let () =
    match p with
    | Self -> F.fprintf fmt "self"
    | Parent -> F.fprintf fmt "parent"
    | Children -> F.fprintf fmt "children"
    | CapturePID i -> F.fprintf fmt "capture@ %d" i
    | Literal i -> F.fprintf fmt "literal@ %lu" i
  in
  F.fprintf fmt ")@]"

let pp_block fmt (ps, ss) =
  F.fprintf fmt "@[<hov 1>(@[<hov 1>(pid";
  List.iter (fun pid -> F.fprintf fmt "@ %a" pp_pid pid) ps;
  F.fprintf fmt ")@]@ @[<hov 1>(";
  List.iteri (fun i s -> if i <> 0 then F.fprintf fmt "@ "; pp_stmt fmt s) ss;
  F.fprintf fmt ")@])@]"

let pp_policy fmt p =
  F.fprintf fmt "@[<v 1>(@[<h>(id@ %lu)@]@ %a@ " p.id pp_namespace p.ns;
  List.iter (fun fp -> F.fprintf fmt "%a@ " pp_flow_policy fp) p.flow_policies;
  List.iter (fun log -> F.fprintf fmt "%a@ " pp_logfile log) p.logfiles;

  F.fprintf fmt "@[<v 1>(init";
  List.iter (fun b -> F.fprintf fmt "@ %a" pp_block b) p.init_blocks;
  F.fprintf fmt ")@]";

  List.iter (fun (_, bs) ->
    F.fprintf fmt "@ @[<v 1>(@[<h>match@ (<abstr>)@]";
    List.iter (fun b -> F.fprintf fmt "@ %a" pp_block b) bs;
    F.fprintf fmt ")@]") p.match_blocks;
  F.fprintf fmt ")@]"
