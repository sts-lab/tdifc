val dbp_version : int (* output DBP version *)

type error = [
  | `SerializeDBPError of string (* message only *)
  ]

(** [print fmt p] prints [p] in DBP (DIFC Binary Policy) format, suitable for
    reading by the kernel. *)
val print : Format.formatter -> Ast.Dbp.policy -> (unit, [> error]) result
