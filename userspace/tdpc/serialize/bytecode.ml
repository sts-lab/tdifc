open Ast.Dbp

type error = [
  | `SerializeDBPError of string (* message only *)
  ]

type 'a res = 'a constraint 'a = (unit, [> error]) result

let dbp_version = 1

let dbp_log_loc = 1
let dbp_namespace = 2
let dbp_match = 3
let dbp_init = 4
let dbp_flow_policy = 5

let dbp_flow_socket = 1
let dbp_flow_process = 2

let dbp_pid_self = 1
let dbp_pid_parent = 2
let dbp_pid_children = 3
let dbp_pid_capture = 4
let dbp_pid_literal = 5

let dbp_stmt_add_tags = 1
let dbp_stmt_del_tags = 2
let dbp_stmt_set_tags = 3
let dbp_stmt_add_caps = 4
let dbp_stmt_del_caps = 5
let dbp_stmt_set_caps = 6
let dbp_stmt_drop_caps = 7

let dbp_log_fd = 1
let dbp_log_path = 2

let dbp_ns_unique = 1
let dbp_ns_normal = 2

let dbp_cap_plus = 0x1l
let dbp_cap_minus = 0x2l
(*
let dbp_cap_unmask = 0x4
let dbp_cap_mutable = 0x8
*)

let dbp_frag_capture = 1
let dbp_frag_literal = 2

let ( let* ) = Result.bind

(** [fold_res f l] applies [f] to every element in [l] until either the list
    is empty or an [Error] is returned. *)
let rec fold_res (f : 'a -> _ res) (l : 'a list) : _ res =
  match l with
  | [] -> Ok ()
  | h :: t -> let* () = f h in fold_res f t

(** [add_header buf p] adds the DBP header for [p] to [buf]. *)
let add_header (buf : Buffer.t) (p : policy) : _ res =
  Buffer.add_uint8 buf dbp_version;
  Buffer.add_int32_be buf p.id;
  Ok ()

(** [add_logfile buf log] adds [log] in DBP format to [buf]. *)
let add_logfile (buf : Buffer.t) (log : logfile) : _ res =
  match log with
  | Fd i -> begin
    Buffer.add_uint8 buf dbp_log_fd;
    Buffer.add_int32_be buf i;
    Ok ()
  end
  | File path -> begin
    Buffer.add_uint8 buf dbp_log_path;
    Util.add_c_string buf path;
    Ok ()
  end

(** [add_logfile buf l] adds [l] in DBP format to [buf]. *)
let add_logfiles (buf : Buffer.t) (l : logfile list) : _ res =
  Buffer.add_uint8 buf dbp_log_loc;
  Buffer.add_int32_be buf (Int32.of_int @@ List.length l);
  fold_res (add_logfile buf) l

(** [add_namespace buf ns] appends [ns] in DBP format to [buf]. *)
let add_namespace (buf : Buffer.t) (ns : namespace) : _ res =
  Buffer.add_uint8 buf dbp_namespace;
  match ns with
  | Unique -> Buffer.add_uint8 buf dbp_ns_unique; Ok ()
  | Shared i -> begin
    Buffer.add_uint8 buf dbp_ns_normal;
    Buffer.add_int32_be buf i;
    Ok ()
  end

(** [add_flow_policy buf fp] appends [fp] in DBP format to [buf]. *)
let add_flow_policy (buf : Buffer.t) (fp : flow_policy) : _ res =
  match fp with
  | MaxProcess i -> begin
    Buffer.add_uint8 buf dbp_flow_process;
    Buffer.add_int32_be buf i;
    Ok ()
  end
  | MaxSocket i -> begin
    Buffer.add_uint8 buf dbp_flow_socket;
    Buffer.add_int32_be buf i;
    Ok ()
  end

(** [add_caps buf cs] appends [cs] in DBP format to [buf]. *)
let add_caps (buf : Buffer.t) (cs : capability list) : _ res =
  let add_flag (flags : int32) (c : capability) : int32 =
    match c with
    | Plus -> Int32.logor flags dbp_cap_plus
    | Minus -> Int32.logor flags dbp_cap_minus
  in
  let flags = List.fold_left add_flag 0l cs in
  Buffer.add_int32_be buf flags;
  Ok ()

(** [add_tag buf t] appends [t] in DBP tagvec format to [buf]. *)
let add_tag (buf : Buffer.t) (t : tag) : _ res =
  let add_frag : tag_frag -> unit = function
    | String s -> begin
      Buffer.add_uint8 buf dbp_frag_literal;
      Util.add_c_string buf s
    end
    | Capture i -> begin
      Buffer.add_uint8 buf dbp_frag_capture;
      (* policy language captures are 1-indexed; dbp captures are 0-indexed *)
      Buffer.add_uint8 buf (i - 1)
    end
  in
  Buffer.add_uint8 buf (List.length t);
  List.iter add_frag t;
  Ok ()

(** [add_stmt buf ss] appends [s] in DBP format to [buf]. *)
let add_stmt (buf : Buffer.t) (s : stmt) : _ res =
  let add_stmt_tagvecs (stmt_type : int) (l : (capability list * tag) list)
      : _ res =
    Buffer.add_uint8 buf stmt_type;
    Buffer.add_int32_be buf (Int32.of_int @@ List.length l);
    List.iter (fun (cs, t) ->
      let _ =
        let* () = add_caps buf cs in add_tag buf t
      in ()) l;
    Ok ()
  in
  match s with
  | Addtags l -> add_stmt_tagvecs dbp_stmt_add_tags l
  | Deltags l -> begin
    List.map (fun t -> ([], t)) l
    |> add_stmt_tagvecs dbp_stmt_del_tags
  end
  | Settags l -> add_stmt_tagvecs dbp_stmt_set_tags l
  | Addcaps l -> add_stmt_tagvecs dbp_stmt_add_caps l
  | Delcaps l -> add_stmt_tagvecs dbp_stmt_del_caps l
  | Setcaps l -> add_stmt_tagvecs dbp_stmt_set_caps l
  | Dropcaps -> Buffer.add_uint8 buf dbp_stmt_drop_caps; Ok ()

(** [add_pid buf p] appends [p] in DBP format to [buf]. *)
let add_pid (buf : Buffer.t) (p : pid) : _ res =
  match p with
  | Self -> Buffer.add_uint8 buf dbp_pid_self; Ok ()
  | Parent -> Buffer.add_uint8 buf dbp_pid_parent; Ok ()
  | Children -> Buffer.add_uint8 buf dbp_pid_children; Ok ()
  | CapturePID i -> begin
    Buffer.add_uint8 buf dbp_pid_capture;
    (* policy language captures are 1-indexed; dbp captures are 0-indexed *)
    Buffer.add_uint8 buf (i - 1);
    Ok ()
  end
  | Literal i -> begin
    Buffer.add_uint8 buf dbp_pid_literal;
    Buffer.add_int32_be buf i;
    Ok ()
  end

(** [add_flow_policies buf fps] appends [fps] in DBP format to [buf]. *)
let add_flow_policies (buf : Buffer.t) (fps : flow_policy list) : _ res =
  Buffer.add_uint8 buf dbp_flow_policy;
  Buffer.add_int32_be buf (Int32.of_int @@ List.length fps);
  fold_res (add_flow_policy buf) fps

(** [add_block buf b] adds [b] in DBP format to [buf]. *)
let add_block (buf : Buffer.t) (ps, ss : block) : _ res =
  Buffer.add_int32_be buf (Int32.of_int @@ List.length ps);
  let* () = fold_res (add_pid buf) ps in
  Buffer.add_int32_be buf (Int32.of_int @@ List.length ss);
  fold_res (add_stmt buf) ss

(** [add_init buf bs] adds init blocks [bs] in DBP format to [buf]. *)
let add_init (buf : Buffer.t) (bs : block list) : _ res =
  if bs = [] then
    Ok ()
  else begin
    Buffer.add_uint8 buf dbp_init;
    Buffer.add_int32_be buf (Int32.of_int @@ List.length bs);
    fold_res (add_block buf) bs
  end

(** [add_match buf (r, bs)] adds [bs] when matching [r] in DBP format to
    [buf]. *)
let add_match (buf : Buffer.t) (r, bs : Regex.t * block list) : _ res =
  let* r' = Regex.serialize r |> Result.map_error @@
    function
    | `RegexSerializeError -> `SerializeDBPError ("failed to serialize regex")
  in
  Buffer.add_bytes buf r';
  Buffer.add_int32_be buf (Int32.of_int @@ List.length bs);
  fold_res (add_block buf) bs

(** [add_matches buf l] adds [l] in DBP format to [buf]. *)
let add_matches (buf : Buffer.t) (l : (Regex.t * block list) list) : _ res =
  Buffer.add_uint8 buf dbp_match;
  Buffer.add_int32_be buf (Int32.of_int @@ List.length l);
  fold_res (add_match buf) l

let print fmt p =
  let buf = Buffer.create 1024 in
  let* () = add_header buf p in
  let* () = add_namespace buf p.ns in
  let* () = add_logfiles buf p.logfiles in
  let* () = add_flow_policies buf p.flow_policies in
  let* () = add_init buf p.init_blocks in
  let* () = add_matches buf p.match_blocks in
  Format.fprintf fmt "%s" @@ Buffer.contents buf;
  Ok ()
