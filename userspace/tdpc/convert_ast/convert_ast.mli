type error = [
  | `ConvertASTError of Lexing.position * Lexing.position * string
  | `ConvertASTMissing of string (* missing an expected statement *)
  ]

(** [to_dbp p] converts parsed AST [p] to a DBP AST.
    Specifically, it merges all init blocks, all regex blocks with the same
    regex, and lowers parsed AST statements to DBP AST statements. *)
val to_dbp : Ast.Parsed.policy -> (Ast.Dbp.policy, [> error]) result
