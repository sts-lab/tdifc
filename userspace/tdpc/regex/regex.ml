module C = Ctypes
module B = Bindings
module T = B.Types
module F = B.Fields

type error = [
  | `RegexParseError
  | `RegexSerializeError
  | `RegexDeserializeError
  ]

type t = T.t

module Fields = struct
  let bufsize regex =
    C.getf regex F.Regex.bufsize |> Unsigned.UInt16.to_int

  let cbufsize regex =
    C.getf regex F.Regex.cbufsize |> Unsigned.UInt16.to_int

  let ncg regex =
    C.getf regex F.Regex.ncg |> Unsigned.UInt8.to_int
end

(** [make_regex ()] allocates a new regex, which is automatically freed once
    it has no more references. *)
let make_regex () : t =
  C.make ~finalise:(fun r -> B.regex_free (C.addr r)) T.regex

let parse pattern verbose =
  let r = make_regex () in
  if B.regex_nparse pattern (Unsigned.Size_t.of_int @@ String.length pattern)
    (C.addr r) verbose
  then Ok r else Error `RegexParseError

let matches regex s =
  let cgs = C.CArray.make T.capture (Fields.ncg regex) in
  if B.regex_match (C.addr regex) s
    (Unsigned.Size_t.of_int @@ String.length s) (C.CArray.start cgs)
  then
    let aux cg l =
      (C.string_from_ptr (C.getf cg F.Capture.start)
        ~length:(Unsigned.Size_t.to_int @@ C.getf cg F.Capture.len)) :: l
    in
    let r = (true, C.CArray.fold_right aux cgs []) in
    r
  else (false, [])

let charpp_to_voidpp =
  C.(coerce (ptr @@ ptr char) (ptr @@ ptr void))

let serialize regex =
  let p = C.(allocate (ptr char) (from_voidp char null)) in
  match Unsigned.Size_t.to_int @@ B.regex_serialize (C.addr regex)
        (charpp_to_voidpp p) with
  | 0 -> Error `RegexSerializeError
  | n -> begin
    let a = C.CArray.from_ptr C.(!@ p) n in
    let buf = Buffer.create n in
    C.CArray.iter (fun c -> Buffer.add_char buf c) a;
    Ok (Buffer.to_bytes buf)
  end

let deserialize bs =
  let r = make_regex () in
  let buf = C.CArray.make C.char (Bytes.length bs) in
  Bytes.iteri (fun i c -> C.CArray.set buf i c) bs;
  if B.regex_deserialize (C.to_voidp @@ C.CArray.start buf) (C.addr r)
  then Ok r else Error `RegexDeserializeError
