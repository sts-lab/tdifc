(** FFI for the C regex.h library (also within this tree). The LSM expects any
    regex to be in regex_deserialize format, so just use the C implementation
    instead of reimplementing regex.h in OCaml.

    The function signatures are slightly adapted from the C versions to avoid
    having out-parameters. *)

(** Regex type. *)
type t

type error = [
  | `RegexParseError
  | `RegexSerializeError
  | `RegexDeserializeError
  ]

(** Accessors for relevant regex fields. *)
module Fields : sig
  val bufsize : t -> int (* number of NFA states *)
  val cbufsize : t -> int (* number of character classes *)
  val ncg : t -> int (* number of capture groups *)
end

(** [parse pattern verbose] attempts to produce a [Regex.t] from [pattern],
    but may return [Error `RegexParseError] if [pattern] is invalid. If
    [verbose] is true, this will print any potential error messages to stderr
    at runtime.

    The C library has two parsing functions, [nparse] and [parse], mostly to
    deal with potential null characters in [pattern]. Since OCaml strings have
    a known length, this actually calls [nparse] under the hood; therefore, it
    is okay to have null characters in [pattern]. *)
val parse : string -> bool -> (t, [> `RegexParseError]) result

(** [matches regex s] returns whether [regex] matches [s]. If true, it also
    returns a list of captured strings (if any) in the order of capture groups
    in [regex]; otherwise, it returns an empty list. *)
val matches : t -> string -> bool * string list

(* TODO use Buffer to build a bytes buffer by using add_*_be; output_bytes *)

(** [serialize regex] serializes [regex] into [Ok bytes] that can be easily
    deserialized back into a [regex], or [Error `RegexSerializeError] if
    serialization fails.

    The serialization of two equal regexes is guaranteed to also be equal. *)
val serialize : t -> (bytes, [> `RegexSerializeError]) result

(** [deserialize bs] deserializes [bs] back into [Ok regex], or [Error
    `RegexDeserializeError] if [bs] is not a valid serialized regex. *)
val deserialize : bytes -> (t, [> `RegexDeserializeError]) result
