/*
 * Regex serializer: parses every regex argument and serializes them to
 * stdout.
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "debug.h"
#include "regex.h"

/**
 * Parse @s and serialize it to stdout.
 */
static void output_regex(const char *s) {
  struct regex regex;
  if (!regex_parse(s, &regex, true)) {
    fnerr("failed to parse %s\n", s);
    return;
  }

  void *buf;
  size_t n = regex_serialize(&regex, &buf);
  if (buf)
    fwrite(buf, 1, n, stdout);
  regex_free(&regex);
  free(buf);
}

int main(int argc, char *argv[]) {
  unsigned int i;
  for (i = 1; i != argc; ++i) {
    output_regex(argv[i]);
  }
  return 0;
}
