/*
 * Regex parser. Full LALR(1) is overkill for regex (which is LL(1)), but
 * adjusting a grammar seems easier than a recursive descent parser.
 */
%define api.pure full
%define parse.trace false

%code top {
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "regex_internal.h"
}

%code requires {
struct range {
  char first;
  char last;
};

union yystype {
  char c;
  uint16_t i;
  struct range rng;
  struct _regex *rgx;
  struct _class *cls;
};

struct yyltype {
  unsigned int pos;
  char c;
};

struct parsectx {
  bool verbose;        /* if true, report errors; silent otherwise */
  /* lexing context */
  unsigned int pos;    /* character position */
  const char *pattern; /* current head of pattern being analyzed */
  const char *end;     /* one character past end of pattern */
  uint16_t ncg;        /* current number of capture groups seen */

  /* parsing context */
  struct regex *regex; /* the actual regex */
  struct _regex *rtl;  /* current tail of used struct _regexes in buffer */
  struct _class *ctl;  /* current tail of used struct _classes in buffer */
};
}

%initial-action {
  @$.pos = 1;
  @$.c = '\0';
}

%code {
#define YYLLOC_DEFAULT(lhs, rhs, n) do { \
  if (n) { \
    (lhs).pos = YYRHSLOC(rhs, 1).pos; \
    (lhs).c = YYRHSLOC(rhs, 1).c; \
  } else { \
    (lhs).pos = YYRHSLOC(rhs, 0).pos; \
    (lhs).c = YYRHSLOC(rhs, 0).c; \
  } \
} while (0)

/** index of undecided next pattern */
#define UNDECIDED ((uint16_t) -1)

struct initstatus {
  enum {
    OK,          /* no error */
    BADCAPTURE,  /* unpaired capture */
    BADCLASS,    /* unpaired class */
    BADESCAPE,   /* invalid escape sequence */
    BUFOVERFLOW, /* bufsize overflowed (regex too big) */
    MALLOC,      /* memory allocation failed */
  } status;
  struct yyltype loc; /* location of error */
};

/**
 * Initialize @ctx for @pattern to be parsed into @r.
 * This must be called before lexing & parsing.
 * Return an initstatus that may denote some parsing error.
 */
static struct initstatus init_context(struct parsectx *ctx,
                                      const char *pattern, size_t n,
                                      struct regex *r, bool verbose);

/**
 * Return a new _class (inverted if @invert) in @ctx.
 */
static struct _class *new_class(struct parsectx *ctx, int invert);

/**
 * Return a new _regex with @type in @ctx.
 */
static struct _regex *new_regex(struct parsectx *ctx, char type);

/**
 * Set @cls so @c is included (or excluded if @invert).
 */
static void class_addc(int invert, char c, struct _class *cls);

/**
 * Set @cls so @r is included (or excluded if @invert).
 */
static void class_addr(int invert, struct range r, struct _class *cls);

/**
 * Simple lexer (tokens are extremely simple; no need for a lexer generator).
 */
static int yylex(union yystype *v, struct yyltype *l, struct parsectx *ctx);

/**
 * Report the error in @s.
 */
static void yyerror(const struct yyltype *l, const struct parsectx *ctx,
                    const char *s);

/**
 * Set undecided nexts in the entire regex @r to point to @dst.
 */
static void set_nexts(const struct parsectx *ctx, struct _regex *r,
                      const struct _regex *dst);

/**
 * If @hd is the current head, make @new the current head instead.
 */
static void update_head(struct parsectx *ctx, struct _regex *hd,
                        struct _regex *new) {
  if (&ctx->regex->buf[ctx->regex->head] == hd)
    ctx->regex->head = new - ctx->regex->buf;
}
}

%define api.value.type {union yystype}
%define api.location.type {struct yyltype}
%define api.token.prefix {TK_}
%param {struct parsectx *ctx}

%token <i>   '<'
%token <c>   CHAR '(' ')' '[' ']' '^' '-' '*' '+' '?' '|' '.' '>'

%type <c>   char cchar_no_ch cchar_no_c cchar_no_h cchar
%type <rng> range_no_c range_no_h range
%type <rgx> one quant seq regex pattern
%type <cls> class classes classes_c classes_r
                  invclasses invclasses_c invclasses_r

%start pattern

%%

cchar_no_ch:
    CHAR
  | '['
  | '*'
  | '+'
  | '?'
  | '|'
  | '.'
  | '('
  | ')'
  | '<' { $$ = '<'; }
  | '>'
  ;

cchar_no_c:
    cchar_no_ch
  | '-'
  ;

cchar_no_h:
    cchar_no_ch
  | '^'
  ;

cchar:
    cchar_no_ch
  | '^'
  | '-'
  ;

range_no_c:
    cchar_no_c[a] '-' cchar[b] { $$.first = $a; $$.last = $b; }
  ;

range_no_h:
    cchar_no_h[a] '-' cchar[b] { $$.first = $a; $$.last = $b; }
  ;

range:
    cchar[a] '-' cchar[b] { $$.first = $a; $$.last = $b; }
  ;

/* ---d-- => ---, d--
 * a--d-- => a--, d--
 * ab-d- => a, b-d, -
 *
 * Note that ab-d-- is not LR(1): at
 *
 *   ab-d- . - (?)
 *
 * the parser can't tell if (?) is ']' (then both - are chars) or anything
 * else (then it is a range).
 *
 * This could be solved with LR(2) or GLR, but this seems rare enough to
 * simply ignore (meaning it is a syntax error).
 *
 * char  => next char/range cannot begin with -
 * range => next char/range can begin with -
 */

/* ends with char */
classes_c:
    cchar_no_c[c] { $$ = new_class(ctx, 0); class_addc(0, $c, $$); }
  | classes_r[cs] cchar[c]      { $$ = $cs; class_addc(0, $c, $cs); }
  | classes_c[cs] cchar_no_h[c] { $$ = $cs; class_addc(0, $c, $cs); }
  ;

/* ends with range */
classes_r:
    range_no_c[r] { $$ = new_class(ctx, 0); class_addr(0, $r, $$); }
  | classes_r[cs] range[r]      { $$ = $cs; class_addr(0, $r, $$); }
  | classes_c[cs] range_no_h[r] { $$ = $cs; class_addr(0, $r, $$); }
  ;

classes:
    /* empty */ { $$ = new_class(ctx, 0); }
  | classes_c
  | classes_r
/*| classes_c[cs] '-'[c] { $$ = $cs; class_addc(0, $c, $$); } /* LR(2) */
/*| classes_r[cs] '-'[c] { $$ = $cs; class_addc(0, $c, $$); } /* LR(2) */
  ;

/* ends with char */
invclasses_c:
    cchar_no_c[c]    { $$ = new_class(ctx, 1); class_addc(1, $c, $$); }
  | invclasses_r[cs] cchar[c]      { $$ = $cs; class_addc(1, $c, $cs); }
  | invclasses_c[cs] cchar_no_h[c] { $$ = $cs; class_addc(1, $c, $cs); }
  ;

/* ends with range */
invclasses_r:
    range_no_c[r]    { $$ = new_class(ctx, 1); class_addr(1, $r, $$); }
  | invclasses_r[cs] range[r]      { $$ = $cs; class_addr(1, $r, $$); }
  | invclasses_c[cs] range_no_h[r] { $$ = $cs; class_addr(1, $r, $$); }
  ;

invclasses:
    /* empty */ { $$ = new_class(ctx, 1); }
  | invclasses_c
  | invclasses_r
/*| invclasses_c[cs] '-'[c] { $$ = $cs; class_addc(0, $c, $$); } /* LR(2) */
/*| invclasses_r[cs] '-'[c] { $$ = $cs; class_addc(0, $c, $$); } /* LR(2) */
  ;

class:
    '[' classes[cs] ']'        { $$ = $cs; }
  | '[' '^' invclasses[cs] ']' { $$ = $cs; }
  ;

char:
    CHAR
  | '^'
  | '-'
  | ']'
  ;

one:
    char[c]  { $$ = new_regex(ctx, LITERAL); $$->c = $c; }
  | class[c] { $$ = new_regex(ctx, CLASS); $$->i = $c - ctx->regex->cbuf; }
  | '.'      { $$ = new_regex(ctx, DOT); }
  | '(' regex[r] ')' { $$ = $r; }
  | '<'[c] regex[r] '>'
    {
      if ($c >= REGEX_MAX_CAPTURES) {
        $$ = $r;
      } else {
        struct _regex *rcap;

        $$ = new_regex(ctx, LCAPTURE);
        $$->i = 2 * $c;
        $$->next = $r - ctx->regex->buf;

        rcap = new_regex(ctx, RCAPTURE);
        rcap->i = 2 * $c + 1;
        set_nexts(ctx, $r, rcap);

        update_head(ctx, $r, $$);
      }
    }
  ;

quant:
    one
  | quant[q] '*'
    /*
     *     ┌─i─>[r]
     *     │     │
     *  ─>[*]<─n─┘
     *     │
     *     └─n─>[UNDECIDED]
     */
    {
      $$ = new_regex(ctx, STAR);
      $$->i = $q - ctx->regex->buf;
      set_nexts(ctx, $q, $$);
      update_head(ctx, $q, $$);
    }
  | quant[q] '+'
    /*
     *     ┌──i───┐
     *     v      │
     *  ─>[r]─n─>[+]─n─>[UNDECIDED]
     */
    {
      struct _regex *plus = new_regex(ctx, PLUS);
      plus->i = $q - ctx->regex->buf;
      set_nexts(ctx, $q, plus);
      $$ = $q;
    }
  | quant[q] '?'
    /*
     *     ┌──i─>[r]─┐
     *  ─>[?]        v
     *     └──n───>[UNDECIDED]
     */
    {
      $$ = new_regex(ctx, QUESTION);
      $$->i = $q - ctx->regex->buf;
      update_head(ctx, $q, $$);
    }
  ;

seq:
    quant
  | seq[s] quant[q]
    {
      $$ = $s;
      set_nexts(ctx, $s, $q);
    }
  ;

regex:
    seq
  | regex[r] '|' seq[s]
    /*
     *     ┌──i─>[r1]─┐
     *  ─>[|]         ├─>[UNDECIDED]
     *     └──n─>[r2]─┘
     */
    {
      $$ = new_regex(ctx, OR);
      $$->i = $r - ctx->regex->buf;
      $$->next = $s - ctx->regex->buf;
      update_head(ctx, $r, $$);
      update_head(ctx, $s, $$);
    }
  ;

pattern:
    regex[r]
    {
      struct _regex *end = new_regex(ctx, ACCEPT);
      $$ = $r;
      set_nexts(ctx, $$, end);
    }
  ;

%%

static struct _class *new_class(struct parsectx *ctx, int invert) {
  struct _class *c = ctx->ctl++;
  int fill = invert ? ~0 : 0;
  memset(c, fill, sizeof(*c));
  return c;
}

static struct _regex *new_regex(struct parsectx *ctx, char type) {
  struct _regex *r = ctx->rtl++;
  r->type = type;
  r->next = UNDECIDED;
  return r;
}

static void class_addc(int invert, char c, struct _class *cls) {
  unsigned char uc = (unsigned char) c;
  unsigned int index = uc / CHAR_BIT, offset = uc % CHAR_BIT;
  if (invert)
    cls->bs[index] &= ~(1 << offset);
  else
    cls->bs[index] |= (1 << offset);
}

static void class_addr(int invert, struct range r, struct _class *cls) {
  char c;
  if (r.last < r.first)
    return;
  for (c = r.first; c != r.last; ++c)
    class_addc(invert, c, cls);
  class_addc(invert, r.last, cls); /* inclusive */
}

#define MAX_OCTAL 3
#define MAX_HEX   2
#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a > b ? b : a)

/**
 * Set @c to the character corresponding to the escape sequence in @s.
 * Return whether this is a valid escape; if invalid, @c is undefined.
 * @end points to the end of the pattern.
 *
 * @s is the string following a backslash representing some escape, and is
 * forwarded to the end of the escape.
 */
static bool escape(const char **s, char *c, const char *end) {
  char buf[MAX(MAX_HEX, MAX_OCTAL) + 1];
  char *p = (char *) *s;
  int i = 0;
  size_t n = end - p;
  if (n == 0) {
    *c = '\\'; /* trailing \ -> literal \ */
    return true;
  }
  ++*s;
  switch (*p) {
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
    /* 1-3 digits */
    n = MIN(n, MAX_OCTAL);
    for (i = 0; i != n; ++i)
      buf[i] = p[i];
    buf[n] = '\0';
    *c = (char) strtol(buf, &p, 8);
    *s += p - buf - 1; /* -1: 1 char already consumed */
    return true;
  case 'x':
    /* 1-2 digits */
    n = MIN(n, MAX_HEX);
    for (i = 0; i != n; ++i)
      buf[i] = p[i + 1]; /* +1: take 2 chars past x */
    buf[n] = '\0';
    *c = (char) strtol(buf, &p, 16);
    if (p - buf == 0) /* no -1: x was consumed */
      return false;
    *s += p - buf;
    return true;
  case 'a':  *c = '\a';   return true;
  case 'b':  *c = '\b';   return true;
  case 'e':  *c = '\x1b'; return true;
  case 'f':  *c = '\f';   return true;
  case 'n':  *c = '\n';   return true;
  case 'r':  *c = '\r';   return true;
  case 't':  *c = '\t';   return true;
  case 'v':  *c = '\v';   return true;
  case '\\': *c = '\\';   return true;
  default:   *c = *p;     return true;
  }
}

/** Set @l based on the current pos and pattern of @ctx. */
static void set_yyl(struct parsectx *ctx, struct yyltype *l) {
  l->pos = ctx->pos;
  l->c = *ctx->pattern;
}

/** Increment pattern and pos of @ctx. */
static void ctx_inc(struct parsectx *ctx) {
  ++ctx->pattern;
  ++ctx->pos;
}

/** Consume one character of @ctx and set @l. */
static void lex_one(struct parsectx *ctx, struct yyltype *l) {
  set_yyl(ctx, l);
  ctx_inc(ctx);
}

/** Consume @c of @ctx (from pattern to @next) and set @l. */
static void lex_seq(struct parsectx *ctx, struct yyltype *l,
    const char *next, char c) {
  l->pos = ctx->pos;
  l->c = c;
  ctx->pos += next - ctx->pattern;
  ctx->pattern = next;
}

static void yyerror(const struct yyltype *l, const struct parsectx *ctx,
    const char *s) {
  if (ctx->verbose)
    fprintf(stderr, "%u:%c:%s\n", l->pos, l->c, s);
}

#define TK_BADESCAPE '\\' /* should be invalid */
static int yylex(union yystype *v, struct yyltype *l, struct parsectx *ctx) {
  const char *next;
  char c;
  if (ctx->pattern == ctx->end) {
    set_yyl(ctx, l);
    return 0;
  }
  c = *ctx->pattern;
  switch (c) {
  case '[':
  case ']':
  case '^':
  case '-':
  case '.':
  case '*':
  case '+':
  case '?':
  case '|':
  case '(':
  case ')':
  case '>':
    v->c = c;
    lex_one(ctx, l);
    return c;
  case '<':
    v->i = ctx->ncg++;
    lex_one(ctx, l);
    return c;
  case '\\':
    next = ctx->pattern + 1;
    if (!escape(&next, &v->c, ctx->end)) {
      set_yyl(ctx, l);
      yyerror(l, ctx, "lex bug: init_context missed bad escape");
      return TK_BADESCAPE;
    }
    lex_seq(ctx, l, next, v->c);
    return TK_CHAR;
  default:
    v->c = c;
    lex_one(ctx, l);
    return TK_CHAR;
  }
}

/**
 * Increment @i and return whether the increment overflows.
 */
static bool inc_overflows(uint16_t *i) {
  if (++*i == 0)
    return true;
  return false;
}

/**
 * Set @is based on @ctx, @pattern, and @status.
 */
static void set_status(struct initstatus *is, struct parsectx *ctx,
                       const char *pattern, int status) {
  is->status = status;
  is->loc.pos = pattern - ctx->pattern + 1; /* not incremented yet */
  is->loc.c = *pattern;
}

static struct initstatus init_context(struct parsectx *ctx,
                                      const char *pattern, size_t n,
                                      struct regex *r, bool verbose) {
  unsigned int in_class = 0, nl = 0, nr = 0, ncg_l = 0, ncg_r = 0;
  struct initstatus ret;

  ctx->pos = 1;
  ctx->pattern = pattern;
  ctx->end = pattern + n;
  ctx->regex = r;
  ctx->ncg = 0;
  ctx->verbose = verbose;

  r->head = 0;
  r->bufsize = 1; /* extra space for END */
  r->cbufsize = 0;
  r->ncg = 0;

  /* preemptively calculate tight buffer sizes */
  for (; pattern != ctx->end; ++pattern) {
    if (!in_class) {
      char tmp;
      switch (*pattern) {
      case '<':
        ++nl;
        if (ncg_l < REGEX_MAX_CAPTURES) {
          ++ncg_l;
          if (inc_overflows(&r->bufsize)) {
            set_status(&ret, ctx, pattern, BUFOVERFLOW);
            goto error_out;
          }
        }
        break;
      case '>':
        ++nr;
        if (nr > nl) {
          set_status(&ret, ctx, pattern, BADCAPTURE);
          goto error_out;
        }
        if (ncg_r < REGEX_MAX_CAPTURES) {
          ++ncg_r;
          if (inc_overflows(&r->bufsize)) {
            set_status(&ret, ctx, pattern, BUFOVERFLOW);
            goto error_out;
          }
        }
        break;
      case '(':
      case ')':
        break;
      case '[':
        in_class = 1;
        ++r->cbufsize;
        if (inc_overflows(&r->bufsize)) {
          set_status(&ret, ctx, pattern, BUFOVERFLOW);
          goto error_out;
        }
        break;
      case '\\':
        ++pattern; /* pass the backslash and escape */
        if (!escape(&pattern, &tmp, ctx->end)) {
          set_status(&ret, ctx, pattern, BADESCAPE);
          goto error_out;
        }
        --pattern; /* for loop will increment, but pattern is already right */
        if (inc_overflows(&r->bufsize)) {
          set_status(&ret, ctx, pattern, BUFOVERFLOW);
          goto error_out;
        }
        break;
      default:
        if (inc_overflows(&r->bufsize)) {
          set_status(&ret, ctx, pattern, BUFOVERFLOW);
          goto error_out;
        }
      }
    } else if (*pattern == ']' && pattern[-1] != '\\') {
      in_class = 0;
    } else if (*pattern == '\\') {
      char tmp;
      ++pattern;
      if (!escape(&pattern, &tmp, ctx->end)) {
        set_status(&ret, ctx, pattern, BADESCAPE);
        goto error_out;
      }
      --pattern;
    }
  }
  /* obvious errors: incomplete capture, incomplete class */
  if (nl != nr) {
    set_status(&ret, ctx, pattern, BADCAPTURE);
    goto error_out;
  } else if (in_class) {
    set_status(&ret, ctx, pattern, BADCLASS);
    goto error_out;
  }
  r->ncg = ncg_l;

  if (!(r->buf = calloc(r->bufsize, sizeof(*r->buf)))) {
    ret.status = MALLOC;
    ret.loc.pos = -1;
    ret.loc.c = '!';
    goto error_out;
  }

  if (!r->cbufsize)
    r->cbuf = NULL;
  else if (!(r->cbuf = calloc(r->cbufsize, sizeof(*r->cbuf)))) {
    ret.status = MALLOC;
    ret.loc.pos = -1;
    ret.loc.c = '!';
    goto free_buf;
  }

  ctx->rtl = r->buf;
  ctx->ctl = r->cbuf;
  ret.status = OK;
  return ret;

free_buf:
  free(r->buf);
error_out:
  r->buf = NULL;
  r->cbuf = NULL;
  ctx->rtl = NULL;
  ctx->ctl = NULL;
  return ret;
}

static void set_nexts(const struct parsectx *ctx, struct _regex *r,
                      const struct _regex *dst) {
  if (r == dst)
    return;

  switch (r->type) {
  case OR:
  case QUESTION:
    set_nexts(ctx, &ctx->regex->buf[r->i], dst);
  }

  if (r->next == UNDECIDED)
    r->next = dst - ctx->regex->buf;
  else
    set_nexts(ctx, &ctx->regex->buf[r->next], dst);
}

bool regex_nparse(const char *pattern, size_t n, struct regex *regex,
                  bool verbose) {
  struct parsectx ctx;
  uint16_t i;
  struct initstatus is;

  is = init_context(&ctx, pattern, n, regex, verbose);
  switch (is.status) {
  case OK:
    break;
  case BADCAPTURE:
    yyerror(&is.loc, &ctx, "parse failed: unpaired < or >");
    return false;
  case BADCLASS:
    yyerror(&is.loc, &ctx, "parse failed: unpaired [");
    return false;
  case BADESCAPE:
    yyerror(&is.loc, &ctx, "parse failed: invalid escape sequence");
    return false;
  case BUFOVERFLOW:
    yyerror(&is.loc, &ctx, "parse failed: regex is too big");
    return false;
  case MALLOC:
    yyerror(&is.loc, &ctx, "parse failed: out of memory");
    return false;
  }

  if (yyparse(&ctx) != 0) {
    regex_free(regex);
    return false;
  }

  /* confirm tight bufsize: should have no INVALID states */
  for (i = 0; i != regex->bufsize; ++i) {
    if (regex->buf[i].type <= LINVALID || regex->buf[i].type >= HINVALID) {
      is.loc.pos = i;
      is.loc.c = regex->buf[i].type;
      yyerror(&is.loc, &ctx, "BUG: parse bufsize is too big");
      return false;
    }
  }

  return true;
}

bool regex_parse(const char *pattern, struct regex *regex, bool verbose) {
  return regex_nparse(pattern, strlen(pattern), regex, verbose);
}
