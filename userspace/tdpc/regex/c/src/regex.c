/*
 * Implementation is based on:
 * https://swtch.com/~rsc/regexp/regexp1.html
 * https://swtch.com/~rsc/regexp/regexp2.html
 *
 * Structurally closer to the second link, but logically closer to the first.
 */

#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "debug.h"
#include "regex_internal.h"

/**
 * NFA state list.
 */
struct nfalist {
  uint16_t *ns; /* list of active states */
  size_t *cgs; /* flat buffer of capture groups; (2 * ncg) per state */
  unsigned int size;
};

/**
 * Shared runtime state for all running NFA states.
 */
struct nfashared {
  const struct regex *r; /* regex being matched */
  size_t si; /* current string index */

  struct nfalist *clist, *nlist; /* double-buffered state lists */
  size_t *latest; /* latest si where an nfastate was active */
};

/**
 * Return a new empty nfalist for @r, or NULL if allocation fails.
 */
static struct nfalist *nfalist_new(const struct regex *r) {
  struct nfalist *list = calloc(1, sizeof(struct nfalist));
  if (!list)
    return NULL;
  if (!(list->ns = calloc(r->bufsize, sizeof(uint16_t))))
    goto free_list;
  if (!(list->cgs = calloc(r->bufsize, 2 * r->ncg * sizeof(size_t))))
    goto free_ns;
  return list;

free_ns:
  free(list->ns);
free_list:
  free(list);
  return NULL;
}

/**
 * Free @list, including the struct nfalist itself.
 */
static void nfalist_delete(struct nfalist *list) {
  free(list->ns);
  free(list->cgs);
  free(list);
}

/**
 * Add the ε-closure of @s to the starting clist. This should only be
 * called during init; it assumes nlist is all 0s.
 */
static void _nfa_init_eclosure(struct nfashared *nfa, uint16_t s) {
  struct _regex *_r;
  /* borrow nlist for membership checks; reset its size later. */
  if (nfa->nlist->ns[s])
    return;
  nfa->nlist->ns[s] = 1;

  _r = &nfa->r->buf[s];
  switch (_r->type) {
  case STAR:
  case PLUS:
  case QUESTION:
  case OR:
    _nfa_init_eclosure(nfa, _r->i);
    _nfa_init_eclosure(nfa, _r->next);
    return;
  case LCAPTURE:
  case RCAPTURE:
    _nfa_init_eclosure(nfa, _r->next);
    return;
  }
  nfa->clist->ns[nfa->clist->size++] = s;
}

/**
 * Initialize @nfa to match @r. Return whether initialization suceeded.
 */
static bool nfa_init(struct nfashared *nfa, const struct regex *r) {
  if (!(nfa->clist = nfalist_new(r)))
    return false;
  if (!(nfa->nlist = nfalist_new(r)))
    goto free_clist;
  if (!(nfa->latest = calloc(r->bufsize, sizeof(size_t))))
    goto free_nlist;

  nfa->r = r;
  nfa->si = 0;
  /* nfalist sizes, cgs, and latest are already 0 */
  _nfa_init_eclosure(nfa, r->head);
  nfa->nlist->size = 0; /* reset; was used in _nfa_init_eclosure */
  return true;

free_nlist:
  nfalist_delete(nfa->nlist);
free_clist:
  nfalist_delete(nfa->clist);
  return false;
}

static void nfa_free(struct nfashared *nfa) {
  nfalist_delete(nfa->nlist);
  nfalist_delete(nfa->clist);
  free(nfa->latest);
}

/**
 * Add the ε-closure of @s (reached from @i in clist) to nlist if it
 * wasn't added. This assumes a character was already matched in this step.
 */
static void nfa_add(struct nfashared *nfa, uint16_t s, unsigned int i) {
  struct _regex *_r;
  size_t backup;
  unsigned int k;
  if (nfa->latest[s] == nfa->si + 1)
    return;
  nfa->latest[s] = nfa->si + 1;

  _r = &nfa->r->buf[s];
  switch (_r->type) {
  case STAR:
  case PLUS:
  case QUESTION:
  case OR:
    /* greedy: try subpattern before next */
    nfa_add(nfa, _r->i, i);
    nfa_add(nfa, _r->next, i);
    return;
  case LCAPTURE:
  case RCAPTURE:
    /* backup instead of copying cgs on every capture */
    k = i * 2 * nfa->r->ncg + _r->i;
    backup = nfa->clist->cgs[k];
    nfa->clist->cgs[k] = nfa->si + 1;
    nfa_add(nfa, _r->next, i);
    nfa->clist->cgs[k] = backup;
    return;
  }

  /* copy captures from i to the new state */
  memcpy(&nfa->nlist->cgs[nfa->nlist->size * 2 * nfa->r->ncg],
         &nfa->clist->cgs[i * 2 * nfa->r->ncg],
         2 * nfa->r->ncg * sizeof(size_t));

  /* add new state */
  nfa->nlist->ns[nfa->nlist->size++] = s;
}

/**
 * Return whether @c is in @cls.
 */
static bool in_class(const struct _class *cls, char c) {
  unsigned char uc = (unsigned char) c;
  unsigned int index = uc / CHAR_BIT, offset = uc % CHAR_BIT;
  return cls->bs[index] & (1 << offset);
}

/**
 * Step the @ith state of clist with @c.
 */
static void nfa_step(struct nfashared *nfa, char c, unsigned int i) {
  struct _regex *_r = &nfa->r->buf[nfa->clist->ns[i]];
  switch (_r->type) {
  case DOT:
    if (c != '\n')
      break;
    return;
  case LITERAL:
    if (c == _r->c)
      break;
    return;
  case CLASS:
    if (in_class(&nfa->r->cbuf[_r->i], c))
      break;
    return;
  default:
    /* all ε-transition states were visited in previous step */
    return;
  }
  nfa_add(nfa, _r->next, i);
}

/**
 * Return whether the @ith current state is an accepting state.
 */
static bool nfa_accept(const struct nfashared *nfa, unsigned int i) {
  return nfa->r->buf[nfa->clist->ns[i]].type == ACCEPT;
}

/**
 * Swap the clist and nlist of @nfa.
 */
static void nfa_swap(struct nfashared *nfa) {
  struct nfalist *tmp = nfa->clist;
  nfa->clist = nfa->nlist;
  nfa->nlist = tmp;
  nfa->nlist->size = 0;
}

void regex_free(struct regex *regex)
{
  free(regex->buf);
  free(regex->cbuf);
}

/**
 * Return whether one of the current NFA states is an accepting state; if so,
 * copy out its capture groups to @cgs.
 */
static bool nfa_accept_copy_cgs(const struct nfashared *nfa, const char *str,
        struct capture *cgs) {
  unsigned int i;
  for (i = 0; i != nfa->clist->size; ++i) {
    if (nfa_accept(nfa, i)) {
      unsigned int k;
      for (k = 0; k != nfa->r->ncg; ++k) {
        unsigned int flatcg = i * 2 * nfa->r->ncg +
                  2 * k;
        cgs[k].start = str + nfa->clist->cgs[flatcg];
        cgs[k].len = nfa->clist->cgs[flatcg + 1] -
               nfa->clist->cgs[flatcg];
      }
      return true;
    }
  }
  return false;
}

bool regex_match(const struct regex *regex, const char *str, size_t len,
        struct capture *cgs) {
  bool ret = false;
  struct nfashared nfa;

  if (regex->ncg && !cgs) {
    fnerr("NULL cgs, but regex can capture\n");
    return false;
  }

  if (!nfa_init(&nfa, regex)) {
    fnerr("failed to init shared nfa state\n");
    return false;
  }

  for (; nfa.clist->size && nfa.si != len; ++nfa.si) {
    unsigned int i;
    for (i = 0; i != nfa.clist->size; ++i)
      nfa_step(&nfa, str[nfa.si], i);
    nfa_swap(&nfa);
  }

  ret = nfa_accept_copy_cgs(&nfa, str, cgs);

  nfa_free(&nfa);
  return ret;
}

/**
 * These set *p = x (with x encoded as network-endian, i.e., big-endian), then
 * return the next p according to sizeof(x).
 */
static uint8_t *push8(uint8_t *p, uint8_t x) {
  memcpy(p, &x, sizeof(x));
  return p + sizeof(x);
}

static uint8_t *push16(uint8_t *p, uint16_t x) {
  uint16_t n = htons(x);
  memcpy(p, &n, sizeof(x));
  return p + sizeof(x);
}

/*
 * Binary format:
 *   u8:  current version (1)
 *   u8:  number of capture groups
 *   u16: _regex buffer size
 *   u16: _class buffer size
 *   u16: _regex head
 *   _regex buffer
 *   _class buffer
 *
 * Each individual _regex is:
 *   u8: type
 *   u16: either c or i; 00 followed by c.
 *        If it doesn't matter (e.g. ACCEPT or DOT), filled with 0s.
 *   u16: next
 *
 * Each individual _class is:
 *   u8[32]: bitarray
 *
 * All values are big-endian (using hton[sl])
 *
 * Note that this means the total size will be
 * 8 + 5 * bufsize + 32 * cbufsize bytes.
 */
size_t regex_serialize(const struct regex *regex, void **buf) {
  size_t len = 8L + 5L * regex->bufsize + 32L * regex->cbufsize;
  uint8_t *p;
  unsigned int i;

  *buf = malloc(len);
  if (!*buf)
    return 0;
  p = *buf;

  p = push8(p, RE_SERIALIZE_VERSION);
  p = push8(p, regex->ncg);

  p = push16(p, regex->bufsize);
  p = push16(p, regex->cbufsize);
  p = push16(p, regex->head);

  for (i = 0; i != regex->bufsize; ++i) {
    struct _regex *_r = &regex->buf[i];
    p = push8(p, _r->type);
    switch (_r->type) {
    case LITERAL:
      p = push8(p, 0);
      p = push8(p, _r->c);
      break;
    case CLASS:
    case STAR:
    case PLUS:
    case QUESTION:
    case OR:
    case LCAPTURE:
    case RCAPTURE:
      p = push16(p, _r->i);
      break;
    case ACCEPT:
    case DOT:
      p = push16(p, 0);
      break;
    default:
      fnerr("unexpected type %u\n", _r->type);
      goto error;
    }
    p = push16(p, _r->next);
  }

  for (i = 0; i != regex->cbufsize; ++i) {
    memcpy(p, regex->cbuf[i].bs, sizeof(struct _class));
    p += sizeof(struct _class);
  }

  return len;

error:
  free(*buf);
  return 0;
}

/**
 * These return the given-sized int from @**p encoded in big-endian, and
 * advance @*p just past the read bytes.
 */
static uint8_t read8(const uint8_t **p) {
  uint8_t x;
  memcpy(&x, *p, sizeof(x));
  *p += sizeof(x);
  return x;
}

static uint16_t read16(const uint8_t **p) {
  uint16_t x;
  memcpy(&x, *p, sizeof(x));
  *p += sizeof(x);
  return ntohs(x);
}

/**
 * Deserialize v1 @buf to a regex.
 */
bool regex_deserialize(const void *buf, struct regex *regex) {
  const uint8_t *p = buf;
  uint8_t tmp;
  unsigned int i;

  tmp = read8(&p);
  if (tmp != RE_SERIALIZE_VERSION) {
    fnerr("unexpected version %u\n", tmp);
    return false;
  }

  regex->ncg = read8(&p);
  if (regex->ncg > REGEX_MAX_CAPTURES) {
    fnerr("too many capture groups: %u\n", regex->ncg);
    return false;
  }

  regex->bufsize = read16(&p);
  regex->cbufsize = read16(&p);
  if (regex->cbufsize > regex->bufsize) {
    fnerr("more classes than nodes: %u > %u\n",
          regex->cbufsize, regex->bufsize);
    return false;
  }

  regex->head = read16(&p);
  if (regex->head >= regex->bufsize) {
    fnerr("head > bufsize: %u > %u\n", regex->head, regex->bufsize);
    return false;
  }

  if (!(regex->buf = calloc(regex->bufsize, sizeof(struct _regex)))) {
    fnerr("failed to allocate buf\n");
    return false;
  }
  if (!(regex->cbuf = calloc(regex->cbufsize, sizeof(struct _class)))) {
    fnerr("failed to allocate cbuf\n");
    goto free_buf;
  }

  for (i = 0; i != regex->bufsize; ++i) {
    struct _regex *_r = &regex->buf[i];
    _r->type = read8(&p);
    if (_r->type <= LINVALID || _r->type >= HINVALID) {
      fnerr("invalid type %u\n", _r->type);
      goto free_all;
    }
    switch (_r->type) {
    case LITERAL:
      p += 1; // throw away junk byte
      _r->c = read8(&p);
      break;
    case CLASS:
    case STAR:
    case PLUS:
    case QUESTION:
    case OR:
    case LCAPTURE:
    case RCAPTURE:
      _r->i = read16(&p);
      if (_r->i >= regex->bufsize) {
        fnerr("i > bufsize: %u > %u\n", _r->i, regex->bufsize);
        goto free_all;
      }
      break;
    default:
      _r->i = read16(&p);
      break;
    }
    _r->next = read16(&p);
    if (_r->type != ACCEPT && _r->next >= regex->bufsize) {
      fnerr("next > bufsize: %u > %u\n", _r->next, regex->bufsize);
      goto free_all;
    }
  }

  for (i = 0; i != regex->cbufsize; ++i) {
    memcpy(regex->cbuf[i].bs, p, sizeof(struct _class));
    p += sizeof(struct _class);
  }

  return true;

free_all:
  free(regex->cbuf);
free_buf:
  free(regex->buf);
  return false;
}
