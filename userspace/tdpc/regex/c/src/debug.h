#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>

#define fnerr(fmt, ...) fprintf(stderr, "%s: " fmt, __func__, ## __VA_ARGS__)

#endif
