#ifndef REGEX_H
#define REGEX_H
/*
 * Supported patterns (all quantifiers are greedy):
 *  a     character literal
 *  .     any character but newline (\n)
 *  [ ]   character class (ranges/empty (e.g. [a-z]/[]) are allowed)
 *  [^ ]  inverted character class (ranges/empty are allowed)
 *  p*    zero or more of p
 *  p+    one or more of p
 *  p?    zero or one of p
 *  (p)   group (quantifiers apply to entire group)
 *  <p>   capture group (group, and location of contents is stored)
 *  a|b   alternation (a or b)
 *
 *
 * For alternation, the left branch is taken first. For example, when given a,
 * <a>|a will capture a, but a|<a> will capture nothing. Relying on this
 * behavior is not advised.
 *
 * The following escapes are always allowed:
 *   \oo?o? \xhh? \a \b \e \f \n \r \t \v \\
 * where \oo?o? represents an octal value with 1-3 digits and \xhh? represents
 * a hex value with 1-2 digits.
 * A backslash followed by any other character is simply that character; this
 * can be used to get ] in a character class (e.g. [\]]) or to interpret a
 * metacharacter as a literal (e.g. \* is a literal asterisk).
 *
 * Empty regexes (e.g. (a|)) are not allowed. You can get the same effect by
 * using []? or []*, since the empty character class cannot match anything.
 *
 * Ranges must be forwards. For example, a-z is all lowercase letters, but z-a
 * is nothing.
 *
 * The internal representation of regexes uses C chars, and assumes 1
 * character is 1 byte. For encodings like UTF-8, this is only a problem if:
 *   1. A multibyte character is used in a character class.
 *      Solution: use a literal and alternation instead.
 *   2. A multibyte character is quantified without parentheses.
 *      Solution: Surround quantified multibyte characters with parentheses.
 *
 * Note that this implementation relies on string length instead of null
 * characters to decide the end of input. It is possible to include null
 * characters in the regex pattern (although to do so, regex_nparse must be
 * used instead of regex_parse, as regex_parse assumes null-terminated
 * strings. In regex_parse, \0 can be used instead.)
 *
 * For iovecs in writev, use copy_from_iter to read in the entire iovec into a
 * single string. This should hopefully be okay, since log messages tend not
 * to be very big.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

struct _regex;
struct _class;

/** Max number of capture groups */
#define REGEX_MAX_CAPTURES 9

/**
 * Regex pattern type.
 */
struct regex {
  /*
   * the regex is a flat, contiguous array in memory. actual max length
   * is 2^16 - 1 = 65535 nodes; however, a better "soft limit" is how
   * many fit in 1 page (on x86, around ~600 nodes). furthermore,
   * only 128 character classes fit into an x86 page. hopefully, none of
   * this will actually matter, because it's unlikely for a pattern to
   * be that big.
   */
  struct _regex *buf;
  uint16_t bufsize; /* number of elements in buf; max 2^16 - 1 */
  uint16_t head; /* offset of head in buf */

  struct _class *cbuf; /* classes are also contiguous */
  uint16_t cbufsize; /* cbufsize < bufsize */

  uint8_t ncg; /* number of capture groups; <= REGEX_MAX_CAPTURES */
};

struct capture {
  const char *start; /* pointer to group within the original string */
  size_t len;        /* length of group */
};

/**
 * Parse a regex from a @n-char string @pattern into @regex, and return
 * whether it succeeds. (If it fails, nothing is allocated.)
 * If @verbose, print error messages to stderr when parsing fails.
 */
bool regex_nparse(const char *pattern, size_t n, struct regex *regex,
                  bool verbose);

/**
 * Parse a regex from a null-terminated string @pattern into @regex, and
 * return whether it succeeds. (If it fails, nothing is allocated.)
 * If @verbose, print error messages to stderr when parsing fails.
 *
 * Note that this is equivalent to calling nparse with n = strlen(pattern).
 * Thus, this could be unsafe if @pattern is not null-terminated.
 */
bool regex_parse(const char *pattern, struct regex *regex, bool verbose);

/**
 * Free a @regex.
 */
void regex_free(struct regex *regex);

/**
 * Return true iff @regex exactly matches @str with length @len.
 * @cgs is a buffer of size regex->ncg; it can be NULL if regex->ncg = 0.
 * If the match fails, @cgs is unmodified.
 */
bool regex_match(const struct regex *regex, const char *str, size_t len,
                 struct capture *cgs);

#define RE_SERIALIZE_VERSION 1
/**
 * Serialize @regex to @buf. New memory is allocated at *@buf;
 * if serialization fails, @buf is NULL (thus *@buf can always be freed).
 * Return the length of the buffer, or 0 on failure. An actual serialized
 * regex will never have length 0.
 */
size_t regex_serialize(const struct regex *regex, void **buf);

/**
 * Deserialize @buf to @regex; return whether @buf was valid.
 * If invalid, regex is undefined (but contains no allocated buffers).
 */
bool regex_deserialize(const void *buf, struct regex *regex);

#endif
