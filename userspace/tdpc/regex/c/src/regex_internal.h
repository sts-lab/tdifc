#ifndef REGEX_INTERNAL_H
#define REGEX_INTERNAL_H

#include <limits.h>

#include "regex.h"

enum {
  LINVALID = 0, ACCEPT, DOT, LITERAL, CLASS, OR, STAR, PLUS, QUESTION,
  LCAPTURE, RCAPTURE, HINVALID
};

/** Bitwise boolean array for whether a char is in the class or not. */
struct _class {
  /* for 8-bit chars, should be 32 bytes = 256 bits */
  unsigned char bs[(1 << CHAR_BIT) / CHAR_BIT];
};

/**
 * Uses 16-bit indices to be smaller and more amenable to binary storage.
 * max allowed regex length: 2**16 - 1 = 65535 (should be more than enough).
 */
struct _regex {
  uint8_t type;
  union {
    char c; /* character literal */
    uint16_t i; /* index of pattern, class, or capture group */
    /* captures are flat, e.g., group 1: (0, 1), 2: (2, 3), ... */
  };
  uint16_t next; /* index of next regex in buffer */
};

#endif
