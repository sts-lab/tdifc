(** Bindings for C library [regex.h], including its structures and public
    functions. *)

module Types : sig
  (** regex phantom type *)
  type regex

  (** [regex.h]'s [struct regex]. Use ctypes accessors for its fields. *)
  type t = regex Ctypes.structure

  (** Value representing [struct regex]. *)
  val regex : t Ctypes.typ

  (** Internal structures of [struct regex]. Outside of [regex.h], these are
      implementation details and thus not useful. *)
  type _regex
  val _regex : _regex Ctypes.structure Ctypes.typ
  type _class
  val _class : _class Ctypes.structure Ctypes.typ

  (** capture phantom type *)
  type capture

  (** Value representing [struct capture]. *)
  val capture : capture Ctypes.structure Ctypes.typ
end

(** Contains Ctypes fields that can be used with Ctypes to get/set struct
    field values. *)
module Fields : sig
  module Regex : sig
    val buf : (Types._regex Ctypes.structure Ctypes.ptr, Types.t) Ctypes.field
    val bufsize : (Unsigned.uint16, Types.t) Ctypes.field
    val head : (Unsigned.uint16, Types.t) Ctypes.field
    val cbuf : (Types._class Ctypes.structure Ctypes.ptr, Types.t) Ctypes.field
    val cbufsize : (Unsigned.uint16, Types.t) Ctypes.field
    val ncg : (Unsigned.uint8, Types.t) Ctypes.field
  end

  module Capture : sig
    val start : (char Ctypes.ptr, Types.capture Ctypes.structure) Ctypes.field
    val len : (Unsigned.size_t, Types.capture Ctypes.structure) Ctypes.field
  end
end

(* Bound public functions from [regex.h]. *)
val regex_nparse : string -> Unsigned.size_t -> Types.t Ctypes.ptr -> bool ->
  bool
val regex_free : Types.t Ctypes.ptr -> unit
val regex_match : Types.t Ctypes.ptr -> string -> Unsigned.size_t ->
  Types.capture Ctypes.structure Ctypes.ptr -> bool
val regex_serialize : Types.t Ctypes.ptr -> unit Ctypes.ptr Ctypes.ptr ->
  Unsigned.size_t
val regex_deserialize : unit Ctypes.ptr -> Types.t Ctypes.ptr -> bool
