module F = Ctypes_bindings.Bindings

let c_headers = "#include <regex.h>"

let main () =
  let c_out = open_out "gen_stubs.c" in
  let ml_out = open_out "gen_bindings.ml" in
  let c_fmt = Format.formatter_of_out_channel c_out in
  let ml_fmt = Format.formatter_of_out_channel ml_out in
  Format.fprintf c_fmt "%s\n" c_headers;
  Cstubs.write_c c_fmt ~prefix:"regex_stub_" (module F);
  Cstubs.write_ml ml_fmt ~prefix:"regex_stub_" (module F);
  Format.pp_print_flush c_fmt ();
  Format.pp_print_flush ml_fmt ();
  close_out c_out;
  close_out ml_out

let () = main ()
