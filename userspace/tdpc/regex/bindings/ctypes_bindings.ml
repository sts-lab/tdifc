module C = Ctypes
module U = Unsigned

(* regex.h requires the user to pass in the struct regex in case it should
   actually be put on the stack instead of the heap. thus, we actually do need
   to reconstruct struct regex here. *)
module Types = struct
  open C

  type regex
  type t = regex structure
  let regex : regex structure typ = structure "regex"

  type _regex
  let _regex : _regex structure typ = structure "_regex"

  type _class
  let _class : _class structure typ = structure "_class"

  type capture
  let capture : capture structure typ = structure "capture"
end

module Fields = struct
  module Regex = struct
    open C

    let buf = field Types.regex "buf" (ptr Types._regex)
    let bufsize = field Types.regex "bufsize" uint16_t
    let head = field Types.regex "head" uint16_t
    let cbuf = field Types.regex "cbuf" (ptr Types._class)
    let cbufsize = field Types.regex "cbufsize" uint16_t
    let ncg = field Types.regex "ncg" uint8_t

    let () = seal Types.regex
  end

  module Capture = struct
    open C

    let start = field Types.capture "start" (ptr char)
    let len = field Types.capture "len" size_t

    let () = seal Types.capture
  end
end

module Bindings (F : C.FOREIGN) = struct
  let regex_nparse = F.foreign "regex_nparse"
    C.(F.(string @-> size_t @-> ptr Types.regex @-> bool @-> returning bool))

  let regex_free = F.foreign "regex_free"
    C.(F.(ptr Types.regex @-> returning void))

  let regex_match = F.foreign "regex_match"
    C.(F.(ptr Types.regex @-> string @-> size_t @-> ptr Types.capture
          @-> returning bool))

  let regex_serialize = F.foreign "regex_serialize"
    C.(F.(ptr Types.regex @-> ptr (ptr void) @-> returning size_t))

  let regex_deserialize = F.foreign "regex_deserialize"
    C.(F.(ptr void @-> ptr Types.regex @-> returning bool))
end
