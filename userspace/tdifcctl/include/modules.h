#ifndef MODULES_H
#define MODULES_H

#include <stddef.h>

#define FORALL_MODULES_DO_F() \
  F(getlabel) \
  F(getpolid) \
  F(setpolid)

struct module {
  char *name;
  int (*main)(int, char **);
};

const struct module *module_lookup(register const char *str,
                                   register size_t len);

#endif
