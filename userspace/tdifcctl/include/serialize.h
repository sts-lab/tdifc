#ifndef SERIALIZE_H
#define SERIALIZE_H

#include <string.h>

/**
 * Return how many bytes the base85 representation of @i would require.
 */
static inline unsigned int base85_u32_bytes(uint32_t i) {
  /*
   * only 5 base85 chars are needed to represent a 32-bit int. however,
   * a lot of leading characters are likely to be 0 (most ints are
   * probably small), so trim off the leading 0s.
   */
  unsigned int max = 5;
  unsigned int n = max; // number of digits
  for (; i / 85 > 0; --n, i /= 85);
  return max - n + 1;
}

static inline unsigned int base85_size_t_bytes(size_t i) {
  if (sizeof(size_t) > sizeof(uint32_t)) {
    unsigned int max = 5 * sizeof(size_t) / sizeof(uint32_t);
    unsigned int n = max;
    for (; i / 85 > 0; --n, i /= 85);
    return max - n + 1;
  } else {
    return base85_u32_bytes(i);
  }
}

static const char *base85_set = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz!#$%&()*+-;<=>?@^_`{|}~";

/**
 * Encode @i as base85 in @dst, and return just past the end of the written
 * part of @dst. @dst must be big enough to fit the base85 encoding of @i.
 */
static inline char *base85_enc(char *dst, size_t i) {
  size_t n = base85_size_t_bytes(i);
  char *end = dst + n;
  dst = end - 1;
  /* if i is literally 0, write one 0 */
  if (i == 0) {
    *dst = base85_set[0];
    return end;
  }

  for (; i != 0; i /= 85)
    *dst-- = base85_set[i % 85];

  return end;
}

/**
 * Decode the base85 prefix from @src up to @end to a size_t. This assumes
 * that the base85 prefix will fit within a size_t. @src is advanced to the
 * first non-base85 character. If @src is not a base85 integer, then it is not
 * forwarded at all, and 0 is returned.
 */
static inline size_t base85_dec(char **src, char *end) {
  size_t i = 0;
  /*
   * A jump table would be marginally faster than strchr, but seems like
   * an unnecessary optimization versus code size (85 lines).
   */
  while (*src < end) {
    char *p = strchr(base85_set, **src);
    if (!p) {
      return i;
    } else {
      i = 85 * i + (uint32_t) (p - base85_set);
      ++*src;
    }
  }
  return i;
}

#endif
