#ifndef LSM_TYPES_H
#define LSM_TYPES_H

#define FORALL_DO_F() \
    F(TDIFC_NS_NOPOLICY) \
    F(TDIFC_NS_UNIQUE) \
    F(TDIFC_NS_NORMAL) \

#define F(x) x,
enum tdifc_ns_type {
  FORALL_DO_F()
};
#undef F

static inline const char *tdifc_ns_type_to_string(enum tdifc_ns_type type) {
#define F(x) case x: return #x;
  switch (type) {
    FORALL_DO_F()
  }
  return "BUG: unknown ns_type";
#undef F
}

#undef MACRO_ITER

#endif
