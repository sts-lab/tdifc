#include <errno.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/xattr.h>

#include "lsm_types.h"
#include "serialize.h"
#include "util.h"

/**
 * Print the label of @path, if it has any.
 * Return 0 on success, or < 0 and set errno on failure.
 */
static int getlabel(const char *path) {
  ssize_t n = getxattr(path, XATTR_LABEL, NULL, 0);
  if (n < 0)
    return n;
  char *xstr = malloc(n);
  n = getxattr(path, XATTR_LABEL, xstr, n);
  if (n < 0) {
    switch (errno) {
    case ERANGE:
      return getlabel(path); // try again
    default:
      return n;
    }
  }

  int ret = 0;
  char *p = xstr, *start = p, *end = start + n;
  printf("%s: {\n", path);
  while (p != end) {
#define READ_BASE85(lhs) \
    do { \
      start = p; \
      (lhs) = base85_dec(&p, end); \
      /* ensure non-empty entry is followed by comma */ \
      if (p == start || p == end || *p != ',') { \
        ret = -1; \
        errno = EINVAL; \
        goto free_xstr; \
      } \
      ++p; /* consume the comma */ \
    } while (0)

    unsigned int ns_type, flags;
    uint32_t ns_id, pol_id;
    size_t tag_length;
    READ_BASE85(ns_type);
    READ_BASE85(ns_id);
    READ_BASE85(pol_id);
    READ_BASE85(flags);
    READ_BASE85(tag_length);
#undef READ_BASE85

    const char *string = p;
    /* ensure string is within xstr */
    p += tag_length;
    if (p > end) {
      ret = -1;
      errno = EINVAL;
      goto free_xstr;
    }
    printf("  { ns_type = %s,\n"
           "    ns_id   = %u,\n"
           "    pol_id  = %u,\n"
           "    flags   = %u,\n"
           "    string  = %.*s,\n"
           "  },\n",
           tdifc_ns_type_to_string(ns_type), ns_id, pol_id, flags,
           (int) tag_length, string);
  }
  puts("}");

free_xstr:
  free(xstr);
  return ret;
}

static void usage(char *arg0) {
  fprintf(stderr,
    "usage: %s FILE\n"
    "\n"
    "  Print the TDIFC label of FILE to stdout.\n",
    arg0);
}

int getlabel_main(int argc, char **argv) {
  for (;;) {
    struct option longopts[] = {
      {"help", 0, NULL, 'h'},
    };
    int longind = 0;
    int c = getopt_long(argc, argv, "h", longopts, &longind);
    if (c == -1) break;
    switch (c) {
    case 'h':
      usage(argv[0]);
      return EXIT_SUCCESS;
    case '?':
      usage(argv[0]);
      return EXIT_FAILURE;
    }
  }

  if (optind + 1 > argc) {
    usage(argv[0]);
    return EXIT_FAILURE;
  }

  int ret = EXIT_SUCCESS;
  for (; optind != argc; ++optind) {
    char *path = argv[optind];
    if (getlabel(path) < 0) {
      fprintf(stderr, "failed to get label of %s: %s\n",
              path, strerror(errno));
      ret = EXIT_FAILURE;
    }
  }
  return ret;
}
