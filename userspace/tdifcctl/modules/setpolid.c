#include <errno.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/xattr.h>

#include "serialize.h"
#include "util.h"

/**
 * Set the file at @path's policy id to @pol_id.
 * Return 0 on success, or < 0 and set errno on failure.
 */
static int setpolid(const char *path, uint32_t pol_id) {
  char buf[5]; // at most 5 chars needed for base85 enc of uint32_t
  unsigned int n = base85_u32_bytes(pol_id);
  (void) base85_enc(buf, pol_id);

  return setxattr(path, XATTR_POLICY, buf, n, 0);
}

static void usage(char *arg0) {
  fprintf(stderr,
    "usage: %s N FILES...\n"
    "\n"
    "  Set the TDIFC policy ID of FILES to N.\n",
    arg0);
}

int setpolid_main(int argc, char **argv) {
  for (;;) {
    struct option longopts[] = {
      {"help", 0, NULL, 'h'},
    };
    int longind = 0;
    int c = getopt_long(argc, argv, "h", longopts, &longind);
    if (c == -1) break;
    switch (c) {
    case 'h':
      usage(argv[0]);
      return EXIT_SUCCESS;
    case '?':
      usage(argv[0]);
      return EXIT_FAILURE;
    }
  }

  if (optind + 2 > argc) {
    usage(argv[0]);
    return EXIT_FAILURE;
  }

  char *end;
  unsigned long n = strtoul(argv[optind], &end, 0);
  if (argv[optind] == end || *end != '\0') {
    fprintf(stderr, "error: invalid ID %s\n", argv[optind]);
    return EXIT_FAILURE;
  }

  uint32_t id = n;
  if (id != n) {
    fprintf(stderr, "error: ID %lu is too large\n", n);
    return EXIT_FAILURE;
  }
  ++optind;

  for (; optind != argc; ++optind) {
    char *path = argv[optind];
    if (setpolid(path, id) < 0) {
      fprintf(stderr, "failed to set policy ID of %s: %s\n",
              path, strerror(errno));
      return EXIT_FAILURE;
    }
  }

  return EXIT_SUCCESS;
}
