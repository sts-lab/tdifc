#include <errno.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/xattr.h>

#include "serialize.h"
#include "util.h"

/**
 * Print the policy ID of @path, if it has any.
 * Return 0 on success, or < 0 and set errno on failure.
 */
static int getpolid(const char *path) {
  ssize_t n = getxattr(path, XATTR_POLICY, NULL, 0);
  if (n < 0)
    return n;
  char *xstr = malloc(n);
  n = getxattr(path, XATTR_POLICY, xstr, n);
  if (n < 0) {
    switch (errno) {
    case ERANGE:
      return getpolid(path); // try again
    default:
      return n;
    }
  }

  int ret = 0;
  char *p = xstr, *start = p, *end = start + n;
  uint32_t id = base85_dec(&p, end);
  if (n == 0) {
    fputs("warning: unexpected empty string for policy ID\n", stderr);
  } else if (p != end) {
    fprintf(stderr, "warning: unexpected policy ID format (got %.*s)\n",
            (int) n, xstr);
  }
  printf("%s: %u\n", path, id);

  free(xstr);
  return ret;
}

static void usage(char *arg0) {
  fprintf(stderr,
    "usage: %s FILES...\n"
    "\n"
    "  Print the TDIFC policy ID of FILES to stdout.\n",
    arg0);
}

int getpolid_main(int argc, char **argv) {
  for (;;) {
    struct option longopts[] = {
      {"help", 0, NULL, 'h'},
    };
    int longind = 0;
    int c = getopt_long(argc, argv, "h", longopts, &longind);
    if (c == -1) break;
    switch (c) {
    case 'h':
      usage(argv[0]);
      return EXIT_SUCCESS;
    case '?':
      usage(argv[0]);
      return EXIT_FAILURE;
    }
  }

  if (optind + 1 > argc) {
    usage(argv[0]);
    return EXIT_FAILURE;
  }

  int ret = EXIT_SUCCESS;
  for (; optind != argc; ++optind) {
    char *path = argv[optind];
    if (getpolid(path) < 0) {
      fprintf(stderr, "failed to get policy ID of %s: %s\n",
              path, strerror(errno));
      ret = EXIT_FAILURE;
    }
  }

  return ret;
}
