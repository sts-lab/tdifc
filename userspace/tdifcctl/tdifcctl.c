#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "modules.h"

void usage(char *arg0) {
#define F(M) " " #M
  fprintf(stderr,
    "usage: %s MODULE ...\n"
    "\n"
    "  Run MODULE with the given arguments.\n"
    "  Arguments and options may vary depending on the module.\n"
    "  Use -h or --help to see the help message for any module.\n"
    "\n"
    "  Known modules are:\n"
    "   " FORALL_MODULES_DO_F() "\n",
    arg0);
#undef F
}

int main(int argc, char **argv) {
  if (argc < 1 + 1) {
    usage(argv[0]);
    return EXIT_FAILURE;
  }

  const struct module *m = module_lookup(argv[1], strlen(argv[1]));
  if (!m) {
    fprintf(stderr, "error: unrecognized module %s\n", argv[1]);
    usage(argv[0]);
    return EXIT_FAILURE;
  }
  --argc;
  ++argv; // shift and turn module into arg0
  return m->main(argc, argv);
}
