#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "modules.h"

bool copy(const char *path, FILE *out) {
  FILE *in = fopen(path, "r");
  if (!in)
    return false;

  for (;;) {
    const size_t bufsize = 4096;
    char buf[bufsize];
    size_t n = fread(buf, 1, bufsize, in);
    if (n == 0) {
      fclose(in);
      return !ferror(in);
    }
    size_t n_write = fwrite(buf, 1, n, out);
    if (n_write != n) {
      fclose(in);
      return false;
    }
  }
}

int main(int argc, char **argv) {
  if (argc < 2 + 1) {
    return EXIT_FAILURE;
  }

  char *infile = argv[1];
  char *outfile = argv[2];

  FILE *out = fopen(outfile, "w");

  fputs("/* THIS IS A GENERATED FILE. DO NOT MODIFY! */\n", out);

  if (!copy(infile, out))
    goto error;

#define F(M) fprintf(out, "%1$s, %1$s_main\n", #M);
  FORALL_MODULES_DO_F()
#undef F

  fputs("%%\n", out);

  fclose(out);
  return EXIT_SUCCESS;

error:
  fclose(out);
  remove(outfile);
  return EXIT_FAILURE;
}
