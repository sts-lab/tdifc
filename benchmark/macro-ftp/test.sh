#!/bin/sh

set -eu

# samples to collect per trial (includes both a file size and operation)
n=100

# where to store results
#outdir=control
outdir=tdifc

mkdir -p "$outdir"

# ftp_session OPS
# Perform FTP operations during a session. Report wall time in seconds
# OPS is a newline-separated list of operations understood by `ftp`
# e.g., "put <file>"
ftp_once() {
  time -f'%e' ftp -4npi 127.0.0.1 <<EOF
user sql sql
$1
EOF
}

for op in put get; do
  for file in 1B 1K 1M 1G; do
    output="$outdir/$op-$file"
    rm -f -- "$output"
    for i in $(seq 1 $n); do
      echo "$op-$file: trial $i"
      ftp_once "$op $file" 2>> "$output"
    done
  done
done

echo "put-${n}G: starting"
ftp_once "$(yes 'put 1G' | head -n"$n")" 2> "$outdir/put-${n}G"

echo "get-${n}G: starting"
ftp_once "$(yes 'get 1G' | head -n"$n")" 2> "$outdir/get-${n}G"
