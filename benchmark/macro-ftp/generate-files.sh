#!/bin/sh

# Generate random files
for size in 1B 1K 1M 1G; do
  tr -Cd '[:alnum:]' < /dev/urandom | head -c"$size" > "$size"
done
