import sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

def load(path):
    with open(path, "r") as f:
        times = [float(line) for line in f]
    if len(times) == 1:
        return times[0], np.nan
    return np.mean(times), np.std(times, ddof=1)

def load_all(typ, ops):
    trials = ["1B", "1K", "1M", "1G", "100G"]

    means, stds, labels = [], [], []
    for op in ops:
        for trial in trials:
            path = "{}/{}-{}".format(typ, op, trial)
            mean, std = load(path)
            means.append(mean)
            stds.append(std)
            labels.append("{}-{}".format(op, trial))
    return means, stds, labels

def main(argv):
    mpl.rcParams.update({"font.size": 16})
    mpl.rcParams.update({"figure.autolayout": True})
    ctrl, ctrlStd, labels = load_all("control", ["get"])
    difc, difcStd, _ = load_all("difc", ["get"])

    width = 0.35
    x = np.arange(len(labels))

    fig, ax = plt.subplots()
    ctrlBars = ax.bar(x - width/2, ctrl, width, yerr=ctrlStd, label="Control")
    difcBars = ax.bar(x + width/2, difc, width, yerr=difcStd, label="T-DIFC")

    ax.set_ylabel("Time (s)")
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()
    ax.set_ylim([1, 10_000])
    ax.set_yscale("log")

    plt.show()
    fig.savefig("ftp-get.pdf")

    # put
    ctrl, ctrlStd, labels = load_all("control", ["put"])
    difc, difcStd, _ = load_all("difc", ["put"])

    width = 0.35
    x = np.arange(len(labels))

    fig, ax = plt.subplots()
    ctrlBars = ax.bar(x - width/2, ctrl, width, yerr=ctrlStd, label="Control")
    difcBars = ax.bar(x + width/2, difc, width, yerr=difcStd, label="T-DIFC")

    ax.set_ylabel("Time (s)")
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()
    ax.set_ylim([1, 10_000])
    ax.set_yscale("log")

    plt.show()
    fig.savefig("ftp-put.pdf")

    #def load_100(typ):
    #    means, labels = [], []
    #    for op in ["put", "get"]:
    #        path = "{}/{}-100G".format(typ, op)
    #        mean, _ = load(path)
    #        means.append(mean)
    #        labels.append("{}-100G".format(op))
    #    return means, labels

    #ctrl, labels = load_100("control")
    #difc, _ = load_100("difc")

    #width = 0.1
    #x = np.arange(len(labels))
    #fig, ax = plt.subplots()
    #ctrlBars = ax.bar(x - width/2, ctrl, width, label="Control")
    #difcBars = ax.bar(x + width/2, difc, width, label="Control")

    #ax.set_ylabel("Time (s)")
    #ax.set_xticks(x)
    #ax.set_xticklabels(labels)
    #ax.legend()

    #plt.show()
    #fig.savefig("ftp-100G-e2e.pdf")

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
