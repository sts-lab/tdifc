#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define NANOSEC_PER_SEC ((long) 1e9)

const unsigned int SAMPLE_SIZE = (unsigned int) 1e6;

static long benchmark_write(int fd, const char *s, size_t n) {
  struct timespec ts0, ts1;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts0);
  int res = write(fd, s, n);
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts1);
  if (res < 0)
    return res;
  else {
    long nsec = ts1.tv_nsec - ts0.tv_nsec;
    time_t sec = ts1.tv_sec - ts0.tv_sec;
    return sec * NANOSEC_PER_SEC + nsec;
  }
}

int main(int argc, char **argv) {
  if (argc < 2) {
    fprintf(stdout, "usage: %s string\n", argv[0]);
    return 1;
  }
  const char *s = argv[1];
  size_t slen = strlen(s);
  double m2 = 0, avg = 0;
  unsigned int count = 0;
  for (int i = 0; i < SAMPLE_SIZE; ++i) {
    long time = benchmark_write(STDERR_FILENO, s, slen);
    ++count;
    double d1 = time - avg;
    avg += d1 / count;
    double d2 = time - avg;
    m2 += d1 * d2;
  }
  fprintf(stdout, "mean var\n%f %f\n", avg, m2 / (count - 1));
  return 0;
}
