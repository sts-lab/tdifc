import numpy as np

import cycler
import matplotlib as mpl
import matplotlib.pyplot as plt
import colorcet as cc

def nanos_to_micros(nanos):
    return nanos / 1000

def fixp_to_float(fixp):
    value, errfrac = fixp.split("+")
    num, denom = errfrac.split("/")
    return int(value) + (int(num) / int(denom))

def load_micro(path):
    with open(path, "r") as f:
        next(f)
        # regex, external policy code, permission + label propagation
        means, stds = [], []
        for line in f:
            _, mean, var = line.split(" ")
            mean, var = fixp_to_float(mean), fixp_to_float(var)
            means.append(nanos_to_micros(mean))
            stds.append(nanos_to_micros(np.sqrt(var)))
    return means, stds

def load_e2e(path):
    with open(path, "r") as f:
        next(f)
        mean, var = f.readline().split(" ")
        mean, var = float(mean), float(var)
    return nanos_to_micros(mean), nanos_to_micros(np.sqrt(var))

# <labelsize>-<statements>
def load_2d(name, loadFn):
    means, sds = [], []
    for x in range(1, 21):
        means.append([])
        sds.append([])
        for y in range(1, 21):
            path = "{}-{}/{}".format(x, y, name)
            mean, sd = loadFn(path)
            means[-1].append(mean)
            sds[-1].append(sd)
    return np.array(means), np.array(sds)

def main():
    mpl.rcParams.update({"font.size": 16})
    mpl.rcParams.update({"figure.autolayout": True})
    difc, difcStd = load_2d("difc-e2e", load_e2e)
    ctrl, ctrlStd = load_2d("control-e2e", load_e2e)
    micro, microStd = load_2d("micro-benchmark", load_micro)

    def overhead(expt, ctrl):
        return (expt - ctrl) / ctrl * 100

    print(overhead(difc[0][0], ctrl[0][0]))
    print(overhead(difc[-1][-1], ctrl[-1][-1]))

    x = np.arange(1, 21)
    major = mpl.ticker.MultipleLocator(base=2)
    minor = mpl.ticker.MultipleLocator(base=1)
    colors = cycler.cycler(color=cc.glasbey_dark)

    # fixed labelsize, vary statements
    fig, ax = plt.subplots()

    #ax.errorbar(x, ctrl[0,:], yerr=ctrlStd[0,:], ls="--")
    ax.plot(x, ctrl[0,:], color="black", ls="--")
    ax.set_prop_cycle(colors)
    for i in range(0, 20):
        ax.errorbar(x, difc[i,:], yerr=difcStd[i,:])
        #ax.plot(x, difc[i,:])
    ax.set_ylabel("Time (μs)")
    ax.set_xlabel("Policy Size (statements)")
    ax.set_ylim([0, 50])
    ax.set_xlim([0, 21])
    ax.xaxis.set_major_locator(major)
    ax.xaxis.set_minor_locator(minor)

    plt.show()
    fig.savefig("write-vary-stmts-err.pdf")

    # fixed statements, vary labelsize
    fig, ax = plt.subplots()

    #ax.errorbar(x, ctrl[:,0], yerr=ctrlStd[:,0], ls="--")
    ax.plot(x, ctrl[:,0], color="black", ls="--")
    ax.set_prop_cycle(colors)
    for i in range(0, 20):
        ax.errorbar(x, difc[:,i], yerr=difcStd[:,i])
        #ax.plot(x, difc[:,i])
    ax.set_ylabel("Time (μs)")
    ax.set_xlabel("Label Size (tags)")
    ax.set_ylim([0, 50])
    ax.set_xlim([0, 21])
    ax.xaxis.set_major_locator(major)
    ax.xaxis.set_minor_locator(minor)

    plt.show()
    fig.savefig("write-vary-labelsize-err.pdf")

    # labelsize=10, vary statements
    width = 0.3
    fig, ax = plt.subplots()
    regex = ax.bar(x - width, micro[10,:,0], width, yerr=microStd[10,:,0], label="Log Regex Match")
    perm = ax.bar(x, micro[10,:,2], width, yerr=microStd[10,:,2], label="Perms, Label Prop.")
    stmts = ax.bar(x + width, micro[10,:,1], width, yerr=microStd[10,:,1], label="Execute Policy Code")

    ax.set_ylabel("Time (μs)")
    ax.set_xlabel("Policy Size (statements)")
    ax.set_ylim([0, 25])
    ax.set_xlim([0, 21])
    ax.xaxis.set_major_locator(major)
    ax.xaxis.set_minor_locator(minor)
    ax.legend()

    plt.show()
    fig.savefig("micro-vary-stmts.pdf")

    # vary labels, statements=10
    width = 0.3
    fig, ax = plt.subplots()
    regex = ax.bar(x - width, micro[:,10,0], width, yerr=microStd[:,10,0], label="Log Regex Match")
    perm = ax.bar(x, micro[:,10,2], width, yerr=microStd[:,10,2], label="Perms, Label Prop.")
    stmts = ax.bar(x + width, micro[:,10,1], width, yerr=microStd[:,10,1], label="Execute Policy Code")

    ax.set_ylabel("Time (μs)")
    ax.set_xlabel("Label Size (tags)")
    ax.set_ylim([0, 25])
    ax.set_xlim([0, 21])
    ax.xaxis.set_major_locator(major)
    ax.xaxis.set_minor_locator(minor)
    ax.legend()

    plt.show()
    fig.savefig("micro-vary-labelsize.pdf")

if __name__ == "__main__":
    main()
