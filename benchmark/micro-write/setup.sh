#!/bin/sh

set -eu

# Copy over generated dbp files. This has probably already been run.

# setup_trail LABELSIZE STMTS
setup_trial() {
  local labelsize=$1
  local stmts=$2
  local trial="$labelsize-$stmts"
  local prefix="../../userspace/tdpc/example/bench/$trial"

  mkdir "$trial"
  for suffix in .dp .dbp .id; do
    cp "${prefix}${suffix}" "$trial"
  done
}

max_labelsize=20
max_stmts=20

for i in $(seq 1 "$max_labelsize"); do
  for k in $(seq 1 "$max_stmts"); do
    setup_trial "$i" "$k"
  done
done
