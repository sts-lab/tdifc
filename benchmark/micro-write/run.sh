#!/bin/sh

set -eu

if [ $# -eq 0 ]; then
  echo "usage: $0 output_prefix"
  exit 1
fi

prefix=$1

tdifcfs=/sys/fs/tdifc
dbpload=$tdifcfs/dbp_load_file
benchmark=$tdifcfs/benchmark

# TODO comment me out for no LSM e2e !!!
#mountpoint -q "$tdifcfs" || sudo mount -t tdifcfs tdifcfs "$tdifcfs"

run_trial() {
  if [ $# -eq 0 ]; then
    echo "usage: $0 <trial>"
    exit 1
  fi

  local trial=$1
  echo "starting trial $trial"

  local tdifcctl="$HOME/bin/tdifcctl"
  local polid=$(cat $trial/$trial.id)

  # TODO comment me out for no LSM e2e !!!
  #sudo sh -c "echo -n $PWD/$trial/$trial.dbp > $dbpload"

  sudo "$tdifcctl" setpolid "$polid" main

  rm -f logfile

  ./main "Benchmark: T-DIFC
" >"$trial/${prefix}-e2e" 2>logfile

  # TODO comment me out for DIFC e2e !!!
  #cat "$benchmark" > "$trial/${prefix}-benchmark"
  #echo reset > "$benchmark"
}

for i in $(seq 1 20); do
  for k in $(seq 1 20); do
    run_trial "$i-$k"
  done
done
