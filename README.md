# T-DIFC #

T-DIFC LSM, userspace tools, and benchmarking scripts.

### LSM Setup ###

The T-DIFC LSM, in directory `lsm/tdifc`, was developed for Linux 5.4. The
code is designed to be self-contained, so it can simply be dropped into
`security` in a Linux source tree. Note that you will also need to add the
following line to `security/Kconfig`:

```
source "security/difc/Kconfig"
```

and the following to `security/Makefile`:

```
subdir-$(CONFIG_SECURITY_TDIFC) += tdifc

# ...

obj-$(CONFIG_SECURITY_TDIFC) += tdifc/
```

### Userspace Tools ###

The general T-DIFC administration utility, `tdifcctl`, is in
`userspace/tdifcctl`. This utility is an all-in-one binary, currently with 3
modules to get or set a program's policy ID, and to get a file's
human-readable label.

The T-DIFC policy compiler is in `userspace/tdpc`; see the included README
there for more details.
