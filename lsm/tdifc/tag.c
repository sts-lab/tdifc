#include "debug.h"

#include <linux/err.h>
#include <linux/limits.h>
#include <linux/overflow.h>
#include <linux/rhashtable.h>
#include <linux/slab.h>
#include <linux/stddef.h>
#include <linux/string.h>

#include "tag.h"
#include "cred.h"
#include "hashfns.h"
#include "policy.h"
#include "regex.h"

void difc_tagvec_fragment_free(struct difc_tagvec_fragment *frag)
{
	if (frag->index == REGEX_MAX_CAPTURES)
		kfree(frag->string);
}

void difc_tagvec_free(struct difc_tagvec *vec)
{
	size_t i;
	for (i = 0; i != vec->n_frags; ++i)
		difc_tagvec_fragment_free(&vec->frags[i]);
	kfree(vec->frags);
}

void difc_tagvec_printk(const struct difc_tagvec *vec) {
	size_t i;
	pr_info("tagvec\n");
	for (i = 0; i != vec->n_frags; ++i) {
		if (vec->frags[i].index == REGEX_MAX_CAPTURES)
			pr_info("%.*s\n", (int) vec->frags[i].length,
				  vec->frags[i].string);
		else
			pr_info("capture %u\n", vec->frags[i].index);
	}
}


/*
 * id -> tag
 * To save space, this just points to the corresponding difc_tag_entry, rather
 * than having duplicated strings.
 */
struct difc_tag_id_entry {
	struct rhash_head chain;
	struct difc_tag_id tag_id; /* tag_entry's key address is unknown */
	struct difc_tag_entry *tag_entry;
};

/* Order is ns.type -> ns.id -> string */
static int difc_tag_entry_cmp(struct rhashtable_compare_arg *arg,
			      const void *obj)
{
	const struct difc_tag *k = arg->key;
	const struct difc_tag_entry *e = obj;
	int cmp = u32cmp(k->ns.type, e->tag.ns.type);
	if (cmp != 0)
		return cmp;
	cmp = u32cmp(k->ns.id, e->tag.ns.id);
	if (cmp != 0)
		return cmp;
	cmp = u32cmp(k->length, e->tag.length);
	if (cmp != 0)
		return cmp;
	/* k->length == y->length */
	return strncmp(k->string, e->tag.string, k->length);
}

/* Order is purely by tag_id */
static int difc_tag_id_entry_cmp(struct rhashtable_compare_arg *arg,
				 const void *obj)
{
	const struct difc_tag_id *k = arg->key;
	const struct difc_tag_id_entry *e = obj;
	return u32cmp(k->id, e->tag_id.id);
}

static struct rhashtable tag_table; /* difc_tag -> difc_tag_entry */
static const struct rhashtable_params tag_params = {
	.key_len = sizeof(struct difc_tag),
	.key_offset = offsetof(struct difc_tag_entry, tag),
	.head_offset = offsetof(struct difc_tag_entry, chain),
	.hashfn = &difc_hash_tag,
	.obj_cmpfn = &difc_tag_entry_cmp,
};

static struct rhashtable id_table;  /* difc_tag_id -> difc_tag_id_entry */
static const struct rhashtable_params id_params = {
	.key_len = sizeof(struct difc_tag_id),
	.key_offset = offsetof(struct difc_tag_id_entry, tag_id),
	.head_offset = offsetof(struct difc_tag_id_entry, chain),
	.hashfn = &difc_hash_tag_id,
	.obj_cmpfn = &difc_tag_id_entry_cmp,
};

static struct difc_tag_id next_free_id;  /* increments linearly */

int __init difc_tag_table_init(void)
{
	int ret;
	next_free_id.id = 0;
	ret = rhashtable_init(&tag_table, &tag_params);
	if (ret != 0)
		return ret;
	return rhashtable_init(&id_table, &id_params);
}

const struct difc_tag_entry *difc_map_tag(const struct difc_tag *tag,
					  struct difc_policy_id pol_id,
					  unsigned int cap_flags)
{
	const struct difc_tag_entry *ret = NULL;
	int tmp;
	struct difc_tag_id_entry *ie;
	struct difc_tag_entry *te;

	te = rhashtable_lookup_fast(&tag_table, tag, tag_params);
	if (likely(te)) {
		return te;
	}

	te = kmalloc(sizeof(struct difc_tag_entry), GFP_KERNEL);
	if (unlikely(!te)) {
		return ERR_PTR(-ENOMEM);
	}
	if (tag->length) {
		te->tag.string = kmalloc(tag->length, GFP_KERNEL);
		if (unlikely(!te)) {
			ret = ERR_PTR(-ENOMEM);
			goto free_te;
		}
	} else {
		te->tag.string = NULL;
	}
	ie = kmalloc(sizeof(struct difc_tag_id_entry), GFP_KERNEL);
	if (unlikely(!ie)) {
		ret = ERR_PTR(-ENOMEM);
		goto free_ie;
	}

	memcpy((void *) te->tag.string, tag->string, tag->length);
	te->tag.length = tag->length;
	te->tag.ns = tag->ns;
	te->owner = pol_id;
	te->tag_id = next_free_id;

	ie->tag_id = te->tag_id;
	ie->tag_entry = te;

	/* technically, this allows 1 less tag than possible */
	if (unlikely(check_add_overflow(next_free_id.id, 1u,
					&next_free_id.id))) {
		ret = ERR_PTR(-EOVERFLOW);
		goto free_ie;
	}
	if (unlikely(rhashtable_insert_fast(&tag_table, &te->chain,
					    tag_params) != 0)) {
		ret = ERR_PTR(-ENOMEM);
		goto revert_next;
	}
	if (unlikely(rhashtable_insert_fast(&id_table, &ie->chain,
					    id_params) != 0)) {
		ret = ERR_PTR(-ENOMEM);
		goto remove_te;
	}
	tmp = difc_cap_new(pol_id, te->tag_id, cap_flags);
	if (tmp < 0) {
		ret = ERR_PTR(tmp);
		goto remove_ie;
	} else if (tmp == 1) {
		DIFC_BUG("capability already exists\n");
	}

	return te;

remove_ie:
	rhashtable_remove_fast(&id_table, &ie->chain, id_params);
remove_te:
	rhashtable_remove_fast(&tag_table, &te->chain, tag_params);
revert_next:
	--next_free_id.id;
free_ie:
	kfree(ie);
	kfree(te->tag.string);
free_te:
	kfree(te);
	return ret;
}

const struct difc_tag_entry *difc_map_tagvec(const struct difc_tagvec *vec,
					     struct difc_policy_id pol_id,
					     unsigned int cap_flags)
{
	const struct difc_tag_entry *ret = NULL;
	int tmp;
	struct difc_tag_id_entry *ie;
	struct difc_tag_entry *te;

	te = kmalloc(sizeof(struct difc_tag_entry), GFP_KERNEL);
	if (unlikely(!te)) {
		return ERR_PTR(-ENOMEM);
	}
	if (vec->n_frags != 0) {
		size_t length = 0, i;
		const char *p;
		for (i = 0; i != vec->n_frags; ++i)
			length += vec->frags[i].length;

		te->tag.string = kmalloc(length, GFP_KERNEL);
		if (unlikely(!te)) {
			ret = ERR_PTR(-ENOMEM);
			goto free_te;
		}

		p = te->tag.string;
		for (i = 0; i != vec->n_frags; ++i) {
			struct difc_tagvec_fragment *frag = &vec->frags[i];
			memcpy((void *) p, frag->string, frag->length);
			p += frag->length;
		}

		te->tag.length = length;
	} else {
		te->tag.string = NULL;
		te->tag.length = 0;
	}
	te->tag.ns = vec->ns;

	ret = rhashtable_lookup_fast(&tag_table, &te->tag, tag_params);
	if (likely(ret)) {
		goto free_string;
	}

	ie = kmalloc(sizeof(struct difc_tag_id_entry), GFP_KERNEL);
	if (unlikely(!ie)) {
		ret = ERR_PTR(-ENOMEM);
		goto free_ie;
	}

	te->owner = pol_id;
	te->tag_id = next_free_id;

	ie->tag_id = next_free_id;
	ie->tag_entry = te;

	/* technically, this allows 1 less tag than possible */
	if (unlikely(check_add_overflow(next_free_id.id, 1u,
					&next_free_id.id))) {
		ret = ERR_PTR(-EOVERFLOW);
		goto free_ie;
	}
	if (unlikely(rhashtable_insert_fast(&tag_table, &te->chain,
					    tag_params) != 0)) {
		ret = ERR_PTR(-ENOMEM);
		goto revert_next;
	}
	if (unlikely(rhashtable_insert_fast(&id_table, &ie->chain,
					    id_params) != 0)) {
		ret = ERR_PTR(-ENOMEM);
		goto remove_te;
	}
	tmp = difc_cap_new(pol_id, te->tag_id, cap_flags);
	if (tmp < 0) {
		ret = ERR_PTR(tmp);
		goto remove_ie;
	} else if (tmp == 1) {
		DIFC_BUG("capability already exists\n");
	}

	return te;

remove_ie:
	rhashtable_remove_fast(&id_table, &ie->chain, id_params);
remove_te:
	rhashtable_remove_fast(&tag_table, &te->chain, tag_params);
revert_next:
	--next_free_id.id;
free_ie:
	kfree(ie);
free_string:
	kfree(te->tag.string);
free_te:
	kfree(te);
	return ret;
}

const struct difc_tag_entry *difc_query_tagvec(const struct difc_tagvec *vec)
{
	const struct difc_tag_entry *ret;
	struct difc_tag tag;
	size_t i;
	const char *p;

	tag.length = 0;
	for (i = 0; i != vec->n_frags; ++i)
		tag.length += vec->frags[i].length;

	tag.string = kmalloc(tag.length, GFP_KERNEL);
	if (unlikely(!tag.string))
		return ERR_PTR(-ENOMEM);

	p = tag.string;
	for (i = 0; i != vec->n_frags; ++i) {
		struct difc_tagvec_fragment *frag = &vec->frags[i];
		memcpy((void *) p, frag->string, frag->length);
		p += frag->length;
	}

	tag.ns = vec->ns;
	ret = rhashtable_lookup_fast(&tag_table, &tag, tag_params);

	kfree(tag.string);
	return ret;
}

const struct difc_tag_entry *difc_query_tag_id(struct difc_tag_id tag_id)
{
	const struct difc_tag_id_entry *ie;
	ie = rhashtable_lookup_fast(&id_table, &tag_id, id_params);
	return ie->tag_entry;
}
