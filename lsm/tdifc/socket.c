#include "debug.h"

#include <linux/spinlock.h>
#include <net/sock.h>

#include "socket.h"
#include "cap.h"
#include "cred.h"
#include "inode.h"
#include "policy.h"

int difc_socket_post_create(struct socket *sock, int family, int type,
			    int protocol, int kern)
{
	//struct difc_cred *cc = difc_cred(current_cred());
	//struct difc_inode *ic = difc_inode(SOCK_INODE(sock));

	// this is already handled by inode_alloc_security
	//spin_lock(&ic->lock);
	//ic->type = kern ? DIFC_INODE_KERNEL : DIFC_INODE_SOCKET;
	//if (!cc->policy)
	//	ic->label = difc_blank_label();
	//else
	//	ic->label = difc_label(cc->policy->ns);
	//spin_unlock(&ic->lock);

	return 0;
}

/**
 * Return true iff @src -> @dst has permission to declassify any tags of @src
 * that are not already in @dst.
 */
static inline bool difc_declassify_perm(const struct difc_label *dst,
					const struct difc_label *src)
{
	bool ret = true;
	struct difc_label diff = difc_label(src->ns);
	struct rb_node *rb;

	difc_label_union(&diff, src);
	difc_label_diff(&diff, dst);

	rb = rb_first_cached(&diff.rtags);
	while (rb) {
		struct difc_rtag *rtag;
		uint32_t i;
		struct difc_tag_id id;

		rtag = rb_entry(rb, struct difc_rtag, rb_node);
		difc_rtag_for_each(i, id, rtag) {
			struct difc_cred *cc = difc_cred(current_cred());
			unsigned int flags;
			flags = difc_eff_cap_flags(cc, &cc->cap_mask, id);
			if (!(flags & DIFC_CAP_MINUS)) {
				ret = false;
				goto out;
			}
		}

		rb = rb_next(rb);
	}

out:
	difc_label_free(&diff);
	return ret;
}

int difc_socket_sendmsg(struct socket *sock, struct msghdr *msg, int size)
{
	struct difc_cred *cc = difc_cred(current_cred());
	struct difc_inode *ic = difc_inode(SOCK_INODE(sock));
	int ret;

	//spin_lock(&ic->lock);
	if (!cc->policy ||
	    (difc_flow_perm(&cc->policy->sock_fpol, &ic->label, &cc->label)
	     && difc_declassify_perm(&ic->label, &cc->label))) {
		difc_label_union(&ic->label, &cc->label);
		ret = 0;
	} else {
		ret = -EPERM;
	}
	//spin_unlock(&ic->lock);
	return ret;
}

int difc_socket_recvmsg(struct socket *sock, struct msghdr *msg, int size,
			int flags)
{
	struct difc_cred *cc = difc_cred(current_cred());
	struct difc_inode *ic = difc_inode(SOCK_INODE(sock));

	//spin_lock(&ic->lock);
	difc_label_union(&cc->label, &ic->label);
	//spin_unlock(&ic->lock);

	return 0;
}
