#include "debug.h"

#include <linux/err.h>
#include <linux/rhashtable.h>
#include <linux/slab.h>

#include "cap.h"
#include "cred.h"
#include "hashfns.h"
#include "policy.h"
#include "tag.h"

/*
 * Must also track the initial flags first set by the policy. These are stored
 * with tags on disk, because if a tag is first encountered from a file, its
 * initial capabilities may be otherwise impossible to find; e.g., if the tag
 * originated from a regex submatch.
 */
struct difc_cap_entry {
	struct rhash_head chain;
	struct difc_cap cap;
	unsigned int initial_flags; /* initial flags set by the policy */
};

static int difc_cap_entry_cmp(struct rhashtable_compare_arg *arg,
			      const void *obj)
{
	const struct difc_tag_id *k = arg->key;
	const struct difc_cap_entry *e = obj;
	return u32cmp(k->id, e->cap.tag_id.id);
}

/*
 * Global capability set, represented as a hashtable. (Likely to be large,
 * and only one exists, so this makes more sense than a tree.)
 */
static struct rhashtable global_caps; /* difc_tag_id -> difc_cap_entry */
static const struct rhashtable_params cap_params = {
	.key_len = sizeof(struct difc_tag_id),
	.key_offset = offsetof(struct difc_cap_entry, cap.tag_id),
	.head_offset = offsetof(struct difc_cap_entry, chain),
	.hashfn = &difc_hash_tag_id,
	.obj_cmpfn = &difc_cap_entry_cmp,
};

int __init difc_cap_table_init(void)
{
	return rhashtable_init(&global_caps, &cap_params);
}

int difc_cap_new(struct difc_policy_id pol_id, struct difc_tag_id tag_id,
		 unsigned int flags)
{
	/* expect capability to not exist */
	struct difc_cap_entry *entry;
	struct difc_cap_entry *new;

	new = kmalloc(sizeof(struct difc_cap_entry), GFP_KERNEL);
	new->cap.pol_id = pol_id;
	new->cap.tag_id = tag_id;
	new->cap.flags = new->initial_flags = flags;

	entry = rhashtable_lookup_get_insert_fast(&global_caps, &new->chain,
						  cap_params);
	if (likely(entry == NULL))
		return 0;

	kfree(new);
	if (IS_ERR(entry))
		return PTR_ERR(entry);
	else
		return 1;
}

struct difc_cap *difc_cap_get(struct difc_policy_id pol_id,
			      struct difc_tag_id tag_id) {
	struct difc_cap_entry *entry;
	entry = rhashtable_lookup_fast(&global_caps, &tag_id, cap_params);
	if (!entry || entry->cap.pol_id.id != pol_id.id)
		return NULL;
	return &entry->cap;
}

unsigned int difc_cap_initial_flags(struct difc_tag_id tag_id)
{
	struct difc_cap_entry *entry;
	entry = rhashtable_lookup_fast(&global_caps, &tag_id, cap_params);
	if (!entry)
		return 0;
	return entry->initial_flags;
}

int difc_cap_mask_union(struct difc_cap_mask *out,
			const struct difc_cap_mask *mask) {
	struct rb_node *rb = rb_first_cached(&mask->rcaps);
	while (rb) {
		int ret;
		struct difc_rcap *rcap;
		rcap = rb_entry(rb, struct difc_rcap, rb_node);
		if ((ret = difc_cap_mask_insert(out, &rcap->cap)) < 0)
			return ret;
		rb = rb_next(rb);
	}
	out->drop_rest = mask->drop_rest;
	return 0;
}

/*
 * Callback to set @dst, possibly using the value of @src.
 * difc_cap_mask_find will return the same value as callback.
 */
typedef int (*cap_callback_t)(struct difc_cap *dst,
			      const struct difc_cap *src);

/**
 * Helper for various options. If a capability with the same tag_id and pol_id
 * for @cap is found, call @found. Otherwise, call @miss.
 */
static int difc_cap_mask_find(struct difc_cap_mask *mask,
			      const struct difc_cap *cap,
			      cap_callback_t found, cap_callback_t miss)
{
	struct rb_node *parent = NULL, **link = &mask->rcaps.rb_root.rb_node;
	struct difc_rcap *dst;
	bool leftmost = true;
	while (*link) {
		parent = *link;
		dst = rb_entry(parent, struct difc_rcap, rb_node);
		if (dst->cap.tag_id.id > cap->tag_id.id) {
			link = &parent->rb_left;
		} else if (dst->cap.tag_id.id < cap->tag_id.id) {
			link = &parent->rb_right;
			leftmost = false;
		} else {
			return found(&dst->cap, cap);
		}
	}
	dst = kmalloc(sizeof(struct difc_rcap), GFP_KERNEL);
	if (unlikely(!dst))
		return -ENOMEM;
	miss(&dst->cap, cap);
	rb_link_node(&dst->rb_node, parent, link);
	rb_insert_color_cached(&dst->rb_node, &mask->rcaps, leftmost);
	return 0;
}

static int insert_found(struct difc_cap *dst, const struct difc_cap *src)
{
	dst->flags = src->flags;
	return 0;
}

static int insert_miss(struct difc_cap *dst, const struct difc_cap *src)
{
	*dst = *src;
	return 0;
}

int difc_cap_mask_insert(struct difc_cap_mask *mask,
			 const struct difc_cap *cap)
{
	return difc_cap_mask_find(mask, cap, &insert_found, &insert_miss);
}

static int or_found(struct difc_cap *dst, const struct difc_cap *src)
{
	dst->flags |= src->flags;
	return 0;
}

static int or_miss(struct difc_cap *dst, const struct difc_cap *src)
{
	*dst = *src;
	dst->flags = DIFC_CAP_ALL;
	return 0;
}

int difc_cap_mask_or(struct difc_cap_mask *mask, const struct difc_cap *cap)
{
	return difc_cap_mask_find(mask, cap, &or_found, &or_miss);
}

static int and_found(struct difc_cap *dst, const struct difc_cap *src)
{
	dst->flags &= src->flags;
	return 0;
}

static int and_miss(struct difc_cap *dst, const struct difc_cap *src)
{
	*dst = *src;
	return 0;
}

int difc_cap_mask_and(struct difc_cap_mask *mask, const struct difc_cap *cap)
{
	return difc_cap_mask_find(mask, cap, &and_found, &and_miss);
}

void difc_cap_mask_free(struct difc_cap_mask *mask)
{
	struct difc_rcap *cap, *next;
	struct rb_root *root = &mask->rcaps.rb_root;
	rbtree_postorder_for_each_entry_safe(cap, next, root, rb_node) {
		kfree(cap);
	}
}

unsigned int difc_eff_cap_flags(const struct difc_cred *cc,
				const struct difc_cap_mask *mask,
				struct difc_tag_id tag_id)
{
	unsigned int def_flags = DIFC_CAP_NONE;
	struct rb_node *rb;

	if (cc->policy) {
		const struct difc_tag_entry *te = difc_query_tag_id(tag_id);
		if (te && te->owner.id == cc->policy->pol_id.id)
			def_flags = DIFC_CAP_ALL;
	}
	if (def_flags == DIFC_CAP_NONE) {
		struct difc_cap_entry *ce;
		ce = rhashtable_lookup_fast(&global_caps, &tag_id,
					    cap_params);
		def_flags = ce->cap.flags;
	}

	rb = mask->rcaps.rb_root.rb_node;
	while (rb) {
		struct difc_cap *cap;
		cap = &rb_entry(rb, struct difc_rcap, rb_node)->cap;
		if (cap->tag_id.id > tag_id.id)
			rb = rb->rb_left;
		else if (cap->tag_id.id < tag_id.id)
			rb = rb->rb_right;
		else
			return cap->flags & def_flags;
	}
	if (mask->drop_rest)
		return DIFC_CAP_NONE;
	else
		return def_flags;
}
