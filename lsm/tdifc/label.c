#include "debug.h"

#include <linux/stddef.h>
#include <linux/string.h>
#include <linux/slab.h>

#include "label.h"
#include "cap.h"
#include "cred.h"
#include "tag.h"

/**
 * Extract the most significant bits from @id.
 */
static uint32_t difc_msbs(struct difc_tag_id id)
{
	return id.id >> DIFC_RTAG_BITS;
}

/**
 * Extract the least significant bits from @id.
 */
static uint32_t difc_lsbs(struct difc_tag_id id)
{
	return id.id & DIFC_RTAG_MASK;
}

/**
 * Set @tag->lsbs to include @id.
 */
static void difc_rtag_set(struct difc_rtag *tag, struct difc_tag_id id)
{
	/*
	 * trying to use bitops here causes page faults, presumably because
	 * the bitvector lsbs is only 32bit, while the atomic ops expect a
	 * 64bit long
	 */
	tag->lsbs |= 1 << difc_lsbs(id);
}

/**
 * Clear @id from @tag->lsbs.
 */
static void difc_rtag_clear(struct difc_rtag *tag, struct difc_tag_id id)
{
	tag->lsbs &= ~(1 << difc_lsbs(id));
}

/**
 * Return whether @tag->lsbs contains the least significant bits of @id.
 */
static bool difc_rtag_test(struct difc_rtag *tag, struct difc_tag_id id)
{
	//return test_bit(difc_lsbs(id), (unsigned long *) &tag->lsbs);
	return tag->lsbs & (1 << difc_lsbs(id));
}

/**
 * Allocate a new difc_rtag for @id.
 */
static struct difc_rtag *difc_rtag_from_id(struct difc_tag_id id)
{
	struct difc_rtag *tag = kmalloc(sizeof(struct difc_rtag), GFP_KERNEL);
	if (likely(tag)) {
		tag->lsbs = 0;
		difc_rtag_set(tag, id);
		tag->msbs = difc_msbs(id);
	}
	return tag;
}

void difc_label_free(struct difc_label *label)
{
	struct difc_rtag *tag, *next;
	struct rb_root *root = &label->rtags.rb_root;
	rbtree_postorder_for_each_entry_safe(tag, next, root, rb_node) {
		kfree(tag);
	}
}

int difc_label_add(struct difc_label *label, struct difc_tag_id id)
{
	uint32_t msbs = difc_msbs(id);
	struct rb_node *parent = NULL, **link = &label->rtags.rb_root.rb_node;
	struct difc_rtag *tag;
	bool leftmost = true;
	while (*link) {
		parent = *link;
		tag = rb_entry(parent, struct difc_rtag, rb_node);
		if (tag->msbs > msbs) {
			link = &parent->rb_left;
		} else if (tag->msbs < msbs) {
			link = &parent->rb_right;
			leftmost = false;
		} else {
			if (difc_rtag_test(tag, id))
				return 1;
			difc_rtag_set(tag, id);
			return 0;
		}
	}
	tag = difc_rtag_from_id(id);
	if (unlikely(!tag))
		return -ENOMEM;
	rb_link_node(&tag->rb_node, parent, link);
	rb_insert_color_cached(&tag->rb_node, &label->rtags, leftmost);
	return 0;
}

/**
 * Union @src into @label. This could potentially create a node.
 * Return 0 on success, or some error code < 0 on failure.
 */
static int difc_rtag_union(struct difc_label *label,
			   const struct difc_rtag *src)
{
	struct rb_node *parent = NULL, **link = &label->rtags.rb_root.rb_node;
	struct difc_rtag *dst;
	bool leftmost = true;
	while (*link) {
		parent = *link;
		dst = rb_entry(parent, struct difc_rtag, rb_node);
		if (dst->msbs > src->msbs) {
			link = &parent->rb_left;
		} else if (dst->msbs < src->msbs) {
			link = &parent->rb_right;
			leftmost = false;
		} else {
			dst->lsbs |= src->lsbs;
			return 0;
		}
	}
	dst = kmalloc(sizeof(struct difc_rtag), GFP_KERNEL);
	if (unlikely(!dst))
		return -ENOMEM;
	dst->msbs = src->msbs;
	dst->lsbs = src->lsbs;
	rb_link_node(&dst->rb_node, parent, link);
	rb_insert_color_cached(&dst->rb_node, &label->rtags, leftmost);
	return 0;
}

int difc_label_union(struct difc_label *out, const struct difc_label *label)
{
	struct rb_node *rb = rb_first_cached(&label->rtags);
	while (rb) {
		int ret;
		struct difc_rtag *tag;
		tag = rb_entry(rb, struct difc_rtag, rb_node);
		if ((ret = difc_rtag_union(out, tag)) < 0)
			return ret;
		rb = rb_next(rb);
	}
	return 0;
}

/**
 * Find the difc_rtag with @id in @label, or NULL if no such difc_rtag exists.
 */
static struct difc_rtag *difc_rtag_find(struct difc_label *label,
					struct difc_tag_id id)
{
	uint32_t msbs = difc_msbs(id);
	struct rb_node *rb = label->rtags.rb_root.rb_node;
	while (rb) {
		struct difc_rtag *tag;
		tag = rb_entry(rb, struct difc_rtag, rb_node);
		if (tag->msbs > msbs)
			rb = rb->rb_left;
		else if (tag->msbs < msbs)
			rb = rb->rb_right;
		else
			return tag;
	}
	return NULL;
}

bool difc_label_contains(struct difc_label *label, struct difc_tag_id id)
{
	struct difc_rtag *rtag = difc_rtag_find(label, id);
	return rtag && difc_rtag_test(rtag, id);
}

/* This removes the node if id was the sole member. */
int difc_label_remove(struct difc_label *label, struct difc_tag_id id)
{
	struct difc_rtag *tag = difc_rtag_find(label, id);
	if (!tag || difc_rtag_test(tag, id))
		return 1;
	difc_rtag_clear(tag, id);
	if (!tag->lsbs) {
		rb_erase_cached(&tag->rb_node, &label->rtags);
		kfree(tag);
	}
	return 0;
}

int difc_label_diff(struct difc_label *out, const struct difc_label *label)
{
	struct rb_node *rb = rb_first_cached(&label->rtags);
	while (rb) {
		struct difc_rtag *rtag;
		uint32_t i;
		struct difc_tag_id id;

		rtag = rb_entry(rb, struct difc_rtag, rb_node);
		difc_rtag_for_each(i, id, rtag) {
			difc_label_remove(out, id);
		}

		rb = rb_next(rb);
	}

	return 0;
}

int difc_label_diff_checked(const struct difc_cred *cred,
			    const struct difc_cap_mask *mask,
			    struct difc_label *out,
			    const struct difc_label *label)
{
	struct rb_node *rb = rb_first_cached(&label->rtags);
	while (rb) {
		struct difc_rtag *rtag;
		uint32_t i;
		struct difc_tag_id id;

		rtag = rb_entry(rb, struct difc_rtag, rb_node);
		difc_rtag_for_each(i, id, rtag) {
			if (DIFC_CAP_MINUS &
			    difc_eff_cap_flags(cred, mask, id)) {
				difc_label_remove(out, id);
			} else {
				pr_err("del denied: no - capability\n");
			}
		}

		rb = rb_next(rb);
	}

	return 0;
}

int difc_label_size(const struct difc_label *label, struct difc_ns ns)
{
	int n = 0;
	struct rb_node *rb = rb_first_cached(&label->rtags);
	while (rb) {
		struct difc_rtag *rtag;
		uint32_t i;
		struct difc_tag_id id;

		rtag = rb_entry(rb, struct difc_rtag, rb_node);
		difc_rtag_for_each(i, id, rtag) {
			const struct difc_tag_entry *te = difc_query_tag_id(id);
			if (!te) {
				pr_err("unrecognized tag id %u\n", id.id);
				continue;
			}
			if (difc_ns_equal(ns, te->tag.ns))
				++n;
		}

		rb = rb_next(rb);
	}
	return n;
}
