#include "debug.h"

#include <linux/lsm_hooks.h>
#include <linux/security.h>

#include "cap.h"
#include "cred.h"
#include "file.h"
#include "inode.h"
#include "policy.h"
#include "socket.h"
#include "tag.h"
#include "write_hook.h"

struct lsm_blob_sizes difc_blob_sizes __lsm_ro_after_init = {
	.lbs_cred = sizeof(struct difc_cred),
	.lbs_inode = sizeof(struct difc_inode),
};

static struct security_hook_list difc_security_hooks[] __lsm_ro_after_init = {
	LSM_HOOK_INIT(bprm_set_creds, difc_bprm_set_creds),
	LSM_HOOK_INIT(cred_alloc_blank, difc_cred_alloc_blank),
	LSM_HOOK_INIT(cred_free, difc_cred_free),
	LSM_HOOK_INIT(cred_prepare, difc_cred_prepare),
	LSM_HOOK_INIT(cred_transfer, difc_cred_transfer),

	LSM_HOOK_INIT(inode_alloc_security, difc_inode_alloc_security),
	LSM_HOOK_INIT(inode_free_security, difc_inode_free_security),

	LSM_HOOK_INIT(socket_post_create, difc_socket_post_create),
	LSM_HOOK_INIT(socket_sendmsg, difc_socket_sendmsg),
	LSM_HOOK_INIT(socket_recvmsg, difc_socket_recvmsg),

	LSM_HOOK_INIT(file_permission, difc_file_permission),
};

static __init int difc_init(void)
{
	difc_tag_table_init();
	difc_cap_table_init();
	difc_policy_table_init();

	security_add_hooks(difc_security_hooks,
			   ARRAY_SIZE(difc_security_hooks), "tdifc");
	hook_syswrite();

	pr_info("initialized\n");
	return 0;
}

DEFINE_LSM(difc) = {
	.init = difc_init,
	.name = "tdifc",
	.blobs = &difc_blob_sizes,
};
