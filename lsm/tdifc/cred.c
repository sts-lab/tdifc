#include "debug.h"

#include <asm/byteorder.h>
#include <linux/binfmts.h>
#include <linux/fs.h>
#include <linux/xattr.h>

#include "cred.h"
#include "file.h"
#include "policy.h"
#include "xattr.h"

int difc_cred_alloc_blank(struct cred *c, gfp_t gfp)
{
	struct difc_cred *cc = difc_cred(c);
	cc->policy = NULL;
	cc->label = difc_blank_label();
	cc->cap_mask = difc_cap_mask();
	return 0;
}

void difc_cred_free(struct cred *c)
{
	struct difc_cred *cc = difc_cred(c);
	if (!cc->policy)
		return;
	difc_label_free(&cc->label);
	difc_cap_mask_free(&cc->cap_mask);
}

static void difc_cred_copy(struct difc_cred *nc, struct difc_cred *oc)
{
	nc->policy = oc->policy;
	if (!nc->policy)
		return;

	nc->label = difc_label(nc->policy->ns);
	difc_label_union(&nc->label, &oc->label);

	nc->cap_mask = difc_cap_mask();
	difc_cap_mask_union(&nc->cap_mask, &oc->cap_mask);
}

int difc_cred_prepare(struct cred *new, const struct cred *old, gfp_t gfp)
{
	struct difc_cred *nc = difc_cred(new);
	struct difc_cred *oc = difc_cred(old);

	difc_cred_copy(nc, oc);
	return 0;
}

void difc_cred_transfer(struct cred *new, const struct cred *old)
{
	struct difc_cred *nc = difc_cred(new);
	struct difc_cred *oc = difc_cred(old);

	difc_cred_copy(nc, oc);
}

int difc_bprm_set_creds(struct linux_binprm *bprm)
{
	int ret;
	struct difc_cred *cc;
	struct difc_policy_id id;

	if (bprm->called_set_creds)
		return 0;
	cc = difc_cred(bprm->cred);

	ret = difc_file_get_policy_id(&id, bprm->file);
	if (ret == -ENODATA || ret == -EOPNOTSUPP) {
		cc->policy = NULL;
	} else if (ret < 0) {
		pr_err("invalid policy ID for %s (ret=%d)\n",
			 bprm->filename, ret);
		cc->policy = NULL;
	} else {
		cc->policy = difc_policy_get(id);
		if (!cc->policy) {
			pr_err("no loaded policy with id %u\n", id.id);
		} else {
			cc->label.ns = cc->policy->ns;
			difc_policy_do_init(cc->policy, cc);
		}
	}
	return 0;
}

