#include "debug.h"

#include <linux/file.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/rhashtable.h>

#include "policy.h"
#include "hashfns.h"

void difc_log_loc_free(struct difc_log_loc *lloc)
{
	if (lloc->type == DIFC_LOG_PATH)
		kfree(lloc->path);
}

bool difc_flow_perm(const struct difc_flow_policy *fpol,
		    const struct difc_label *dst,
		    const struct difc_label *src)
{
	bool ret;
	struct difc_label join = difc_label(src->ns);

	difc_label_union(&join, src);
	difc_label_union(&join, dst);

	ret = difc_label_size(&join, src->ns) <= fpol->max_size;

	difc_label_free(&join);
	return ret;
}

void difc_policy_free(struct difc_policy *policy)
{
	size_t i;

	difc_log_loc_free(&policy->lloc);

	for (i = 0; i != policy->n_actions; ++i)
		difc_action_free(&policy->actions[i]);
	kfree(policy->actions);

	for (i = 0; i != policy->n_init; ++i)
		difc_policy_block_free(&policy->init[i]);
	kfree(policy->init);
}

struct difc_policy_entry {
	struct rhash_head chain;
	struct difc_policy policy;
};

/* order is purely by policy_id, since policy_id is unique */
static int difc_policy_entry_cmp(struct rhashtable_compare_arg *arg,
				 const void *obj)
{
	const struct difc_policy_id *k = arg->key;
	const struct difc_policy_entry *e = obj;
	return u32cmp(k->id, e->policy.pol_id.id);
}

static struct rhashtable difc_policy_table; /* difc_policy_id -> difc_policy_entry */
static const struct rhashtable_params policy_params = {
	.key_len = sizeof(struct difc_policy_id),
	.key_offset = offsetof(struct difc_policy_entry, policy.pol_id),
	.head_offset = offsetof(struct difc_policy_entry, chain),
	.hashfn = &difc_hash_policy_id,
	.obj_cmpfn = &difc_policy_entry_cmp,
};

bool difc_fd_is_log(struct difc_log_loc lloc, int fd)
{
	bool ret;
	struct fd f;

	switch (lloc.type) {
	case DIFC_LOG_FD:
		return lloc.fd == fd;
	case DIFC_LOG_PATH:
		f = fdget_pos(fd);
		if (!f.file)
			return false;
		ret = strcmp(f.file->f_path.dentry->d_iname, lloc.path) == 0;
		fdput_pos(f);
		return ret;
	}
	DIFC_BUG("missing difc_log_type case");
}

void difc_policy_do_init(const struct difc_policy *pol,
			 struct difc_cred *cred)
{
	size_t i;
	for (i = 0; i != pol->n_init; ++i) {
		/* init has no captures, therefore NULL cgs is okay */
		difc_execute_block(pol, cred, &pol->init[i], NULL);
	}
}

int difc_policy_put(struct difc_policy *pol)
{
	struct difc_policy_entry *entry;

	entry = kmalloc(sizeof(struct difc_policy_entry), GFP_KERNEL);
	if (!entry)
		return -ENOMEM;

	entry->policy = *pol;

	return rhashtable_lookup_insert_fast(&difc_policy_table,
					     &entry->chain,
					     policy_params);
}

const struct difc_policy *difc_policy_get(struct difc_policy_id pol_id)
{
	const struct difc_policy_entry *pe;
	pe = rhashtable_lookup_fast(&difc_policy_table, &pol_id,
				    policy_params);
	if (pe == NULL)
		return NULL;
	return &pe->policy;
}

int __init difc_policy_table_init(void)
{
	return rhashtable_init(&difc_policy_table, &policy_params);
}
