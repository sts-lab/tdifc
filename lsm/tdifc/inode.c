#include "inode.h"

int difc_inode_alloc_security(struct inode *inode)
{
	struct difc_inode *ic = difc_inode(inode);
	spin_lock_init(&ic->lock);
	ic->type = DIFC_INODE_DEFAULT;
	ic->label = difc_blank_label();
	return 0;
}

void difc_inode_free_security(struct inode *inode)
{
	struct difc_inode *ic = difc_inode(inode);
	difc_label_free(&ic->label);
}
