#include "debug.h"

#include <linux/err.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/string.h>

#include "dbp.h"
#include "policy.h"
#include "policy_dsl.h"
#include "serialize.h"
#include "regex.h"

/*
 * This checks if (lloc->path == NULL) to decide whether @lloc is currently a
 * path. Therefore, it must be NULL if an FD or uninitialized.
 */
static int parse_log_loc(struct difc_policy *pol, struct difc_log_loc *lloc,
			 const uint8_t **p, loff_t *size)
{
	uint8_t log_type;
	size_t pathlen;
	if (*size < 1)
		return -EINVAL;

	/* free the previous path, if necessary */
	if (lloc->path) {
		kfree(lloc->path);
		lloc->path = NULL;
	}

	log_type = read8(p);
	*size -= 1;

	switch (log_type) {
	case DBP_LOG_FD:
		if (*size < 4)
			return -EINVAL;
		lloc->type = DIFC_LOG_FD;
		lloc->fd = read32(p);
		*size -= 4;
		return 0;

	case DBP_LOG_PATH:
		if (*size < 1)
			return -EINVAL;
		lloc->type = DIFC_LOG_PATH;
		pathlen = strlen((char *) *p) + 1; /* include null char */
		lloc->path = kmalloc(pathlen, GFP_KERNEL);
		if (!lloc->path)
			return -ENOMEM;
		memcpy(lloc->path, *p, pathlen);
		*size -= pathlen;
		*p += pathlen;
		return 0;

	default:
		return -EINVAL;
	}
}

static int parse_log_locs(struct difc_policy *pol, const uint8_t **p,
			  loff_t *size)
{
	uint32_t i, nlocs;

	if (*size < 4)
		return -EINVAL;
	nlocs = read32(p);
	*size -= 4;

	pol->lloc.path = NULL;
	for (i = 0; i != nlocs; ++i) {
		int ret = parse_log_loc(pol, &pol->lloc, p, size);
		if (ret < 0)
			return ret;
	}
	return 0;
}

static int parse_namespace(struct difc_policy *pol, struct difc_ns *ns,
			   const uint8_t **p, loff_t *size)
{
	uint8_t ns_type;

	if (*size < 1)
		return -EINVAL;
	ns_type = read8(p);
	*size -= 1;

	switch (ns_type) {
	case DBP_NS_UNIQUE:
		ns->type = DIFC_NS_UNIQUE;
		ns->id = pol->pol_id.id;
		return 0;
	case DBP_NS_NORMAL:
		ns->type = DIFC_NS_NORMAL;
		if (*size < 4)
			return -EINVAL;
		ns->id = read32(p);
		*size -= 4;
		return 0;
	default:
		return -EINVAL;
	}
}

static int parse_tagvec(struct difc_policy *pol, struct difc_tagvec *vec,
			const uint8_t **p, loff_t *size)
{
	uint8_t n_frags, i;
	int ret;

	memcpy(&vec->ns, &pol->ns, sizeof(struct difc_ns));

	if (*size < 1)
		return -EINVAL;
	n_frags = read8(p);
	*size -= 1;

	vec->n_frags = n_frags;
	if (vec->n_frags == 0) {
		vec->frags = NULL;
		return 0;
	}

	vec->frags = kcalloc(n_frags, sizeof(struct difc_tagvec_fragment),
			     GFP_KERNEL);
	if (!vec->frags)
		return -ENOMEM;

	for (i = 0; *size > 0 && i != n_frags; ++i) {
		uint8_t frag_type = read8(p);
		*size -= 1;
		if (*size < 1)
			return -EINVAL;
		switch (frag_type) {
		case DBP_FRAG_CAPTURE:
			vec->frags[i].index = read8(p);
			*size -= 1;
			if (vec->frags[i].index >= REGEX_MAX_CAPTURES) {
				ret = -EINVAL;
				goto err;
			}
			break;
		case DBP_FRAG_LITERAL:
			vec->frags[i].index = REGEX_MAX_CAPTURES;
			/* don't need null terminator */
			vec->frags[i].length = strlen((char *) *p);
			vec->frags[i].string = kmalloc(vec->frags[i].length,
						       GFP_KERNEL);
			if (!vec->frags[i].string) {
				ret = -ENOMEM;
				goto err;
			}
			memcpy(vec->frags[i].string, *p, vec->frags[i].length);
			*size -= vec->frags[i].length + 1;
			*p += vec->frags[i].length + 1;
			break;
		default:
			ret = -EINVAL;
			goto err;
		}
	}

	return 0;

err:
	/* don't free uninitialized fragments */
	vec->n_frags = i;
	difc_tagvec_free(vec);
	return ret;
}

static int parse_stmt_cap_tagvec(struct difc_policy *pol,
				 struct difc_policy_stmt_data *out,
				 const uint8_t **p, loff_t *size)
{
	if (*size < 4)
		return -EINVAL;
	out->flags = read32(p);
	*size -= 4;

	return parse_tagvec(pol, &out->vec, p, size);
}

static int parse_stmt_cap_tagvecs(struct difc_policy *pol,
				  struct difc_policy_stmt *out,
				  const uint8_t **p, loff_t *size)
{
	size_t i, k;
	int ret;

	if (*size < 4)
		return -EINVAL;
	out->n_data = read32(p);
	*size -= 4;

	out->data = kcalloc(out->n_data, sizeof(struct difc_policy_stmt_data),
			    GFP_KERNEL);
	if (!out->data)
		return -ENOMEM;

	for (i = 0; i != out->n_data; ++i) {
		ret = parse_stmt_cap_tagvec(pol, &out->data[i], p, size);
		if (ret < 0)
			goto err;
	}

	return 0;

err:
	for (k = 0; k != i; ++k)
		difc_policy_stmt_data_free(&out->data[k]);
	return ret;
}

static int parse_stmts(struct difc_policy *pol,
		       struct difc_policy_stmt **out, size_t *n_out,
		       const uint8_t **p, loff_t *size)
{
	int ret;
	uint32_t n_stmts, i, k;
	struct difc_policy_stmt *stmts;

	if (*size < 4)
		return -EINVAL;
	n_stmts = read32(p);
	*size -= 4;

	stmts = kcalloc(n_stmts, sizeof(struct difc_policy_stmt), GFP_KERNEL);
	if (!stmts)
		return -ENOMEM;

	for (i = 0; *size > 0 && i != n_stmts; ++i) {
		uint8_t stmt_type = read8(p);
		*size -= 1;
		switch (stmt_type) {
		case DBP_STMT_ADD_TAGS:
			stmts[i].type = DIFC_ACTION_ADD_TAGS;
			break;
		case DBP_STMT_DEL_TAGS:
			stmts[i].type = DIFC_ACTION_DEL_TAGS;
			break;
		case DBP_STMT_SET_TAGS:
			stmts[i].type = DIFC_ACTION_SET_TAGS;
			break;
		case DBP_STMT_ADD_CAPS:
			stmts[i].type = DIFC_ACTION_ADD_CAPS;
			break;
		case DBP_STMT_DEL_CAPS:
			stmts[i].type = DIFC_ACTION_DEL_CAPS;
			break;
		case DBP_STMT_SET_CAPS:
			stmts[i].type = DIFC_ACTION_SET_CAPS;
			break;
		case DBP_STMT_DROP_CAPS:
			stmts[i].type = DIFC_ACTION_DROP_CAPS;
			break;
		default:
			ret = -EINVAL;
			goto err;
		}
		switch (stmt_type) {
		case DBP_STMT_ADD_TAGS:
		case DBP_STMT_DEL_TAGS:
		case DBP_STMT_SET_TAGS:
		case DBP_STMT_ADD_CAPS:
		case DBP_STMT_DEL_CAPS:
		case DBP_STMT_SET_CAPS:
			ret = parse_stmt_cap_tagvecs(pol, &stmts[i], p, size);
			if (ret < 0)
				goto err;
			break;
		case DBP_STMT_DROP_CAPS:
			break;
		default:
			ret = -EINVAL;
			goto err;
		}
	}

	if (i != n_stmts) {
		pr_err("too few statements\n");
		ret = -EINVAL;
		goto err;
	}

	*out = stmts;
	*n_out = n_stmts;
	return 0;

err:
	/* free up to i */
	for (k = 0; k != i; ++k) {
		difc_policy_stmt_free(&stmts[k]);
	}
	kfree(stmts);
	return ret;
}

static int parse_pids(struct difc_policy *pol,
		      struct difc_policy_pid **out, size_t *n_out,
		      const uint8_t **p, loff_t *size)
{
	uint32_t n_pids, i;
	struct difc_policy_pid *pids;

	if (*size < 4)
		return -EINVAL;
	n_pids = read32(p);
	*size -= 4;

	pids = kcalloc(n_pids, sizeof(struct difc_policy_pid), GFP_KERNEL);
	if (!pids)
		return -ENOMEM;

	for (i = 0; *size > 0 && i != n_pids; ++i) {
		uint8_t pid_type = read8(p);
		*size -= 1;
		switch (pid_type) {
		case DBP_PID_SELF:
			pids[i].type = DIFC_POLICY_BLOCK_PID_SELF;
			break;
		case DBP_PID_PARENT:
			pids[i].type = DIFC_POLICY_BLOCK_PID_PARENT;
			break;
		case DBP_PID_CHILDREN:
			pids[i].type = DIFC_POLICY_BLOCK_PID_CHILDREN;
			break;
		case DBP_PID_CAPTURE:
			pids[i].type = DIFC_POLICY_BLOCK_PID_CAPTURE;
			if (*size < 1)
				goto err;
			pids[i].pid = read8(p);
			*size -= 1;
			if (pids[i].pid < 0 ||
			    pids[i].pid >= REGEX_MAX_CAPTURES)
				goto err;
			break;
		case DBP_PID_LITERAL:
			pids[i].type = DIFC_POLICY_BLOCK_PID_LITERAL;
			if (*size < 4)
				goto err;
			pids[i].pid = read32(p);
			*size -= 4;
			break;
		default:
			goto err;
		}
	}

	*out = pids;
	*n_out = n_pids;
	return 0;

err:
	kfree(pids);
	return -EINVAL;
}

static int parse_block(struct difc_policy *pol, struct difc_policy_block *out,
		       const uint8_t **p, loff_t *size)
{
	int ret = parse_pids(pol, &out->pids, &out->n_pids, p, size);
	if (ret < 0)
		return ret;
	ret = parse_stmts(pol, &out->stmts, &out->n_stmts, p, size);
	if (ret < 0)
		goto err;
	return 0;

err:
	kfree(out->pids);
	return ret;
}

static int parse_match_blocks(struct difc_policy *pol, const uint8_t **p,
			      loff_t *size)
{
	int ret;
	size_t i, k;

	if (*size < 4)
		return -EINVAL;
	pol->n_actions = read32(p);
	*size -= 4;

	if (pol->n_actions > 0) {
		pol->actions = kcalloc(pol->n_actions,
				       sizeof(struct difc_action),
				       GFP_KERNEL);
		if (!pol->actions) {
			pr_err("out of memory\n");
			kfree(pol->actions);
			return -ENOMEM;
		}
	}

	for (i = 0; i != pol->n_actions; ++i) {
		struct difc_action *axn = &pol->actions[i];
		ssize_t nread = regex_deserialize(*p, *size, &axn->regex);
		if (nread < 0) {
			ret = nread;
			goto err;
		}
		*p += nread;
		*size -= nread;

		if (*size < 4) {
			ret = -EINVAL;
			goto err;
		}
		axn->n_blocks = read32(p);
		*size -= 4;

		axn->blocks = kcalloc(axn->n_blocks,
				      sizeof(struct difc_policy_block),
				      GFP_KERNEL);
		if (!axn->blocks) {
			pr_err("out of memory\n");
			ret = -ENOMEM;
			goto err;
		}

		for (k = 0; k != axn->n_blocks; ++k) {
			ret = parse_block(pol, &axn->blocks[k], p, size);
			if (ret < 0) {
				/* don't free uninitialized blocks */
				axn->n_blocks = k;
				difc_action_free(axn);
				goto err;
			}
		}
	}

	return 0;

err:
	for (k = 0; k != i; ++k)
		difc_action_free(&pol->actions[k]);
	pol->n_actions = 0;
	return ret;
}

static int parse_flow_policies(struct difc_policy *pol, const uint8_t **p,
			       loff_t *size)
{
	uint32_t n_pols;
	unsigned int i;
	unsigned long uninit =
			(1 << DBP_FLOW_SOCKET) |
			(1 << DBP_FLOW_PROCESS);

	if (*size < 4)
		return -EINVAL;
	n_pols = read32(p);
	*size -= 4;

	for (i = 0; i != n_pols; ++i) {
		uint8_t type;
		if (*size < 3)
			return -EINVAL;
		type = read8(p);
		*size -= 1;
		switch (type) {
		case DBP_FLOW_SOCKET:
			pol->sock_fpol.max_size = read32(p);
			*size -= 4;
			uninit &= ~(1 << DBP_FLOW_SOCKET);
			break;
		case DBP_FLOW_PROCESS:
			pol->proc_fpol.max_size = read32(p);
			*size -= 4;
			uninit &= ~(1 << DBP_FLOW_PROCESS);
			break;
		default:
			return -EINVAL;
		}
	}

	if (uninit)
		return -EINVAL;

	return 0;
}

struct difc_policy *dbp_read(const char *path)
{
	int ret;
	uint8_t *buf, tmp;
	const uint8_t *p;
	loff_t size;
	struct difc_policy *pol;
	size_t i;
	unsigned long uninit = /* uninitialized tracker; must be 0 by end */
			(1 << DBP_LOG_LOC) |
			(1 << DBP_NAMESPACE) |
			(1 << DBP_MATCH) |
			(1 << DBP_INIT) |
			(1 << DBP_FLOW_POLICY);

	ret = kernel_read_file_from_path(path, (void **) &buf, &size, 0,
					 READING_POLICY);
	if (ret < 0) {
		pr_err("failed to open policy %s (err=%d)\n", path, ret);
		return ERR_PTR(ret);
	}

	p = buf;
	if (size < DBP_HEADER_SIZE) { /* header size */
		ret = -EINVAL;
		goto err_msg;
	}
	tmp = read8(&p);
	if (tmp != DBP_VERSION) {
		ret = -EINVAL;
		goto err_msg;
	}

	pol = kzalloc(sizeof(struct difc_policy), GFP_KERNEL);
	if (!pol) {
		pr_err("out of memory\n");
		ret = -ENOMEM;
		goto err_out;
	}

	pol->pol_id.id = read32(&p);
	size -= DBP_HEADER_SIZE;

	while (size > 0) {
		uint8_t type = read8(&p);
		size -= 1;
		switch (type) {
		case DBP_LOG_LOC:
			ret = parse_log_locs(pol, &p, &size);
			if (ret < 0)
				goto err_free_pol;
			uninit &= ~(1 << DBP_LOG_LOC);
			break;
		case DBP_NAMESPACE:
			ret = parse_namespace(pol, &pol->ns, &p, &size);
			if (ret < 0)
				goto err_free_pol;
			uninit &= ~(1 << DBP_NAMESPACE);
			break;
		case DBP_MATCH:
			if (!(uninit & (1 << DBP_MATCH))) {
				ret = -EINVAL;
				goto err_free_pol;
			}

			ret = parse_match_blocks(pol, &p, &size);
			if (ret < 0)
				goto err_free_pol;
			uninit &= ~(1 << DBP_MATCH);
			break;
		case DBP_INIT:
			if (!(uninit & (1 << DBP_INIT))) {
				ret = -EINVAL;
				goto err_free_pol;
			}

			if (size < 4) {
				ret = -EINVAL;
				goto err_free_pol;
			}
			pol->n_init = read32(&p);
			size -= 4;

			pol->init = kcalloc(pol->n_init,
					    sizeof(struct difc_policy_block),
					    GFP_KERNEL);
			for (i = 0; i != pol->n_init; ++i) {
				ret = parse_block(pol, &pol->init[i],
						  &p, &size);
				if (ret < 0) {
					/* don't free uninitialized blocks */
					pol->n_init = i;
					goto err_free_pol;
				}
			}
			uninit &= ~(1 << DBP_INIT);
			break;
		case DBP_FLOW_POLICY:
			ret = parse_flow_policies(pol, &p, &size);
			if (ret < 0)
				goto err_free_pol;
			uninit &= ~(1 << DBP_FLOW_POLICY);
			break;
		default:
			ret = -EINVAL;
			goto err_free_pol;
		}
	}

	/* default values, if appropriate */
	if (uninit & (1 << DBP_INIT)) {
		/* because of kzalloc, this is already true
		pol->init = NULL;
		pol->n_init = 0;
		*/
		uninit &= ~(1 << DBP_INIT);
	}

	if (uninit) {
		pr_err("uninitialized\n");
		ret = -EINVAL;
		goto err_free_pol;
	}

	vfree(buf);
	return pol;

err_free_pol:
	/* don't free uninitialized actions */
	difc_policy_free(pol);
	kfree(pol);
err_msg:
	pr_err("invalid policy %s (err=%d)\n", path, ret);
err_out:
	vfree(buf);
	return ERR_PTR(ret);
}
