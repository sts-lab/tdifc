#include "debug.h"

#include <asm/paravirt.h>
#include <asm/syscall.h>
#include <asm/unistd.h>
#include <linux/kallsyms.h>
#include <linux/mm.h>
#include <linux/types.h>
#include <linux/uio.h>

#include "write_hook.h"
#include "cap.h"
#include "cred.h"
#include "label.h"
#include "policy.h"
#include "policy_dsl.h"
#include "regex.h"

/**
 * Copy the userspace log message in @iter to kernel space, match it with the
 * @cred->policy's regexes, and apply any actions as necessary.
 */
static void match_log(struct difc_cred *cred, struct iov_iter *iter)
{
	size_t len = iov_iter_count(iter);
	char *str;
	size_t i;

	if (!(str = kvmalloc(len, GFP_KERNEL))) {
		pr_err("kvmalloc failed\n");
		return;
	}

	if (copy_from_iter(str, len, iter) != len) {
		pr_err("incomplete copy\n");
		goto out;
	}

	for (i = 0; i != cred->policy->n_actions; ++i) {
		struct difc_action *axn = &cred->policy->actions[i];
		struct capture cgs[REGEX_MAX_CAPTURES];
		size_t j;
		if (!regex_match(&axn->regex, str, len, cgs))
			continue;
		for (j = 0; j != axn->n_blocks; ++j)
			difc_execute_block(cred->policy, cred,
					   &axn->blocks[j], cgs);
		/* multiple regexes could match same string; must continue */
	}

out:
	kvfree(str);
}

/*
 * x86_64 pt_regs: rdi, rsi, rdx, r10, r8, r9
 * Manually handling the arguments (instead of using SYSCALL macros) is a bit
 * of a hack, but there doesn't seem to be an easy way to both:
 *   1. stick the new entry in sys_call_table, and
 *   2. call the old entry from the new entry
 * without fiddling with the registers manually anyways.
 */

/* ssize_t write(int fd, const void *buf, size_t count) */
static asmlinkage sys_call_ptr_t sys_write;
static asmlinkage ssize_t new_write(const struct pt_regs *regs)
{
	int fd = regs->di;
	char __user *buf = (char __user *)regs->si;
	size_t count = regs->dx;

	struct difc_cred *cred = difc_cred(current_cred());
	if (cred->policy && difc_fd_is_log(cred->policy->lloc, fd)) {
		struct iovec iov = { .iov_base = buf, .iov_len = count };
		struct iov_iter iter;

		iov_iter_init(&iter, READ, &iov, 1, count);
		match_log(cred, &iter);
	}

	return sys_write(regs);
}

/* ssize_t writev(int fildes, const struct iovec *iov, int iovcnt) */
static asmlinkage sys_call_ptr_t sys_writev;
static asmlinkage ssize_t new_writev(const struct pt_regs *regs)
{
	int fildes = regs->di;
	const struct iovec __user *iov = (const struct iovec __user *)regs->si;
	int iovcnt = regs->dx;

	struct difc_cred *cred = difc_cred(current_cred());
	if (cred->policy && difc_fd_is_log(cred->policy->lloc, fildes)) {
		struct iov_iter iter;
		struct iovec fastiov[UIO_FASTIOV];
		struct iovec *fast = fastiov;

		if (import_iovec(READ, iov, iovcnt, ARRAY_SIZE(fastiov),
				 &fast, &iter) < 0) {
			pr_err("import_iovec error\n");
			return sys_writev(regs);
		}

		match_log(cred, &iter);
		kfree(fast);
	}

	return sys_writev(regs);
}

#define HOOK_SYSCALL(name) \
	sys_##name = sys_call_table[__NR_##name]; \
	sys_call_table[__NR_##name] = &new_##name
void __init hook_syswrite(void)
{
	sys_call_ptr_t *sys_call_table = (sys_call_ptr_t *)
			kallsyms_lookup_name("sys_call_table");

	write_cr0(read_cr0() & ~0x10000);
	HOOK_SYSCALL(write);
	HOOK_SYSCALL(writev);
	write_cr0(read_cr0() | 0x10000);
}
