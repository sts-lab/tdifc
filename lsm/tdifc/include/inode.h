#ifndef DIFC_INODE_H
#define DIFC_INODE_H
/*
 * DIFC per-inode security context. For now, this is only used for sockets;
 * file labels are handled purely with xattrs, not security contexts.
 */
#include <linux/fs.h>
#include <linux/spinlock.h>
#include <linux/lsm_hooks.h>

#include "label.h"

struct difc_inode {
	enum difc_inode_type {
		DIFC_INODE_DEFAULT,
		DIFC_INODE_KERNEL, /* internal; always allow */
		DIFC_INODE_SOCKET,
	} type;
	spinlock_t lock;
	struct difc_label label;
};

extern struct lsm_blob_sizes difc_blob_sizes;
static inline struct difc_inode *difc_inode(const struct inode *i)
{
	return i->i_security + difc_blob_sizes.lbs_inode;
}

/*
 * LSM hooks for DIFC inodes.
 */
int difc_inode_alloc_security(struct inode *inode);
void difc_inode_free_security(struct inode *inode);

#endif
