#ifndef DIFC_WRITE_HOOK_H
#define DIFC_WRITE_HOOK_H
/*
 * Intercept log messages by hooking sys_write. We do this instead of just
 * modifying actual write functions to keep all changes local to this LSM
 * source tree, but in reality, changing the functions is probably better than
 * wrapping them.
 */
#include <linux/init.h>

/**
 * Hook write syscalls to intercept log messages.
 */
void __init hook_syswrite(void);

#endif
