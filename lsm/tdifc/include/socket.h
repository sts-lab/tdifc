#ifndef DIFC_SOCKET_H
#define DIFC_SOCKET_H
/*
 * LSM hooks for sockets. Note that LSM security blobs for sockets are stored
 * in their corresponding SOCK_INODE.
 */
struct socket;
struct msghdr;

int difc_socket_post_create(struct socket *sock, int family, int type,
			    int protocol, int kern);
int difc_socket_sendmsg(struct socket *sock, struct msghdr *msg, int size);
int difc_socket_recvmsg(struct socket *sock, struct msghdr *msg, int size,
			int flags);

#endif
