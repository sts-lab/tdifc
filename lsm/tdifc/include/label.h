#ifndef DIFC_LABEL_H
#define DIFC_LABEL_H
/*
 * Labels are sets of tags. However, since only membership of IDs in the label
 * is required, the set is represented as a red-black tree of bitmaps. The
 * 32-bit space of IDs is broken into a uint32_t representing the 5 least
 * significant bits, and a uint32_t representing the remaining 27 most
 * significant bits.
 */
#include "debug.h"

#include <linux/rbtree.h>

#include "id.h"
struct difc_cap_mask;
struct difc_cred;

/*
 * Members of label tag sets at runtime.
 */
struct difc_rtag {
	struct rb_node rb_node;
	uint32_t msbs;
	uint32_t lsbs;
};

#define DIFC_RTAG_MASK 0x1fu
#define DIFC_RTAG_BITS 5

/**
 * Return the difc_tag_id corresponding to bit number @lsb of @tag.
 */
static inline struct difc_tag_id difc_id_from(const struct difc_rtag *tag,
				       unsigned int bit)
{
	return (struct difc_tag_id)
			{ .id = (tag->msbs << DIFC_RTAG_BITS) | bit };
}

static inline uint32_t u32_first_bit(const uint32_t *i, unsigned long size)
{
	return find_first_bit((const unsigned long *) i, size);
}

static inline uint32_t u32_next_bit(const uint32_t *i, unsigned long size,
				    unsigned long offset)
{
	return find_next_bit((const unsigned long *) i, size, offset);
}

/**
 * Iterate over all difc_tag_ids @id stored in difc_rtag *@rtag.
 * @i is a temporary uint32_t needed for iteration.
 */
#define difc_rtag_for_each(i, id, rtag) \
	for ((i) = u32_first_bit(&rtag->lsbs, 1 << DIFC_RTAG_BITS), \
	     (id) = difc_id_from(rtag, i); \
	     (i) < 1 << DIFC_RTAG_BITS; \
	     (i) = u32_next_bit(&rtag->lsbs, 1 << DIFC_RTAG_BITS, (i) + 1), \
	     (id) = difc_id_from(rtag, i))

/*
 * A label is a set of tags in some particular namespace. The label and all
 * its contained tags should have the same namespace. However, since labels
 * operate purely on IDs, it is up to the caller to maintain the namespace
 * invariant when directly using the label manipulation functions.
 *
 * The tag set is implemented as a red-black tree of bit sets.
 */
struct difc_label {
	struct difc_ns ns;
	struct rb_root_cached rtags; /* tree of difc_rtags */
};

/**
 * Initialize a difc_label in struct difc_ns @ns.
 */
static inline struct difc_label difc_label(struct difc_ns ns)
{
	return (struct difc_label) { .rtags = RB_ROOT_CACHED, .ns = ns };
}

/**
 * Initialize a completely blank label, with no namespace.
 */
static inline struct difc_label difc_blank_label(void)
{
	return difc_label((struct difc_ns) { .type = DIFC_NS_NOPOLICY });
}

/**
 * Free the contents of @label.
 */
void difc_label_free(struct difc_label *label);

/**
 * Add @id to @label.
 * Return 0 if @id was successly added, 1 if @id was already in @label, or
 * some error code < 0 in case of error.
 */
int difc_label_add(struct difc_label *label, struct difc_tag_id id);

/**
 * Return whether @label contains @id.
 */
bool difc_label_contains(struct difc_label *label, struct difc_tag_id id);

/**
 * Set @out to be the union of @out and @label, i.e.,
 *   out = out + label
 * Note that if @out is empty (e.g., just allocated by @difc_label), then this
 * is equivalent to copying @label to @out.
 *
 * Return 0 on success, or some error code < 0 on failure.
 * @out may be a partial copy of @label on failure.
 */
int difc_label_union(struct difc_label *out, const struct difc_label *label);

/**
 * Remove @id from @label.
 * Return 0 if @id was successly removed or 1 if @id wasn't in @label.
 */
int difc_label_remove(struct difc_label *label, struct difc_tag_id id);

/**
 * Set @out to be the difference of @out and @label, i.e.,
 *   out = out - label
 * Return 0 on success, or some error code < 0 on failure.
 */
int difc_label_diff(struct difc_label *out, const struct difc_label *label);

/**
 * Same as @difc_label_diff, but only remove tags from @out if @mask has the
 * corresponding minus capability based on @cred.
 */
int difc_label_diff_checked(const struct difc_cred *cred,
			    const struct difc_cap_mask *mask,
			    struct difc_label *out,
			    const struct difc_label *label);

/**
 * Return the size of @label, i.e., the number of tags in @ns.
 */
int difc_label_size(const struct difc_label *label, struct difc_ns ns);

#endif
