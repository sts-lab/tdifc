#ifndef DIFC_DEBUG_H
#define DIFC_DEBUG_H
/*
 * Debugging and bug macros
 */
#undef pr_fmt
#define pr_fmt(fmt) "tdifc: " fmt

#include <linux/bug.h>
#include <linux/printk.h>

#define DIFC_BUG(...) \
	do { \
		pr_err(__VA_ARGS__); \
		BUG(); \
	} while (0)

#endif
