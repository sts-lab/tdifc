#ifndef DIFC_POLICY_DSL_H
#define DIFC_POLICY_DSL_H
/*
 * DIFC policy DSL: defines some representation of statements executed when
 * logs are matched. This is basically bytecode, with included functions to
 * interpret said bytecode.
 */
#include <linux/types.h>

#include "tag.h"
#include "regex.h"
struct difc_cred;
struct difc_policy;

/*
 * A single DIFC policy statement.
 */
struct difc_policy_stmt {
	enum difc_stmt_type {
		DIFC_ACTION_ADD_TAGS, /* maps tags if it does not exist */
		DIFC_ACTION_DEL_TAGS, /* nop if tag does not exist */
		DIFC_ACTION_SET_TAGS, /* empty set_tags to clear */
		DIFC_ACTION_ADD_CAPS, /* add caps per tag */
		DIFC_ACTION_DEL_CAPS, /* remove caps per tag */
		DIFC_ACTION_SET_CAPS, /* set caps per tag */
		DIFC_ACTION_DROP_CAPS, /* drop all capabilities */
		DIFC_ACTION_MASK_CAP, /* nop if tag does not exist */
		DIFC_ACTION_UNMASK_CAP, /* nop if tag does not exist */
	} type;
	size_t n_data;
	struct difc_policy_stmt_data {
		unsigned int flags; /* iff applicable (*_CAPS and *_TAGS) */
		struct difc_tagvec vec;
	} *data;
};

/*
 * PID for a block to act on
 */
struct difc_policy_pid {
	enum difc_policy_pid_type {
		DIFC_POLICY_BLOCK_PID_SELF,
		DIFC_POLICY_BLOCK_PID_PARENT,
		DIFC_POLICY_BLOCK_PID_CHILDREN,
		DIFC_POLICY_BLOCK_PID_CAPTURE,
		DIFC_POLICY_BLOCK_PID_LITERAL,
	} type;
	pid_t pid; /* may also be a capture group */
};

/*
 * A block of DIFC policy statements acting on some particular PID.
 */
struct difc_policy_block {
	struct difc_policy_pid *pids;
	size_t n_pids;
	struct difc_policy_stmt *stmts;
	size_t n_stmts;
};

/*
 * List of blocks to execute when matching a log message.
 */
struct difc_action {
	struct regex regex; /* regex to match */
	struct difc_policy_block *blocks;
	size_t n_blocks;
};

/**
 * The following functions free the contents of the struct, but not the
 * structs themselves.
 * This assumes everything in the structs are dynamically allocated, so if
 * their contents point anywhere else, don't use these.
 */

void difc_policy_stmt_free(struct difc_policy_stmt *stmt);
void difc_policy_stmt_data_free(struct difc_policy_stmt_data *data);
void difc_policy_block_free(struct difc_policy_block *block);
void difc_action_free(struct difc_action *axn);

/**
 * Execute a single @stmt under policy @pol with capture groups @cgs and
 * @cred. @cred itself will probably be modified (either its tags or caps).
 *
 * Note that @stmt->vec contains a template, which may include capture
 * indices. Any capture groups will modify the fragment in-place so @string
 * points to the appropriate entry in @cgs. If @stmt->vec doesn't reference
 * captures, then @cgs can be NULL.
 *
 * Return 0 on success, or some error code < 0 on failure.
 */
int difc_execute_stmt(const struct difc_policy *pol,
		      struct difc_cred *cred,
		      struct difc_policy_stmt *stmt,
		      struct capture *cgs);

/**
 * Like @difc_execute_stmt, but for a @block instead. This continues even in
 * case of errors (logging them as KERN_ERRs). Note that @block specifies the
 * PIDs to execute on, so this will automatically find the correct difc_cred.
 */
void difc_execute_block(const struct difc_policy *pol,
			struct difc_cred *cred,
			struct difc_policy_block *block,
			struct capture *cgs);

#endif
