#ifndef DIFC_XATTR_H
#define DIFC_XATTR_H
/*
 * This should really be in <uapi/linux/xattr.h>, but we avoid modifying
 * anything outside of this LSM for easier portability.
 */
#include <uapi/linux/xattr.h>

#define XATTR_NAME_DIFC_POLICY XATTR_SECURITY_PREFIX "tdifc.policy"
#define XATTR_NAME_DIFC_LABEL XATTR_SECURITY_PREFIX "tdifc.label"

#endif
