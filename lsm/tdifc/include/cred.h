#ifndef DIFC_CRED_H
#define DIFC_CRED_H
/*
 * DIFC per-task security context. Each process needs to know its:
 *   1. policy ID
 *   2. policy actions (i.e., regex -> action mapping)
 *   3. current label
 *   4. capability mask set
 *
 * The policy is set on loading a new executable by inspecting the file's
 * XATTR_NAME_DIFC_POLICY xattr, where the policy_id is encoded as a uint32_t
 * literal in network byte order. Any unspecified values are otherwise
 * inherited from the parent process.
 *
 * For a difc_cred @cred, if @cred->policy is NULL, then no active policy is
 * loaded, so no need to do any explicit permission checks, etc.
 */
#include <linux/cred.h>
#include <linux/lsm_hooks.h>
struct linux_binprm;

#include "cap.h"
#include "label.h"
struct difc_policy;


struct difc_cred {
	const struct difc_policy *policy; /* immutable table entry */
	struct difc_label label;
	struct difc_cap_mask cap_mask;
};

extern struct lsm_blob_sizes difc_blob_sizes;
static inline struct difc_cred *difc_cred(const struct cred *c)
{
	return c->security + difc_blob_sizes.lbs_cred;
}

/*
 * LSM hooks for DIFC creds.
 */
int difc_cred_alloc_blank(struct cred *c, gfp_t gfp);
void difc_cred_free(struct cred *c);
int difc_cred_prepare(struct cred *new, const struct cred *old, gfp_t gfp);
void difc_cred_transfer(struct cred *new, const struct cred *old);
int difc_bprm_set_creds(struct linux_binprm *bprm);

#endif
