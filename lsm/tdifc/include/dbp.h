#ifndef DIFC_DBP_H
#define DIFC_DBP_H
/*
 * DIFC Binary Policy format
 * This contains the parser for the DBP format. It only does minor validity
 * checking; actual compilation should happen in userspace.
 *
 * Use kernel_read_file_from_path to read the policy given by dbp_load_file:
 *
 * echo '/path/to/file' > dbp_load_file
 *
 * While file IO is typically frowned upon in kernel space, we do this as:
 * 1. There's precedent in other LSMs (loadpin/integrity)
 * 2. DBP files could be really big, depending on what regexes they contain
 *    and what the regexes actually are. Thus, it might not fit into a single
 *    page.
 *
 *    This causes two big problems for sysfs: regular sysfs files don't allow
 *    for larger than PAGE_SIZE writes, and binary attributes split the write
 *    across multiple function calls, which means we need some way to both
 *    collect together statements across multiple write calls, and more
 *    annoyingly, we might have to parse a regex broken up across multiple
 *    buffers.
 *
 * Format:
 * Actual data is given as
 *   <bytes>: <description>
 * Everything else is just formatting for legibility
 * =================================================
 * header {
 *   1: version (= DBP_VERSION) (uint8_t)
 *   4: policy id (uint32_t)
 * }
 * body: repeat {
 *   1: dbp_type
 *   if DBP_LOG_LOC {
 *     4: total number of DBP_LOG_LOCs (uint32_t)
 *     dbp_log_loc: repeat {
 *       1: dbp_log_type
 *       if DBP_LOG_FD {
 *         4: fd (uint32_t)
 *       } else if DBP_LOG_PATH {
 *         ?: path (null-terminated string, must be valid length)
 *       }
 *     }
 *   } else if DBP_NAMESPACE {
 *     1: namespace type (uint8_t)
 *     if DBP_NS_NORMAL {
 *       4: namespace id (uint32_t)
 *     }
 *   } else if DBP_MATCH { // some kind of block
 *     4: total number of DBP_MATCH blocks (uint32_t)
 *     dbp_blocks: repeat {
 *       ?: binary regex (regex_deserialize format)
 *       4: total number of code blocks (uint32_t)
 *       code_blocks: repeat {
 *         4: total number of pids (uint32_t)
 *         pids: repeat {
 *           1: dbp_pid_type
 *           switch (dbp_pid_type) {
 *           case DBP_PID_SELF:
 *           case DBP_PID_PARENT:
 *           case DBP_PID_CHILDREN:
 *             0: empty
 *           case DBP_PID_CAPTURE:
 *             1: capture index in 0 .. MAX_CAPTURES (uint8_t)
 *           case DBP_PID_LITERAL:
 *             4: pid (uint32_t)
 *           }
 *         }
 *         4: total number of statements in block (uint32_t)
 *         statements: repeat {
 *           1: dbp_stmt_type (uint8_t)
 *           switch (dbp_stmt_type) {
 *           case DBP_STMT_*_TAGS:
 *           case DBP_STMT_*_CAPS:
 *             4: total number of tags
 *             tags: repeat {
 *               4: cap_flags
 *               tagvec {
 *                 1: n_frags (uint8_t) // won't have >255 fragments
 *                 fragments: repeat {
 *                   1: type
 *                   if DBP_FRAG_CAPTURE {
 *                     1: index
 *                   } else if DBP_FRAG_LITERAL {
 *                     ?: null-terminated string
 *                   }
 *                 }
 *               }
 *             }
 *           case DBP_STMT_DROP_CAPS:
 *             0: empty
 *           }
 *         }
 *       }
 *     }
 *   } else if DBP_INIT {
 *     4: total number of blocks (uint32_t)
 *     ?: code_blocks (described in DBP_MATCH)
 *   } else if DBP_FLOW_POLICY {
 *     4: total number of flow_policies (uint32_t)
 *     flow_policies: repeat {
 *       1: dbp_flow_policy_type
 *       4: max label size (uint32_t)
 *     }
 *   }
 * }
 *
 * IMPLEMENTATION DETAIL: for now, only one location per policy, so multiple
 * LOG_LOCs just overwrite the previous value. This could easily be changed to
 * check every log_loc instead.
 */
struct file;

#define DBP_VERSION 1
#define DBP_HEADER_SIZE (1 + 4)

/*
 * all enums start at 1 to make 0 an error
 * (might make read errors more obvious if using zero-initialized memory)
 */
enum dbp_type {
	DBP_LOG_LOC = 1, /* what is a log write? */
	DBP_NAMESPACE, /* policy namespace */
	DBP_MATCH, /* regex match statement(s) */
	DBP_INIT, /* initialization statement(s) */
	DBP_FLOW_POLICY, /* configuration of allowable flows */
};

enum dbp_flow_policy_type {
	DBP_FLOW_SOCKET = 1,
	DBP_FLOW_PROCESS,
};

enum dbp_pid_type {
	DBP_PID_SELF = 1,
	DBP_PID_PARENT,
	DBP_PID_CHILDREN,
	DBP_PID_CAPTURE,
	DBP_PID_LITERAL,
};

enum dbp_stmt_type {
	DBP_STMT_ADD_TAGS = 1,
	DBP_STMT_DEL_TAGS,
	DBP_STMT_SET_TAGS,
	DBP_STMT_ADD_CAPS,
	DBP_STMT_DEL_CAPS,
	DBP_STMT_SET_CAPS,
	DBP_STMT_DROP_CAPS,
};

enum dbp_log_type {
	DBP_LOG_FD = 1,
	DBP_LOG_PATH,
};

enum dbp_ns_type {
	DBP_NS_UNIQUE = 1,
	DBP_NS_NORMAL,
};

/*
 * Intentional redefinition; this allows the internal DIFC_CAP_* flags to
 * potentially be modified if necessary
 */
#define DBP_CAP_PLUS    0x1u
#define DBP_CAP_MINUS   0x2u
#define DBP_CAP_UNMASK  0x4u
#define DBP_CAP_MUTABLE 0x8u

enum dbp_frag_type {
	DBP_FRAG_CAPTURE = 1,
	DBP_FRAG_LITERAL,
};

/**
 * Parse the binary policy in @path and return a difc_policy. This does NOT
 * check for any permissions on @path. May return a PTR_ERR on failure.
 */
struct difc_policy *dbp_read(const char *path);

#endif
