#ifndef DIFC_HASHFNS_H
#define DIFC_HASHFNS_H
/*
 * Various hash and comparison functions used for hash tables.
 */
#include <linux/types.h>

static inline int u32cmp(uint32_t x, uint32_t y)
{
	return x < y ? -1 : x > y;
};

u32 difc_hash_tag(const void *data, u32 len, u32 seed);
u32 difc_hash_tag_id(const void *data, u32 len, u32 seed);
u32 difc_hash_policy_id(const void *data, u32 len, u32 seed);

#endif
