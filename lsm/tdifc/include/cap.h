#ifndef DIFC_CAP_H
#define DIFC_CAP_H
/*
 * If tags represent ownership of data, then capabilities roughly represent
 * ownership of tags. (Roughly, because they also include metacapabilities
 * about valid operations on capability sets.) All processes have an effective
 * capability set that dictates what operations are allowed (e.g., increasing
 * security level by adding a secrecy tag, etc.).
 *
 * Because tags can be dynamically allocated, actually having each process
 * maintain a complete set of capabilities is unfeasible; otherwise, all
 * process capability sets would have to modified every time a tag is created
 * or when an owner decides to change the allowable capabilities. Instead,
 * capabilities are kept in a globally shared set, and each process instead
 * maintains a mask set if they wish to remove some capabilities. The
 * effective capability set is the difference of the global and mask sets.
 *
 * The first policy to get a tag becomes the tag's owner, and thus gains
 * control of its capabilities in the global capability set. This means the
 * owner may also control capabilities for all other processes at any time.
 *
 * TODO consider concurrency issues. RCU seems ideal?
 */
#include <linux/init.h>
#include <linux/rbtree.h>

#include "id.h"
struct difc_cred;

/*
 * Capability type, including the affected tag and the actual capabilities
 * encoded as a flag bitmap.
 */
struct difc_cap {
	struct difc_policy_id pol_id; /* policy that owns this capability */
	struct difc_tag_id tag_id; /* tag this capability represents */
	unsigned int flags;
};

/*
 * DIFC capability flags
 */
#define DIFC_CAP_PLUS     0x1u /* may add tag (e.g., increase level) */
#define DIFC_CAP_MINUS    0x2u /* may remove tag (e.g., declassify) */
#define DIFC_CAP_UNMASK   0x4u /* may unmask this capability */
#define DIFC_CAP_MUTABLE  0x8u /* owner may modify this capability */

#define DIFC_CAP_NONE     0u
#define DIFC_CAP_ALL (DIFC_CAP_PLUS | DIFC_CAP_MINUS | DIFC_CAP_UNMASK | \
		      DIFC_CAP_MUTABLE)

/**
 * Initialize the global capability table.
 * Return 0 on success, or code < 0 on failure.
 */
int __init difc_cap_table_init(void);

/**
 * Create a new capability for @tag_id owned by @pol_id with initial global
 * value @flags. Return 0 on success, 1 if the capability already exists, or
 * some error code < 0 on failure.
 */
int difc_cap_new(struct difc_policy_id pol_id, struct difc_tag_id tag_id,
		 unsigned int flags);

/**
 * Return the global capability for @tag_id if it exists and is owned by
 * @pol_id, or NULL otherwise.
 */
struct difc_cap *difc_cap_get(struct difc_policy_id pol_id,
			      struct difc_tag_id tag_id);

/**
 * Return the initial global capability flags value for @tag_id.
 *
 * This returns 0 if @tag_id doesn't correspond to a tag. However, this is NOT
 * a reliable way to decide if @tag_id is unmapped, because a value of 0 could
 * alternatively mean no capabilities.
 */
unsigned int difc_cap_initial_flags(struct difc_tag_id tag_id);

/*
 * Runtime capability tree nodes. These are used as a mask for the default
 * capabilities; i.e., any process can intentionally drop some capabilities,
 * but they cannot add capabilities beyond the default set.
 */
struct difc_rcap {
	struct rb_node rb_node;
	struct difc_cap cap;
};

/*
 * Mask set for DIFC capabilities.
 */
struct difc_cap_mask {
	struct rb_root_cached rcaps; /* tree of rcaps */
	bool drop_rest; /* caps not in rcaps are dropped (i.e. CAP_NONE) */
};

/**
 * Initialize a new empty cap mask.
 */
static inline struct difc_cap_mask difc_cap_mask(void)
{
	return (struct difc_cap_mask) {
		.rcaps = RB_ROOT_CACHED,
		.drop_rest = false,
	};
}

/**
 * Copy @mask to @out. Return 0 on success, or some error code < 0 on failure.
 * On failure, @out may be a partial copy of @mask.
 */
int difc_cap_mask_union(struct difc_cap_mask *out,
			const struct difc_cap_mask *mask);

/**
 * Insert @cap into @mask, overwriting any previous entry (if any).
 * Return 0 on success, or some error code < 0 on failure.
 */
int difc_cap_mask_insert(struct difc_cap_mask *mask,
			 const struct difc_cap *cap);

/**
 * Bitwise OR @cap->flags with @mask. If @mask has no such cap yet,
 * it is treated as though the flags were DIFC_CAP_ALL.
 * Return 0 on success, or some error code < 0 on failure.
 */
int difc_cap_mask_or(struct difc_cap_mask *mask, const struct difc_cap *cap);

/**
 * Bitwise AND @cap->flags with @mask. If @mask has no such cap yet,
 * it is treated as though the flags were DIFC_CAP_ALL.
 * Return 0 on success, or some error code < 0 on failure.
 */
int difc_cap_mask_and(struct difc_cap_mask *mask, const struct difc_cap *cap);

/**
 * Free the contents of @mask.
 */
void difc_cap_mask_free(struct difc_cap_mask *mask);

/**
 * Return the effective capability set based on @mask for @tag_id for current
 * cred @cc. If @rcap doesn't contain an entry for @tag_id, this is equivalent
 * to the global capability value.
 */
unsigned int difc_eff_cap_flags(const struct difc_cred *cc,
				const struct difc_cap_mask *mask,
				struct difc_tag_id tag_id);

#endif
