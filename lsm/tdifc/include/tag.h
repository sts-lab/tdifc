#ifndef DIFC_TAG_H
#define DIFC_TAG_H
/*
 * Conceptually, tags are strings belonging to some namespace, and labels are
 * sets of tags all in the same namespace.
 *
 * To minimize operations on strings at runtime (especially since tags could
 * be arbitrarily large, depending on the policy), tags are mapped to 32-bit
 * integer IDs. (This means a system is limited to ~4.3 million tags at
 * runtime, which seems more than sufficient).
 *
 * Allocated tags are maintained in a hash table. Lookups to the table use a
 * tag's namespace and string value. Tags are "created" by looking up the
 * table and not finding a match; then the tag is inserted in the table, given
 * a new ID, and the subject creating the tag is given ownership.
 *
 * TODO consider whether tags should ever become "deallocated". Probably not?
 */
#include <linux/init.h>
#include <linux/types.h>
#include <linux/rhashtable-types.h>

#include "id.h"

/*
 * Tag string semantic value. Note that two distinct tags could have the same
 * string if they are in different namespaces.
 */
struct difc_tag {
	char *string; /* not null terminated. NULL if empty string */
	size_t length; /* length of string; 0 if empty string */
	struct difc_ns ns;
};

/*
 * Outside of regex matching/policy loading, these can be treated as though
 * they are always string + length.
 *
 * Policy tagvecs may actually be capture groups, in which case the index in
 * the buffer is known, but the string and length are not known. After
 * matching, string and length must be copied from the capture buffer before
 * using the tagvec.
 */
struct difc_tagvec_fragment {
	/*
	 * capture: index in [0, REGEX_MAX_CAPTURES); i.e., capture index
	 * literal: index = REGEX_MAX_CAPTURES
	 */
	unsigned int index;
	char *string;
	size_t length;
};

/**
 * Free @frag (but not the struct itself).
 */
void difc_tagvec_fragment_free(struct difc_tagvec_fragment *frag);

/*
 * Iovec-like tags: string may be scattered across multiple fragments.
 */
struct difc_tagvec {
	struct difc_tagvec_fragment *frags;
	size_t n_frags; /* 0 if empty */
	struct difc_ns ns;
};

/**
 * Free @vec (but not the struct itself).
 */
void difc_tagvec_free(struct difc_tagvec *vec);

/**
 * Print the contents of @vec via DIFC_INFO.
 */
void difc_tagvec_printk(const struct difc_tagvec *vec);

/*
 * Maps a tag to its unique ID, as well as its owner.
 */
struct difc_tag_entry {
	struct rhash_head chain;
	struct difc_tag tag;
	struct difc_tag_id tag_id;
	struct difc_policy_id owner;
};

/**
 * Return the tag entry for @tag. If @tag is not in the table yet, then
 * @pol_id will gain ownership of the tag, and the initial global capability
 * flags are set to @cap_flags. @tag will be copied if necessary; the caller
 * maintains ownership of @tag itself.
 *
 * The returned entry must not be modified.
 *
 * This may return a PTR_ERR in case of failure.
 */
const struct difc_tag_entry *difc_map_tag(const struct difc_tag *tag,
					  struct difc_policy_id pol_id,
					  unsigned int cap_flags);

/**
 * Same as @difc_map_tag, but for @vec instead.
 */
const struct difc_tag_entry *difc_map_tagvec(const struct difc_tagvec *vec,
					     struct difc_policy_id pol_id,
					     unsigned int cap_flags);

/**
 * Return the tag entry for @vec, or NULL if @vec is not in the table.
 * The returned entry must not be modified.
 *
 * This may return a PTR_ERR in case of failure.
 */
const struct difc_tag_entry *difc_query_tagvec(const struct difc_tagvec *vec);

/**
 * Return the tag entry for @tag_id, or NULL if @tag_id is not in the table.
 * The returned entry must not be modified.
 */
const struct difc_tag_entry *difc_query_tag_id(struct difc_tag_id tag_id);

/**
 * Initialize the tag table. Return 0 on success, or code < 0 on failure.
 */
int __init difc_tag_table_init(void);

#endif
