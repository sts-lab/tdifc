#ifndef DIFC_RE_H
#define DIFC_RE_H
/*
 * Supported patterns (all quantifiers are greedy):
 *  a     character literal
 *  .     any character but newline (\n)
 *  [ ]   character class (ranges/empty (e.g. [a-z]/[]) are allowed)
 *  [^ ]  inverted character class (ranges/empty are allowed)
 *  p*    zero or more of p
 *  p+    one or more of p
 *  p?    zero or one of p
 *  (p)   group (quantifiers apply to entire group)
 *  <p>   capture group (group, and location of contents is stored)
 *  a|b   alternation (a or b)
 *
 *
 * For alternation, the left branch is taken first. For example, when given a,
 * <a>|a will capture a, but a|<a> will capture nothing. Relying on this
 * behavior is not advised.
 *
 * The following escapes are always allowed:
 *   \oo?o? \xhh? \a \b \e \f \n \r \t \v \\
 * where \oo?o? represents an octal value with 1-3 digits and \xhh? represents
 * a hex value with 1-2 digits.
 * A backslash followed by any other character is simply that character; this
 * can be used to get ] in a character class (e.g. [\]]) or to interpret a
 * metacharacter as a literal (e.g. \* is a literal asterisk).
 *
 * Empty regexes (e.g. (a|)) are not allowed. You can get the same effect by
 * using []? or []*, since the empty character class cannot match anything.
 *
 * Ranges must be forwards. For example, a-z is all lowercase letters, but z-a
 * is nothing.
 *
 * The internal representation of regexes uses C chars, and assumes 1
 * character is 1 byte. For encodings like UTF-8, this is only a problem if:
 *   1. A multibyte character is used in a character class.
 *      Solution: use a literal and alternation instead.
 *   2. A multibyte character is quantified without parentheses.
 *      Solution: Surround quantified multibyte characters with parentheses.
 *
 * Note that this implementation relies on string length instead of null
 * characters to decide the end of input. It is possible to include null
 * characters in the regex pattern (although not directly, because the parser
 * assumes null-terminated strings; use the escape \0 instead).
 *
 * Note that the regex parser is NOT included here. The kernel can simulate a
 * regex, but it can't parse one.
 */
#include <linux/types.h>
#include <linux/list.h>

#ifndef CHAR_BIT
#define CHAR_BIT 8
#endif

/** Max number of capture groups */
#define REGEX_MAX_CAPTURES 9

enum {
	LINVALID = 0, ACCEPT, DOT, LITERAL, CLASS, OR, STAR, PLUS, QUESTION,
	LCAPTURE, RCAPTURE, HINVALID
};

/** Bitwise boolean array for whether a char is in the class or not. */
struct _class {
	/* for 8-bit chars, should be 32 bytes = 256 bits */
	unsigned char bs[(1 << CHAR_BIT) / CHAR_BIT];
};

/**
 * Uses indices to be more amenable to binary storage.
 * max allowed regex length: 2**16 - 1 = 65535 (should be more than enough).
 */
struct _regex {
	uint8_t type;
	union {
		char c; /* character literal */
		uint16_t i; /* index of pattern, class, or capture group */
		/* captures are flat, e.g., group 1: (0, 1), 2: (2, 3)... */
	};
	uint16_t next; /* index of next regex in buffer */
};

/**
 * Regex pattern type.
 */
struct regex {
	/*
	 * the regex is a flat, contiguous array in memory. actual max length
	 * is 2^16 - 1 = 65535 nodes; however, a better "soft limit" is how
	 * many fit in 1 page (on x86, around ~600 nodes). furthermore,
	 * only 128 character classes fit into an x86 page. hopefully, none of
	 * this will actually matter, because it's unlikely for a pattern to
	 * be that big.
	 */
	struct _regex *buf;
	uint16_t bufsize; /* number of elements in buf; max 2^16 - 1 */
	uint16_t head; /* offset of head in buf */

	struct _class *cbuf; /* classes are also contiguous */
	uint16_t cbufsize; /* cbufsize < bufsize */

	uint8_t ncg; /* number of capture groups; <= REGEX_MAX_CAPTURES */

	/* TODO struct kmem_cache for runtime? */
	struct list_head list;
};

struct capture {
	const char *start; /* index of start of capture */
	size_t len;        /* length of substring */
};

/**
 * Free a @regex.
 */
void regex_free(struct regex *regex);

/**
 * Return true iff @regex exactly matches @str with length @len.
 * @cgs is a buffer of size regex->ncg; it can be NULL if regex->ncg = 0.
 * If the match fails, @cgs is unmodified.
 */
bool regex_match(const struct regex *regex, const char *str, size_t len,
	      struct capture *cgs);

#define RE_SERIALIZE_VERSION 1

/**
 * Deserialize a regex starting in @buf to @regex; return how much of @buf was
 * read. If invalid, regex is undefined (but contains no allocated buffers)
 * and return some code < 0. The @regex->list_head is unmodified.
 */
ssize_t regex_deserialize(const void *buf, size_t len, struct regex *regex);

#endif
