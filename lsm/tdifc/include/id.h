#ifndef DIFC_ID_H
#define DIFC_ID_H
/*
 * Various ID structs are here to avoid cyclic dependencies. For now, all IDs
 * themselves are just 32bit ints, since anything larger seems unnecessary;
 * using structs enforces type safety.
 *
 * These are fairly small, so it would be reasonable to pass them by value
 * instead of by pointer.
 */
#include <linux/types.h>

/*
 * Namespace for DIFC tags and labels. At runtime, labels and tags not within
 * the same namespace are completely ignored (as though they didn't exist).
 */
struct difc_ns {
	enum difc_ns_type {
		DIFC_NS_NOPOLICY, // dummy namespace: no policy loaded
		DIFC_NS_UNIQUE,
		DIFC_NS_NORMAL,
	} type;
	uint32_t id; /* as given by policy, or policy ID if unique */
};

/**
 * Return whether @x equals @y.
 */
static inline bool difc_ns_equal(struct difc_ns x, struct difc_ns y)
{
	return x.type == y.type && x.id == y.id;
}

struct difc_policy_id {
	uint32_t id;
};

struct difc_tag_id {
	uint32_t id;
};

#endif
