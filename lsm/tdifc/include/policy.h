#ifndef DIFC_POLICY_H
#define DIFC_POLICY_H
/*
 * Maps a policy (which includes some ID) to its actions.
 */
#include <linux/rhashtable-types.h>

#include "id.h"
#include "label.h"
#include "policy_dsl.h"

/*
 * Defines whether a write is actually a log message.
 */
struct difc_log_loc {
	enum difc_log_type {
		DIFC_LOG_FD, /* probably std{in,out,err} */
		DIFC_LOG_PATH,
	} type;
	union {
		unsigned int fd;
		char *path;
	};
};

/**
 * Free @lloc (but not the struct itself).
 */
void difc_log_loc_free(struct difc_log_loc *lloc);

/*
 * Propagation policy for labels; i.e., when a process writes to something
 * (e.g., a file), should the file label gain tags, or should the write be
 * forbidden?
 */
struct difc_flow_policy {
	uint16_t max_size; /* max size a label can have */
};

/**
 * Return whether @fpol allows a flow from @src to @dst.
 */
bool difc_flow_perm(const struct difc_flow_policy *fpol,
		    const struct difc_label *dst,
		    const struct difc_label *src);

/*
 * A policy has some ID, specifies what writes are log messages, sets the
 * label propagation policy for files and sockets, and contains a list of
 * actions associated with a regex.
 *
 * To minimize regex checks, each action should have a different regex
 * (although multiple may match a particular string). However, this is not
 * required for a difc_policy to behave correctly.
 */
struct difc_policy {
	struct difc_policy_id pol_id;
	struct difc_ns ns;
	struct difc_log_loc lloc;

	struct difc_flow_policy proc_fpol;
	struct difc_flow_policy sock_fpol;

	struct difc_action *actions;
	size_t n_actions;

	struct difc_policy_block *init; /* always run on load; may be NULL */
	size_t n_init;
};

/**
 * Execute @policy's init blocks for @cred. On errors, this prints a KERN_ERR
 * message and continues to the next statement.
 */
void difc_policy_do_init(const struct difc_policy *pol,
			 struct difc_cred *cred);

/**
 * Free the contents of @policy (but not the struct itself).
 */
void difc_policy_free(struct difc_policy *policy);

/**
 * Return whether @fd represents a log file according to @lloc.
 */
bool difc_fd_is_log(struct difc_log_loc lloc, int fd);

/**
 * Put @pol in the table. @pol->pol_id is used as the key. This takes
 * ownership of the contents of @pol, so it should not be modified after
 * insertion, and shouldn't be on the stack.
 *
 * Return 0 on success, -EEXIST if @pol->pol_id is already mapped,
 * or some other error code < 0 on failure.
 */
int difc_policy_put(struct difc_policy *pol);

/**
 * Return the immutable policy entry for @pol_id, or NULL if it's not found.
 */
const struct difc_policy *difc_policy_get(struct difc_policy_id pol_id);

/**
 * Initialize the difc_policy_table. Return 0 on success, or < 0 on failure.
 */
int __init difc_policy_table_init(void);

#endif
