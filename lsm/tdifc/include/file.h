#ifndef DIFC_FILE_H
#define DIFC_FILE_H
/*
 * DIFC file-related functions, including reading/writing labels and
 * permission checks.
 *
 * (De)serialization of labels for files: xattr labels are lists of:
 *
 *   NS_TYPE,NS_ID,POL_ID,FLAGS,LENGTH,STRING
 *
 * tuples, where tuple elements are separated by commas. (Tuples in the list
 * have no explicit separator, since LENGTH gives the end of one tuple.)
 *
 * NS_TYPE,NS_ID is the namespace of the tag, POL_ID is the policy that owns
 * the tag, FLAGS is the global capability flags for the tag, LENGTH is the
 * length of the tag, and STRING is the tag.
 *
 * Empty labels can be represented as both an empty string xattr, or as no
 * xattr. The latter is the preferred representation.
 *
 * NS_TYPE, NS_ID, POL_ID, and LENGTH are all base85-encoded integers with
 * leading 0s omitted. (Base85 allows for arbitrary-length integers that won't
 * contain ',', the tuple separator.)
 */
#include <linux/types.h>
struct file;

#include "id.h"
struct difc_tag;
struct difc_label;

/**
 * Set @out to @file's xattr policy ID.
 * The policy ID is stored as a base85-encoded uint32_t.
 * Return 0 on success, or some error code < 0 on failure.
 */
int difc_file_get_policy_id(struct difc_policy_id *out, struct file *file);

/**
 * Set @file's label (stored in xattrs) to @label.
 * Return 0 on success, or some error code < 0 on failure.
 */
int difc_file_set_label(struct file *file, const struct difc_label *label);

/**
 * Iterator callback function for @difc_file_label_for_each.
 * If the callback returns 0, iteration continues normally. Otherwise,
 * iteration ends early.
 */
typedef int (*difc_file_callback_t)(void *arg, struct difc_policy_id pol_id,
				    unsigned int flags, struct difc_tag *tag);

/**
 * Apply @fn(@arg, ...) for each tag in @file's label.
 * Return the last value of @fn.
 *
 * May return some error code < 0 on other errors, e.g., when reading xattrs.
 */
int difc_file_label_for_each(const struct file *file, void *arg,
			     difc_file_callback_t callback);

/**
 * Get the tags of @file's label in the same namespace as @label (or all tags,
 * if @all_tags is true) and add them to @label. Return 0 on success, or some
 * error code < 0 on failure.
 *
 * Note that @file might not have any tags (i.e., when getxattr gives
 * -ENODATA). Then difc_file_get_label will still return 0, since this means
 * the empty set was added to @label.
 *
 * If a failure occurs, some (but not all) tags may have been added to @label.
 */
int difc_file_get_label(const struct file *file, struct difc_label *label,
			bool all_tags);

/**
 * DIFC file_permission hook:
 *
 * On file read, add @file's label to the current process label. If one of
 * the tags doesn't have the associated capability, deny the read instead.
 *
 * On file write, add the current process label to @file's label.
 *
 * TODO "caching" by difc_file_ctx instead of purely by xattr (if worthwhile)
 */
int difc_file_permission(struct file *file, int mask);

/**
 * DIFC inode_permission hook:
 *
 * TODO is this safe?
 * On file write ONLY, the current label can be cleared. (If append, the
 * current label cannot be cleared.)
 */
/*
int difc_inode_permission(struct inode *inode, int mask);
*/

#endif
