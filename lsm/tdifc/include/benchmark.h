#ifndef DIFC_TIME_H
#define DIFC_TIME_H

#include "debug.h"

#include <linux/ktime.h>
#include <linux/mutex.h>

#include "cred.h"

/* enable DIFC benchmarking. not in Kconfig to avoid mass recompilation */
#define DIFC_BENCHMARK_ENABLED

#ifdef DIFC_BENCHMARK_ENABLED

/* policy ID range of tests */
#define DIFC_BENCHMARK_MIN_POLID 100
#define DIFC_BENCHMARK_MAX_POLID 499

/* fixed point arithmetic: 54bit integer + 10bit fraction */
#define DIFC_FIXPOINT_BITS 10 /* 2^10 = 1024 */
#define DIFC_FIXPOINT_MASK ((1ll << DIFC_FIXPOINT_BITS) - 1ll)

static inline int64_t difc_to_fixp(int64_t x)
{
	return x << DIFC_FIXPOINT_BITS;
}

static inline int64_t difc_fixp_int(int64_t fixp)
{
	return (uint64_t) fixp >> DIFC_FIXPOINT_BITS;
}

static inline unsigned int difc_fixp_frac(int64_t fixp)
{
	return fixp & DIFC_FIXPOINT_MASK;
}

static inline int64_t difc_fixp_mul(int64_t fixp1, int64_t fixp2)
{
	return (uint64_t) (fixp1 * fixp2) >> DIFC_FIXPOINT_BITS;
}

struct difc_bench {
	struct list_head list;
	const char *name; /* used to find correct entry (linear search) */

	/* track average and std dev via Welford's algorithm */
	int64_t m2_fixp;
	int64_t avg_fixp;
	int64_t count;
};

/**
 * Add a new measurement to a benchmark.
 */
void difc_bench_add(struct difc_bench *bench, int64_t x);

extern struct list_head difc_benches;
extern struct mutex difc_benches_mutex;

void __difc_end_timer(struct difc_cred *cc, const char *s, int64_t ts);

/* Notice that these are complete statements, INCLUDING a semicolon. */
#define DIFC_START_TIMER() \
	uint64_t bench0__ = ktime_get_ns();

#define DIFC_END_TIMER(cc, s) do { \
	uint64_t bench1__ = ktime_get_ns(); \
	__difc_end_timer(cc, s, bench1__ - bench0__); \
} while (0);

#define DIFC_END_TIMER_CURRENT(s) \
	DIFC_END_TIMER(difc_cred(current_cred()), s)

#else /* DIFC_BENCHMARK_ENABLED */

#define DIFC_START_TIMER()
#define DIFC_END_TIMER(cc, s)
#define DIFC_END_TIMER_CURRENT(s)

#endif /* DIFC_BENCHMARK_ENABLED */

#endif
