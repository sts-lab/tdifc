#include <linux/jhash.h>

#include "hashfns.h"
#include "id.h"
#include "tag.h"

u32 difc_hash_tag(const void *data, u32 len, u32 seed)
{
	/*
	 * Streaming hash: ns.type, ns.id, string
	 * ns is fixed-size, so no need for delimiters
	 *
	 * not really sure if it's appropriate to use the previous hash as the
	 * seed for a streaming hash. jhash.h seems to indicate so, and this
	 * also doesn't need to be cryptographically secure, so it's probably
	 * fine.
	 */
	const struct difc_tag *tag = data;
	u32 hash = jhash(&tag->ns.type, sizeof(tag->ns.type), seed);
	hash = jhash(&tag->ns.id, sizeof(tag->ns.id), hash);
	hash = jhash(tag->string, tag->length, hash);
	return hash;
}

u32 difc_hash_tag_id(const void *data, u32 len, u32 seed)
{
	/*
	 * this is not strictly necessary, but avoids any potential future
	 * issues from struct padding.
	 */
	const struct difc_tag_id *tag = data;
	return jhash(&tag->id, sizeof(tag->id), seed);
}

u32 difc_hash_policy_id(const void *data, u32 len, u32 seed)
{
	/*
	 * this is not strictly necessary, but avoids any potential future
	 * issues from struct padding.
	 */
	const struct difc_policy_id *pol_id = data;
	return jhash(&pol_id->id, sizeof(pol_id->id), seed);
}
