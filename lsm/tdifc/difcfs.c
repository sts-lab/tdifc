#include "debug.h"

#include <linux/fs.h>
#include <linux/fs_context.h>
#include <linux/list.h>
#include <linux/seq_file.h>
#include <linux/slab.h>
#include <linux/sysfs.h>
#include <linux/uaccess.h>

#include "benchmark.h"
#include "dbp.h"
#include "policy.h"

/* this is really supposed to be in uapi/linux/magic.h */
#define DIFC_MAGIC 0xD1FC20

enum difcfs_files {
	DIFCFS_ROOT = 1,
	DIFCFS_LOAD,
	DIFCFS_BENCHMARK,
};

static ssize_t difcfs_load_file_write(struct file *file,
				      const char __user *buf, size_t count,
				      loff_t *ppos)
{
	struct difc_policy *policy;
	char *path;
	ssize_t ret;

	if (!(path = kmalloc(count + 1, GFP_KERNEL))) {
		pr_err("failed kmalloc\n");
		return -EFBIG;
	}

	if (copy_from_user(path, buf, count)) {
		pr_err("failed copy\n");
		ret = -EFAULT;
		goto free_path;
	}
	path[count] = '\0';

	policy = dbp_read(path);
	if (IS_ERR(policy)) {
		ret = PTR_ERR(policy);
		goto free_path;
	}

	ret = difc_policy_put(policy);
	if (ret < 0)
		goto free_all;

	ret = count;
	goto free_path;

free_all:
	difc_policy_free(policy);
	kfree(policy);
free_path:
	kfree(path);
	return ret;
}

#ifdef DIFC_BENCHMARK_ENABLED
static void *difcfs_bench_seq_start(struct seq_file *m, loff_t *pos)
{
	if (mutex_lock_interruptible(&difc_benches_mutex) != 0)
		return NULL;
	return seq_list_start_head(&difc_benches, *pos);
}

static void *difcfs_bench_seq_next(struct seq_file *m, void *v, loff_t *pos)
{
	return seq_list_next(v, &difc_benches, pos);
}

static void difcfs_bench_seq_stop(struct seq_file *m, void *v)
{
	mutex_unlock(&difc_benches_mutex);
}

static int difcfs_bench_seq_show(struct seq_file *m, void *v)
{
	struct difc_bench *b = list_entry(v, struct difc_bench, list);
	int64_t var_fixp;

	/* header: use seq_list_start_head */
	if (v == &difc_benches) {
		seq_printf(m, "component avg var\n");
		return 0;
	}

	if (b->count <= 1) {
		seq_printf(m, "%s: not enough data\n", b->name);
		goto out;
	}
	var_fixp = b->m2_fixp / (b->count - 1);
	seq_printf(m, "%s %lld+%u/%u %lld+%u/%u\n",
		   b->name,
		   difc_fixp_int(b->avg_fixp), difc_fixp_frac(b->avg_fixp),
		   (1u << DIFC_FIXPOINT_BITS),
		   difc_fixp_int(var_fixp), difc_fixp_frac(var_fixp),
		   (1u << DIFC_FIXPOINT_BITS));
out:
	return 0;
}

static int difcfs_bench_open(struct inode *inode, struct file *file)
{
	static const struct seq_operations ops = {
		.start = difcfs_bench_seq_start,
		.next = difcfs_bench_seq_next,
		.stop = difcfs_bench_seq_stop,
		.show = difcfs_bench_seq_show,
	};
	return seq_open(file, &ops);
}

static ssize_t difcfs_bench_write(struct file *file, const char __user *buf,
				  size_t count, loff_t *ppos)
{
	struct difc_bench *i;
	list_for_each_entry(i, &difc_benches, list) {
		i->m2_fixp = i->avg_fixp = i->count = 0;
	}
	return count;
}
#endif /* DIFC_BENCHMARK_ENABLED */

static int difcfs_fill_super(struct super_block *sb, struct fs_context *fc)
{
	static const struct file_operations dbp_load_ops = {
		.write = difcfs_load_file_write,
		.llseek = generic_file_llseek,
	};
#ifdef DIFC_BENCHMARK_ENABLED
	static const struct file_operations difc_bench_ops = {
		.open = difcfs_bench_open,
		.write = difcfs_bench_write,
		.read = seq_read,
		.llseek = seq_lseek,
		.release = seq_release,
	};
#endif /* DIFC_BENCHMARK_ENABLED */
	static const struct tree_descr files[] = {
		[DIFCFS_LOAD] = {"dbp_load_file", &dbp_load_ops, 00200},
#ifdef DIFC_BENCHMARK_ENABLED
		[DIFCFS_BENCHMARK] = {"benchmark", &difc_bench_ops, 00666},
#endif /* DIFC_BENCHMARK_ENABLED */
		{""},
	};
	int ret = simple_fill_super(sb, DIFC_MAGIC, files);
	if (ret < 0)
		pr_err("failed to create inodes (err=%d)\n", ret);
	return ret;
}

static int difcfs_get_tree(struct fs_context *fc)
{
	return get_tree_single(fc, difcfs_fill_super);
}

static int difcfs_init_fs_context(struct fs_context *fc)
{
	const static struct fs_context_operations ops = {
		.get_tree = difcfs_get_tree,
	};
	fc->ops = &ops;
	return 0;
}

static int __init difcfs_init(void)
{
	static struct file_system_type fstype = {
		.name = "tdifcfs",
		.init_fs_context = difcfs_init_fs_context,
		.kill_sb = kill_litter_super,
	};
	struct vfsmount *mnt;
	int ret;

	if ((ret = sysfs_create_mount_point(fs_kobj, "tdifc")))
		return ret;

	if ((ret = register_filesystem(&fstype))) {
		sysfs_remove_mount_point(fs_kobj, "tdifc");
		return ret;
	}

	mnt = kern_mount(&fstype);
	if (IS_ERR(mnt)) {
		pr_err("mount failed\n");
		return PTR_ERR(mnt);
	}

	return ret;
}

__initcall(difcfs_init);
