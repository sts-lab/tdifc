#include "debug.h"

#include <linux/err.h>
#include <linux/rcupdate.h>
#include <linux/sched.h>
#include <linux/sched/task.h>
#include <linux/slab.h>

#include "policy_dsl.h"
#include "cap.h"
#include "cred.h"
#include "policy.h"
#include "tag.h"
#include "benchmark.h"

void difc_policy_stmt_free(struct difc_policy_stmt *stmt)
{
	size_t i;
	for (i = 0; i != stmt->n_data; ++i)
		difc_policy_stmt_data_free(&stmt->data[i]);
}

void difc_policy_stmt_data_free(struct difc_policy_stmt_data *data)
{
	difc_tagvec_free(&data->vec);
}

void difc_policy_block_free(struct difc_policy_block *block)
{
	size_t i;

	kfree(block->pids);

	for (i = 0; i != block->n_stmts; ++i)
		difc_policy_stmt_free(&block->stmts[i]);

	kfree(block->stmts);
}

void difc_action_free(struct difc_action *axn)
{
	size_t i;

	regex_free(&axn->regex);

	for (i = 0; i != axn->n_blocks; ++i)
		difc_policy_block_free(&axn->blocks[i]);

	kfree(axn->blocks);
}

/**
 * For all captures in fragments of @vec, set @string and @length based on
 * @cgs.
 *
 * Note that we are actually overwriting these fields of the statement's
 * template vector on each match.
 */
static void populate_vec(struct difc_tagvec *vec,
			 struct capture *cgs)
{
	size_t i;
	for (i = 0; i != vec->n_frags; ++i) {
		struct difc_tagvec_fragment *frag = &vec->frags[i];
		if (frag->index < REGEX_MAX_CAPTURES) {
			frag->string = (char *) cgs[frag->index].start;
			frag->length = cgs[frag->index].len;
		}
	}
}

int difc_execute_stmt(const struct difc_policy *pol,
		      struct difc_cred *cred,
		      struct difc_policy_stmt *stmt,
		      struct capture *cgs)
{
	int ret;
	size_t i;
	const struct difc_tag_entry *te;
	struct difc_label label;

	for (i = 0; i != stmt->n_data; ++i) {
		populate_vec(&stmt->data[i].vec, cgs);
	}

	switch (stmt->type) {
	case DIFC_ACTION_ADD_TAGS:
		for (i = 0; i != stmt->n_data; ++i) {
			te = difc_map_tagvec(&stmt->data[i].vec, pol->pol_id,
					     stmt->data[i].flags);
			if (unlikely(IS_ERR(te)))
				return PTR_ERR(te);
			if (DIFC_CAP_PLUS &
			    difc_eff_cap_flags(cred, &cred->cap_mask, te->tag_id)) {
				ret = difc_label_add(&cred->label, te->tag_id);
				if (ret < 0)
					return ret;
			} else {
				pr_err("add denied: no + capability\n");
			}
		}
		return 0;

	case DIFC_ACTION_DEL_TAGS:
		for (i = 0; i != stmt->n_data; ++i) {
			te = difc_query_tagvec(&stmt->data[i].vec);
			if (!te)
				continue; /* nothing to do */
			if (unlikely(IS_ERR(te)))
				return PTR_ERR(te);
			if (DIFC_CAP_MINUS &
			    difc_eff_cap_flags(cred, &cred->cap_mask, te->tag_id)) {
				ret = difc_label_remove(&cred->label, te->tag_id);
				if (ret < 0)
					return ret;
			} else {
				pr_err("del denied: no - capability\n");
			}
		}
		return 0;

	case DIFC_ACTION_SET_TAGS:
		label = difc_label(cred->label.ns);
		difc_label_union(&label, &cred->label);
		/* invariant: label contains tags to be removed */
		for (i = 0; i != stmt->n_data; ++i) {
			te = difc_map_tagvec(&stmt->data[i].vec, pol->pol_id,
					     stmt->data[i].flags);
			if (unlikely(IS_ERR(te)))
				return PTR_ERR(te);
			if (difc_label_contains(&cred->label, te->tag_id)) {
				/* don't need to remove this */
				difc_label_remove(&label, te->tag_id);
			} else if (DIFC_CAP_PLUS &
				   difc_eff_cap_flags(cred, &cred->cap_mask,
				                      te->tag_id)) {
				difc_label_add(&cred->label, te->tag_id);
			} else {
				pr_err("add denied: no + capability\n");
			}
		}
		difc_label_diff_checked(cred, &cred->cap_mask, &cred->label, &label);
		return 0;

	case DIFC_ACTION_ADD_CAPS:
		for (i = 0; i != stmt->n_data; ++i) {
			struct difc_cap *cap;
			te = difc_query_tagvec(&stmt->data[i].vec);
			if (!te)
				continue; /* nothing to do */
			if (unlikely(IS_ERR(te)))
				return PTR_ERR(te);
			cap = difc_cap_get(pol->pol_id, te->tag_id);
			if (!cap)
				continue;
			cap->flags |= stmt->data[i].flags;
		}
		return 0;

	case DIFC_ACTION_DEL_CAPS:
		for (i = 0; i != stmt->n_data; ++i) {
			struct difc_cap *cap;
			te = difc_query_tagvec(&stmt->data[i].vec);
			if (!te)
				continue; /* nothing to do */
			if (unlikely(IS_ERR(te)))
				return PTR_ERR(te);
			cap = difc_cap_get(pol->pol_id, te->tag_id);
			if (!cap)
				continue;
			cap->flags &= ~stmt->data[i].flags;
		}
		return 0;

	case DIFC_ACTION_SET_CAPS:
		for (i = 0; i != stmt->n_data; ++i) {
			struct difc_cap *cap;
			te = difc_query_tagvec(&stmt->data[i].vec);
			if (!te)
				continue; /* nothing to do */
			if (unlikely(IS_ERR(te)))
				return PTR_ERR(te);
			cap = difc_cap_get(pol->pol_id, te->tag_id);
			if (!cap)
				continue;
			cap->flags = stmt->data[i].flags;
		}
		return 0;

	case DIFC_ACTION_DROP_CAPS:
		difc_cap_mask_free(&cred->cap_mask);
		cred->cap_mask.drop_rest = true;
		return 0;

//	case DIFC_ACTION_MASK_CAP:
//		te = difc_query_tagvec(&stmt->data->vec);
//		if (!te)
//			return 0; /* nothing to do */
//		if (unlikely(IS_ERR(te)))
//			return PTR_ERR(te);
//		cap.pol_id = pol->pol_id;
//		cap.tag_id = te->tag_id;
//		cap.flags = ~(DIFC_CAP_PLUS | DIFC_CAP_MINUS);
//		return difc_cap_mask_and(&cred->cap_mask, &cap);
//
//	case DIFC_ACTION_UNMASK_CAP:
//		te = difc_query_tagvec(&stmt->data->vec);
//		if (!te)
//			return 0; /* nothing to do */
//		if (unlikely(IS_ERR(te)))
//			return PTR_ERR(te);
//		cap.pol_id = pol->pol_id;
//		cap.tag_id = te->tag_id;
//		cap.flags = DIFC_CAP_PLUS | DIFC_CAP_MINUS;
//		return difc_cap_mask_or(&cred->cap_mask, &cap);

	default:
		return -EINVAL;
	}
}

static void difc_execute_stmts(const struct difc_policy *pol,
			       struct difc_cred *cred,
			       struct difc_policy_stmt *stmts,
			       size_t n_stmts, struct capture *cgs)
{
	DIFC_START_TIMER()
	size_t i;
	for (i = 0; i != n_stmts; ++i) {
		int ret = difc_execute_stmt(pol, cred, &stmts[i], cgs);
		if (ret < 0)
			pr_err("statement failed (type=%d; ret=%d)\n",
				 stmts[i].type, ret);
	}
	DIFC_END_TIMER(cred, "stmts")
}

static void difc_execute_task(const struct difc_policy *pol,
			      struct task_struct *task,
			      struct difc_policy_stmt *stmts,
			      size_t n_stmts, struct capture *cgs)
{
	const struct cred *cred;

	if (!task)
		return;

	rcu_read_lock();
	get_task_struct(task);
	rcu_read_unlock();

	cred = get_task_cred(task);

	difc_execute_stmts(pol, difc_cred(cred), stmts, n_stmts, cgs);

	put_cred(cred);
	put_task_struct(task);
}

static void difc_execute_vpid(const struct difc_policy *pol,
			      pid_t vpid, struct difc_policy_stmt *stmts,
			      size_t n_stmts, struct capture *cgs)
{
	struct task_struct *task;

	rcu_read_lock();
	task = find_task_by_vpid(vpid);
	rcu_read_unlock();

	difc_execute_task(pol, task, stmts, n_stmts, cgs);
}

static void difc_execute_children(const struct difc_policy *pol,
				  struct difc_policy_stmt *stmts,
				  size_t n_stmts, struct capture *cgs)
{
	struct task_struct *child;
	list_for_each_entry(child, &current->children, sibling)
		difc_execute_task(pol, child, stmts, n_stmts, cgs);
}

static void difc_execute_capture(const struct difc_policy *pol,
				 unsigned int cg,
				 struct difc_policy_stmt *stmts,
				 size_t n_stmts, struct capture *cgs)
{
	/* a 32-bit int only requires <= 10 chars (2**31 - 1) + null term */
#define BUFSIZE 11
	char buf[BUFSIZE];
	size_t n = BUFSIZE - 1;
	pid_t vpid;
	if (cgs[cg].len < n)
		n = cgs[cg].len;
	memcpy(buf, cgs[cg].start, n);
	buf[n] = '\0';
	if (kstrtoint(buf, 0, &vpid) < 0) {
		pr_err("capture group %.*s is not valid PID\n",
			 (int) cgs[cg].len, cgs[cg].start);
		return;
	}
	difc_execute_vpid(pol, vpid, stmts, n_stmts, cgs);
}

void difc_execute_block(const struct difc_policy *pol,
			struct difc_cred *cred,
			struct difc_policy_block *block,
			struct capture *cgs)
{
	size_t i;
	for (i = 0; i != block->n_pids; ++i) {
		switch (block->pids[i].type) {
		case DIFC_POLICY_BLOCK_PID_SELF:
			difc_execute_stmts(pol, cred, block->stmts,
					   block->n_stmts, cgs);
			break;
		case DIFC_POLICY_BLOCK_PID_PARENT:
			difc_execute_task(pol, current->real_parent,
					  block->stmts, block->n_stmts, cgs);
			break;
		case DIFC_POLICY_BLOCK_PID_CHILDREN:
			difc_execute_children(pol, block->stmts,
					      block->n_stmts, cgs);
			break;
		case DIFC_POLICY_BLOCK_PID_CAPTURE:
			difc_execute_capture(pol, block->pids[i].pid,
					     block->stmts, block->n_stmts,
					     cgs);
			break;
		case DIFC_POLICY_BLOCK_PID_LITERAL:
			difc_execute_vpid(pol, block->pids[i].pid,
					  block->stmts, block->n_stmts, cgs);
			break;
		}
	}
}
