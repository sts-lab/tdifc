#include "debug.h"

#include <linux/dcache.h>
#include <linux/fs.h>
#include <linux/limits.h>
#include <linux/rbtree.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/xattr.h>

#include "file.h"
#include "benchmark.h"
#include "cred.h"
#include "label.h"
#include "policy.h"
#include "serialize.h"
#include "tag.h"
#include "xattr.h"

int difc_file_get_policy_id(struct difc_policy_id *out, struct file *file)
{
	int ret;
	struct dentry *d = file_dentry(file);
	char *start, *end, *p, *xstr = NULL;
	ssize_t xn;
	struct difc_policy_id id;

	dget(d);
	xn = vfs_getxattr_alloc(d, XATTR_NAME_DIFC_POLICY,
				&xstr, 0, GFP_KERNEL);
	dput(d);
	if (xn < 0)
		return xn;

	start = p = xstr;
	end = xstr + xn;
	id.id = base85_dec(&p, end);
	if (p == start) {
		ret = -ENODATA;
		goto free_xstr;
	} else if (p - start > sizeof(id.id)) {
		ret = -E2BIG;
		goto free_xstr;
	}

	*out = id;
	ret = 0;

free_xstr:
	kfree(xstr);
	return ret;
}


/**
 * Return how many bytes the serialized string of @label would require,
 * including the null terminator.
 */
static size_t label_bytes(const struct difc_label *label)
{
	size_t n = 0;
	struct rb_node *rb;

	for (rb = rb_first_cached(&label->rtags); rb; rb = rb_next(rb)) {
		struct difc_rtag *rtag;
		struct difc_tag_id id;
		unsigned int i;
		rtag = rb_entry(rb, struct difc_rtag, rb_node);
		difc_rtag_for_each(i, id, rtag) {
			unsigned int flags = difc_cap_initial_flags(id);
			const struct difc_tag_entry *te;
			te = difc_query_tag_id(id);

			n += base85_u32_bytes(te->tag.ns.type);
			++n; /* ',' */
			n += base85_u32_bytes(te->tag.ns.id);
			++n; /* ',' */
			n += base85_u32_bytes(te->owner.id);
			++n; /* ',' */
			n += base85_u32_bytes(flags);
			++n; /* ',' */
			n += base85_size_t_bytes(te->tag.length);
			++n; /* ',' */
			n += te->tag.length; /* length of tag.string */
		}
	}

	return n;
}

int difc_file_set_label(struct file *file, const struct difc_label *label)
{
	size_t n = label_bytes(label);
	char *xstr, *p;
	struct rb_node *rb;
	struct dentry *d;
	int ret;

	if (n == 0) {
		/* clear the label */
		d = file_dentry(file);
		dget(d);
		vfs_removexattr(d, XATTR_NAME_DIFC_LABEL);
		dput(d);
		return 0;
	}

	xstr = kmalloc(n, GFP_KERNEL);
	p = xstr;

	if (!xstr) {
		pr_err("alloc failed, bailing out\n");
		return -ENOMEM;
	}

	for (rb = rb_first_cached(&label->rtags); rb; rb = rb_next(rb)) {
		struct difc_rtag *rtag;
		struct difc_tag_id id;
		unsigned int i;
		rtag = rb_entry(rb, struct difc_rtag, rb_node);
		difc_rtag_for_each(i, id, rtag) {
			unsigned int flags = difc_cap_initial_flags(id);
			const struct difc_tag_entry *te;
			te = difc_query_tag_id(id);

			p = base85_enc(p, te->tag.ns.type);
			*p++ = ',';
			p = base85_enc(p, te->tag.ns.id);
			*p++ = ',';
			p = base85_enc(p, te->owner.id);
			*p++ = ',';
			p = base85_enc(p, flags);
			*p++ = ',';
			p = base85_enc(p, te->tag.length);
			*p++ = ',';
			memcpy(p, te->tag.string, te->tag.length);
			p += te->tag.length;
		}
	}

	d = file_dentry(file);
	dget(d);
	inode_lock(d->d_inode);
	ret = __vfs_setxattr_noperm(d, XATTR_NAME_DIFC_LABEL, xstr, n, 0);
	inode_unlock(d->d_inode);
	dput(d);

	return ret;
}

int difc_file_label_for_each(const struct file *file, void *arg,
			     difc_file_callback_t callback)
{
	int ret;
	char *xstr = NULL;
	char *p, *end;
	struct dentry *d = file_dentry(file);
	ssize_t xn;
	
	dget(d);
	xn = vfs_getxattr_alloc(d, XATTR_NAME_DIFC_LABEL,
				&xstr, 0, GFP_KERNEL);
	dput(d);

	if (xn == -ENODATA)
		return 0;
	if (xn < 0)
		return xn;

	p = xstr;
	end = xstr + xn;
	while (p != end) {
		const char *start;
		struct difc_policy_id pol_id;
		struct difc_tag tag;
		unsigned int flags;

#define READ_BASE85(lhs) \
		do { \
			start = p; \
			(lhs) = base85_dec(&p, end); \
			/* ensure non-empty entry is followed by comma */ \
			if (p == start || p == end || *p != ',') { \
				ret = -EINVAL; \
				goto free_xstr; \
			} \
			++p; /* consume the comma */ \
		} while (0)

		READ_BASE85(tag.ns.type);
		READ_BASE85(tag.ns.id);
		READ_BASE85(pol_id.id);
		READ_BASE85(flags);
		READ_BASE85(tag.length);
#undef READ_BASE85

		tag.string = p;
		/* ensure string is within xstr */
		p += tag.length;
		if (p > end) {
			ret = -EINVAL;
			goto free_xstr;
		}

		ret = callback(arg, pol_id, flags, &tag);
		if (ret != 0)
			goto free_xstr;
	}

	ret = 0;

free_xstr:
	kfree(xstr);
	return ret;
}

struct get_label_args {
	struct difc_label *label;
	bool all_tags;
};

static int get_label_callback(void *arg, struct difc_policy_id pol_id,
			      unsigned int flags, struct difc_tag *tag)
{
	struct get_label_args *args = arg;
	if (args->all_tags || difc_ns_equal(tag->ns, args->label->ns)) {
		int ret;
		const struct difc_tag_entry *entry;
		entry = difc_map_tag(tag, pol_id, flags);
		if (IS_ERR(entry))
			return PTR_ERR(entry);
		ret = difc_label_add(args->label, entry->tag_id);
		if (ret < 0)
			return ret;
	}
	return 0;
}

int difc_file_get_label(const struct file *file, struct difc_label *label,
			bool all_tags)
{
	struct get_label_args args = {.label = label, .all_tags = all_tags};
	return difc_file_label_for_each(file, &args, &get_label_callback);
}

/**
 * Add the current process label to @file's label.
 */
static int difc_file_permission_write(struct difc_cred *cred,
				      struct file *file)
{
	DIFC_START_TIMER()
	int ret;
	struct difc_label label = difc_label(cred->label.ns);

	difc_file_get_label(file, &label, true);
	if (!cred->policy) {
		/* could be memoized: double union (one inside flow_perm) */
		difc_label_union(&label, &cred->label);
		ret = difc_file_set_label(file, &label);
		if (ret == -EOPNOTSUPP) {
			/* TODO not sure what to do here... */
			ret = 0;
		} else if (ret < 0)
			pr_err("failed to set file label (ret=%d)\n", ret);
	} else {
		ret = -EPERM;
	}

	difc_label_free(&label);
	DIFC_END_TIMER(cred, "write")
	return ret;
}

struct permission_read_args {
	struct difc_cred *cred;
	struct difc_label label; /* don't union until read is permitted */
};

static int permission_read_callback(void *arg, struct difc_policy_id pol_id,
				    unsigned int flags, struct difc_tag *tag)
{
	struct permission_read_args *args = arg;
	if (difc_ns_equal(args->cred->label.ns, tag->ns)) {
		const struct difc_tag_entry *entry;
		unsigned int eflags;
		int ret;

		entry = difc_map_tag(tag, pol_id, flags);
		if (IS_ERR(entry))
			return PTR_ERR(entry);

		/* tag may already exist; we need to check its real flags */
		eflags = difc_eff_cap_flags(args->cred, &args->cred->cap_mask,
					    entry->tag_id);
		if (!(eflags & DIFC_CAP_PLUS))
			return -EPERM;

		ret = difc_label_add(&args->label, entry->tag_id);
		if (ret < 0)
			return ret;
	}
	return 0;
}

/**
 * Add @file's label to the current process label. If one of the tags doesn't
 * have the associated capability, deny the read instead.
 */
static int difc_file_permission_read(struct difc_cred *cred,
				     struct file *file)
{
	DIFC_START_TIMER()

	struct permission_read_args args = {
		.cred = cred,
		.label = difc_label(cred->label.ns)
	};
	int ret = difc_file_label_for_each(file, &args,
					   &permission_read_callback);
	if (ret == 0) {
		/*
		 * TODO atomic update?
		 * i.e. tmp <- cred + file, cred <- tmp
		 */
		if (cred->policy && !difc_flow_perm(&cred->policy->proc_fpol,
						    &cred->label,
						    &args.label)) {
			ret = -EPERM;
			goto out;
		}
		ret = difc_label_union(&cred->label, &args.label);
	} else if (ret == -EOPNOTSUPP) {
		/* just allow if fs doesn't support xattrs */
		ret = 0;
		goto out;
	}
out:
	difc_label_free(&args.label);
	DIFC_END_TIMER(cred, "read")
	return ret;
}

int difc_file_permission(struct file *file, int mask)
{
	struct difc_cred *cred = difc_cred(current_cred());
	if (mask & MAY_READ) {
		return difc_file_permission_read(cred, file);
	} else if (mask & MAY_WRITE) {
		return difc_file_permission_write(cred, file);
	} else {
		return 0;
	}
}
