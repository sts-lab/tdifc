#include "debug.h"

#include <linux/string.h>

#include "benchmark.h"
#include "policy.h"

#ifdef DIFC_BENCHMARK_ENABLED

LIST_HEAD(difc_benches);
DEFINE_MUTEX(difc_benches_mutex);

void __difc_end_timer(struct difc_cred *cc, const char *s, int64_t ts)
{
	struct difc_bench *i;

	if (!cc->policy
	    || cc->policy->pol_id.id < DIFC_BENCHMARK_MIN_POLID
	    || cc->policy->pol_id.id > DIFC_BENCHMARK_MAX_POLID)
		return;

	if (mutex_lock_killable(&difc_benches_mutex) != 0)
		return;
	list_for_each_entry(i, &difc_benches, list) {
		if (strcmp(s, i->name) == 0) {
			difc_bench_add(i, ts);
			goto unlock;
		}
	}
	/* no entry yet, make a new one */
	i = kzalloc(sizeof(struct difc_bench), GFP_KERNEL);
	if (!i) {
		pr_err("out of memory\n");
		goto unlock;
	}
	i->name = s;
	/* fields are already zero-initialized by kzalloc */
	list_add_tail(&i->list, &difc_benches);
	difc_bench_add(i, ts);
unlock:
	mutex_unlock(&difc_benches_mutex);
}

void difc_bench_add(struct difc_bench *bench, int64_t x)
{
	int64_t d1_fixp, d2_fixp, x_fixp = difc_to_fixp(x);
	++bench->count;
	d1_fixp = x_fixp - bench->avg_fixp;
	bench->avg_fixp += d1_fixp / bench->count;
	d2_fixp = x_fixp - bench->avg_fixp;
	bench->m2_fixp += difc_fixp_mul(d1_fixp, d2_fixp);
}

#endif
